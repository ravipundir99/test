<?php
/*
 *	Paradox Labs, Inc.
 *	http://www.paradoxlabs.com
 *	717.431.3330
 *	Twitter: ParadoxLabs
 *
 *	ParadoxLabs_Testimonials Magento Plugin
 *
 *	Having a problem with the plugin?
 *	Not sure what something means?
 *	Need custom development?
 *		Give us a call!
 *
 *	Copyright 2010, Paradox Labs, Inc.
 *	Licensed under OSL 3.0 License
 *		http://opensource.org/licenses/osl-3.0.php
 */

	class ParadoxLabs_Testimonials_Model_Status extends Varien_Object
	{
		const STATUS_ENABLED	= 1;
		const STATUS_DISABLED	= 2;

		static public function getOptionArray()
		{
			return array(
				self::STATUS_ENABLED    => Mage::helper('testimonials')->__('Enabled'),
				self::STATUS_DISABLED   => Mage::helper('testimonials')->__('Disabled')
			);
		}
	}