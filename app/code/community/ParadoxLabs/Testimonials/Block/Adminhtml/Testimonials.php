<?php
/*
 *	Paradox Labs, Inc.
 *	http://www.paradoxlabs.com
 *	717.431.3330
 *	Twitter: ParadoxLabs
 *
 *	ParadoxLabs_Testimonials Magento Plugin
 *
 *	Having a problem with the plugin?
 *	Not sure what something means?
 *	Need custom development?
 *		Give us a call!
 *
 *	Copyright 2010, Paradox Labs, Inc.
 *	Licensed under OSL 3.0 License
 *		http://opensource.org/licenses/osl-3.0.php
 */

	class ParadoxLabs_Testimonials_Block_Adminhtml_Testimonials extends Mage_Adminhtml_Block_Widget_Grid_Container
	{
		public function __construct()
		{
			$this->_controller = 'adminhtml_testimonials';
			$this->_blockGroup = 'testimonials';
			$this->_headerText = Mage::helper('testimonials')->__('Manage Testimonials');
			$this->_addButtonLabel = Mage::helper('testimonials')->__('Add New Testimonial');
			parent::__construct();
		}
	}