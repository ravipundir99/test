<?php
/*
 *	Paradox Labs, Inc.
 *	http://www.paradoxlabs.com
 *	717.431.3330
 *	Twitter: ParadoxLabs
 *
 *	ParadoxLabs_Testimonials Magento Plugin
 *
 *	Having a problem with the plugin?
 *	Not sure what something means?
 *	Need custom development?
 *		Give us a call!
 *
 *	Copyright 2010, Paradox Labs, Inc.
 *	Licensed under OSL 3.0 License
 *		http://opensource.org/licenses/osl-3.0.php
 */

	class ParadoxLabs_Testimonials_Block_Adminhtml_Testimonials_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
	{
		protected function _prepareLayout()
		{
			parent::_prepareLayout();
			if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
				$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
			}
		}
		
		protected function _prepareForm()
		{
			$form = new Varien_Data_Form(array(
										  'id' => 'edit_form',
										  'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
										  'method' => 'post'
				));
		  
			$fieldset = $form->addFieldset('testimonials_form', array(
				'legend'	  => Mage::helper('testimonials')->__('Testimonial'),
				'class'		=> 'fieldset-wide'
			  )
			);

			$fieldset->addField('testimonial_name', 'text', array(
				'name'      => 'name',
				'label'     => Mage::helper('testimonials')->__('Name'),
				'class'     => 'required-entry',
				'required'  => true,
			));

			$fieldset->addField('testimonial_store', 'select', array(
				'name'      => 'store',
				'label'     => Mage::helper('core')->__('Store View'),
				'title'     => Mage::helper('core')->__('Store View'),
				'required'  => true,
				'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
			));

			$fieldset->addField('testimonial_text', 'editor', array(
				'name'      => 'text',
				'label'     => Mage::helper('testimonials')->__('Text'),
				'title'     => Mage::helper('testimonials')->__('Text'),
				'style'     => 'width:100%;height:300px;',
				'required'  => true,
				'config'    => Mage::getSingleton('testimonials/wysiwyg_config')->getConfig()
			));

			if (Mage::registry('testimonials')) {
			  $form->setValues(Mage::registry('testimonials')->getData());
			}

			$form->setUseContainer(true);
			$this->setForm($form);
			return parent::_prepareForm();
		}
	}