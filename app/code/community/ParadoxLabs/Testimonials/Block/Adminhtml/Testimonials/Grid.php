<?php
/*
 *	Paradox Labs, Inc.
 *	http://www.paradoxlabs.com
 *	717.431.3330
 *	Twitter: ParadoxLabs
 *
 *	ParadoxLabs_Testimonials Magento Plugin
 *
 *	Having a problem with the plugin?
 *	Not sure what something means?
 *	Need custom development?
 *		Give us a call!
 *
 *	Copyright 2010, Paradox Labs, Inc.
 *	Licensed under OSL 3.0 License
 *		http://opensource.org/licenses/osl-3.0.php
 */

	class ParadoxLabs_Testimonials_Block_Adminhtml_Testimonials_Grid extends Mage_Adminhtml_Block_Widget_Grid
	{

		public function __construct()
		{
			parent::__construct();
			$this->setId('testimonialsGrid');
			$this->setDefaultSort('testimonial_name');
			$this->setDefaultDir('ASC');
			$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
			$this->setCollection(Mage::getModel('testimonials/testimonials')->getCollection());
			return parent::_prepareCollection();
		}

		protected function _prepareColumns()
		{
			$this->addColumn('id', array(
				'header'    => Mage::helper('testimonials')->__('ID'),
				'align'     => 'right',
				'width'     => '50px',
				'index'     => 'testimonial_id',
				'type'      => 'number',
			));

			$this->addColumn('testimonial_name', array(
				'header'    => Mage::helper('testimonials')->__('Name'),
				'align'     => 'left',
				'index'     => 'testimonial_name',
			));

			$this->addColumn('testimonial_text', array(
				'header'    => Mage::helper('testimonials')->__('Text'),
				'align'     => 'left',
				'index'     => 'testimonial_text',
			));
			
            $this->addColumn('testimonial_store', array(
                'header'        => Mage::helper('cms')->__('Store View'),
                'index'         => 'testimonial_store',
                'type'          => 'store',
                'store_all'     => true,
                'store_view'    => true,
                'sortable'      => false,
            ));
			
			return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			return $this->getUrl('*/*/edit', array('id' => $row->getId()));
		}

	}
