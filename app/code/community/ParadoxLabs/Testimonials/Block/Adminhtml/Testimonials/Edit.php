<?php
/*
 *	Paradox Labs, Inc.
 *	http://www.paradoxlabs.com
 *	717.431.3330
 *	Twitter: ParadoxLabs
 *
 *	ParadoxLabs_Testimonials Magento Plugin
 *
 *	Having a problem with the plugin?
 *	Not sure what something means?
 *	Need custom development?
 *		Give us a call!
 *
 *	Copyright 2010, Paradox Labs, Inc.
 *	Licensed under OSL 3.0 License
 *		http://opensource.org/licenses/osl-3.0.php
 */

	class ParadoxLabs_Testimonials_Block_Adminhtml_Testimonials_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
	{
		public function __construct()
		{
			parent::__construct();
			
			$this->_objectId = 'id';
			$this->_blockGroup = 'testimonials';
			$this->_controller = 'adminhtml_testimonials';
			
			$this->_updateButton('save', 'label', Mage::helper('testimonials')->__('Save Testimonial'));
			$this->_updateButton('delete', 'label', Mage::helper('testimonials')->__('Delete Testimonial'));

			if( $this->getRequest()->getParam($this->_objectId) ) {
				$model = Mage::getModel('testimonials/testimonials')->load($this->getRequest()->getParam($this->_objectId));
				Mage::register('testimonials', $model);
			}
		}

		public function getHeaderText()
		{
			if( Mage::registry('testimonials') && Mage::registry('testimonials')->getId() ) {
				return Mage::helper('testimonials')->__('Edit Testimonial');
			} else {
				return Mage::helper('testimonials')->__('Add Testimonial');
			}
		}
	}
