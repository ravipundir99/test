<?php
/*
 *	Paradox Labs, Inc.
 *	http://www.paradoxlabs.com
 *	717.431.3330
 *	Twitter: ParadoxLabs
 *
 *	ParadoxLabs_Testimonials Magento Plugin
 *
 *	Having a problem with the plugin?
 *	Not sure what something means?
 *	Need custom development?
 *		Give us a call!
 *
 *	Copyright 2010, Paradox Labs, Inc.
 *	Licensed under OSL 3.0 License
 *		http://opensource.org/licenses/osl-3.0.php
 */

	class ParadoxLabs_Testimonials_Block_View extends Mage_Core_Block_Template
	{
		public function getRandomTestimonial() {
			$w = Mage::getSingleton('core/resource')->getConnection('core_read');
			$table = Mage::getSingleton('core/resource')->getTableName('testimonials/testimonials');
			$store = Mage::app()->getStore()->getStoreId();
			
			$result = $w->select()
						->from( array( 't' => $table ),
								array(	'testimonial_id',
										'testimonial_name',
										'testimonial_text' => "concat(substring_index(testimonial_text,' ',40),'...')" ) )
						->where('testimonial_store in(0,?)', $store)
						->order('rand()')
						->limit(1, 0);
			$row = $w->fetchRow($result);
			
			$row['testimonial_text'] = str_replace(array('<p>', '</p>', '<br />...'), array('', '<br />', ''), $row['testimonial_text']);
			$row['testimonial_text'] = Mage::helper('cms')->getBlockTemplateProcessor()->filter($row['testimonial_text']);

			return $row;
		}
	}
