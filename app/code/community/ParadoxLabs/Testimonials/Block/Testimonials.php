<?php
/*
 *	Paradox Labs, Inc.
 *	http://www.paradoxlabs.com
 *	717.431.3330
 *	Twitter: ParadoxLabs
 *
 *	ParadoxLabs_Testimonials Magento Plugin
 *
 *	Having a problem with the plugin?
 *	Not sure what something means?
 *	Need custom development?
 *		Give us a call!
 *
 *	Copyright 2010, Paradox Labs, Inc.
 *	Licensed under OSL 3.0 License
 *		http://opensource.org/licenses/osl-3.0.php
 */

	class ParadoxLabs_Testimonials_Block_Testimonials extends Mage_Core_Block_Template
	{
		public function _prepareLayout()
		{
			return parent::_prepareLayout();
		}
		
		 public function getTestimonials()     
		 { 
			if (!$this->hasData('testimonials')) {
				$this->setData('testimonials', Mage::registry('testimonials'));
			}
			return $this->getData('testimonials');
		}
		
		public function getTestimonialById( $id )
		{
			$w = Mage::getSingleton('core/resource')->getConnection('core_read');
			$table = Mage::getSingleton('core/resource')->getTableName('testimonials/testimonials');
			$store = Mage::app()->getStore()->getStoreId();
			
			$result = $w->select()
						->from( $table )
						->where( 'testimonial_id=?', $id )
						->where( 'testimonial_store in(0,?)', $store );
			
			$row = $w->fetchRow($result);
			
			$row['testimonial_text'] = Mage::helper('cms')->getBlockTemplateProcessor()->filter($row['testimonial_text']);

			return $row;
		}
		
		public function getLatestTestimonials( $num=15 )
		{
			$w = Mage::getSingleton('core/resource')->getConnection('core_read');
			$table = Mage::getSingleton('core/resource')->getTableName('testimonials/testimonials');
			$store = Mage::app()->getStore()->getStoreId();
			
			$result = $w->select()
						->from($table)
						->where('testimonial_store in(0,?)', $store)
						->order('testimonial_id desc')
						->limit($num,0);
			
			$rows = $w->fetchAll($result);
			
			foreach($rows as $k => $row) {
				$rows[$k]['testimonial_text'] = Mage::helper('cms')->getBlockTemplateProcessor()->filter($row['testimonial_text']);
			}
			
			return $rows;
		}
	}