<?php
/*
 *	Paradox Labs, Inc.
 *	http://www.paradoxlabs.com
 *	717.431.3330
 *	Twitter: ParadoxLabs
 *
 *	ParadoxLabs_Testimonials Magento Plugin
 *
 *	Having a problem with the plugin?
 *	Not sure what something means?
 *	Need custom development?
 *		Give us a call!
 *
 *	Copyright 2010, Paradox Labs, Inc.
 *	Licensed under OSL 3.0 License
 *		http://opensource.org/licenses/osl-3.0.php
 */

	class ParadoxLabs_Testimonials_Adminhtml_TestimonialsController extends Mage_Adminhtml_Controller_Action
	{
		public function indexAction()
		{
			$this->loadLayout();
			$this->_setActiveMenu('cms/testimonials');
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Testimonials'), Mage::helper('adminhtml')->__('Testimonials'));

			$this->renderLayout();
		}

		public function editAction()
		{
			$this->loadLayout();
			$this->_setActiveMenu('cms/testimonials');
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Testimonials'), Mage::helper('adminhtml')->__('Testimonials'));

			$this->_addContent($this->getLayout()->createBlock('testimonials/adminhtml_testimonials_edit'));
			$this->renderLayout();
		}

		public function newAction()
		{
			$this->editAction();
		}

		public function saveAction()
		{
			if ( $this->getRequest()->getPost() ) {
				try {
					$model = Mage::getModel('testimonials/testimonials')
						->setTestimonialId($this->getRequest()->getParam('id'))
						->setTestimonialStore($this->getRequest()->getParam('store'))
						->setTestimonialName($this->getRequest()->getParam('name'))
						->setTestimonialText($this->getRequest()->getParam('text'))
						->save();

					Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Testimonial was successfully saved'));

					$this->_redirect('*/*/');
					return;
				} catch (Exception $e) {
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
					$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
					return;
				}
			}
			$this->_redirect('*/*/');
		}

		public function deleteAction()
		{
			if( $this->getRequest()->getParam('id') > 0 ) {
				try {
					$model = Mage::getModel('testimonials/testimonials');
					$model->setTestimonialId($this->getRequest()->getParam('id'))
						->delete();
					Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Testimonial was successfully deleted'));
					$this->_redirect('*/*/');
				} catch (Exception $e) {
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
					$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				}
			}
			$this->_redirect('*/*/');
		}

		protected function _isAllowed()
		{
			return Mage::getSingleton('admin/session')->isAllowed('cms/testimonials');
		}
	}
