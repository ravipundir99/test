<?php
/*
 *	Paradox Labs, Inc.
 *	http://www.paradoxlabs.com
 *	717.431.3330
 *	Twitter: ParadoxLabs
 *
 *	ParadoxLabs_Testimonials Magento Plugin
 *
 *	Having a problem with the plugin?
 *	Not sure what something means?
 *	Need custom development?
 *		Give us a call!
 *
 *	Copyright 2010, Paradox Labs, Inc.
 *	Licensed under OSL 3.0 License
 *		http://opensource.org/licenses/osl-3.0.php
 */

	$table = $this->getTable('testimonials');

	$this->startSetup()->run("
		drop table if exists {$table};
		create table {$table} (
			testimonial_id int(11) unsigned not null auto_increment,
			testimonial_store int(11) not null,
			testimonial_name varchar(50) not null default '',
			testimonial_text text not null default '',
			PRIMARY KEY(testimonial_id)
		) engine=InnoDB default charset=utf8;
	")->endSetup();
