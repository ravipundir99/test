<?php

class Orderstatusmail_Custmail_Model_Observer
{
    public function invoicedStatusChange($event)
    {
        $order = $event->getOrder();
        $orderStatus = $order->getStatus();
      if ($order->getStatus() == "low_quality_design")
            $this->_sendStatusMail($order);
	   		
    }
 
    private  function _sendStatusMail($order)
    {
        $emailTemplate  = Mage::getModel('core/email_template');
        $idd = $order->getIncrementId();
        $emailTemplate->loadDefault('custom_order_tpl');
        $emailTemplate->setTemplateSubject('Order'.' '.'#'.$idd.' '.'Request for high quality image');
 
        // Get General email address (Admin->Configuration->General->Store Email Addresses)
        $salesData['email'] = Mage::getStoreConfig('trans_email/ident_general/email');
        $salesData['name'] = Mage::getStoreConfig('trans_email/ident_general/name');
 
        $emailTemplate->setSenderName($salesData['name']);
        $emailTemplate->setSenderEmail($salesData['email']);
 
        $emailTemplateVariables['username']  = $order->getCustomerFirstname() . ' ' . $order->getCustomerLastname();
        $emailTemplateVariables['order_id'] = $order->getIncrementId();
        $emailTemplateVariables['store_name'] = $order->getStoreName();
        $emailTemplateVariables['store_url'] = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        $emailTemplate->send($order->getCustomerEmail(), $order->getStoreName(), $emailTemplateVariables);
    }
}

?>