<?php
	
	$installer = $this;
	$installer->startSetup();
	
	$installer->run("
		DROP TABLE IF EXISTS {$this->getTable('weplugin/settings')};
		CREATE TABLE IF NOT EXISTS {$this->getTable('weplugin/settings')} (
		`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		`option_key` varchar(64) NOT NULL default '',
		`option_value` text NOT NULL default '',
		PRIMARY KEY (`id`)
		)  ENGINE=InnoDB DEFAULT CHARSET=utf8; 	
	");
	
	$installer->run("INSERT INTO {$this->getTable('weplugin/settings')} (option_key, option_value) values ('license_code', '');");
	$installer->run("INSERT INTO {$this->getTable('weplugin/settings')} (option_key, option_value) values ('widget_status', '');");
	
	$installer->endSetup();

