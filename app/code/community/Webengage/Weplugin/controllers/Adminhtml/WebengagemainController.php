<?php

class Webengage_Weplugin_Adminhtml_WebengagemainController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction() 
	{
		$this->loadLayout();
		$this->_setActiveMenu('webengage_menu_head');  
		$this->renderLayout();  
	  
	}  
	
	public function postAction() 
	{
		Mage::helper('weplugin')->handleRequest($this->getRequest()->getParams()); 
	}    
	
	protected function _isAllowed()
	{
		return Mage::getSingleton('admin/session')->isAllowed('webengage_menu_head/webengage_menu_main');
	}
}

