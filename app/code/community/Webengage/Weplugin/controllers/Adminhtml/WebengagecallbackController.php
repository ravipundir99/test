<?php

class Webengage_Weplugin_Adminhtml_WebengagecallbackController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction() 
	{
		$this->loadLayout();
		$this->renderLayout();  
	}  
	
	public function postAction() 
	{
		Mage::helper('weplugin')->handleRequest($this->getRequest()->getParams()); 
	}
}

