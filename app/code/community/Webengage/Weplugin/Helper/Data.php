<?php

	class Webengage_Weplugin_Helper_Data extends Mage_Core_Helper_Abstract
	{
		public function _construct()
		{
			parent::_construct();
			$this->init('weplugin/weplugin');
		}
		
		public function readLicenseCodeFromDb()
		{
			$licenseCode = "";
			
			$resource = Mage::getSingleton('core/resource');
			$connection = $resource->getConnection('core_write');	
			if($connection->isTableExists("webengage_settings"))
			{
				try
				{	
					$model = Mage::getModel('weplugin/settings');
					$model = $model->load(1);
					if($model->getId())
					{
						$licenseCode = $model->getOptionValue();
					}
				}
				catch(Exception $e)
				{
					
				}
			}
			
			return $licenseCode;
		}
		
		public function readWidgetStatusFromDb()
		{
			$widgetStatus = 0;
			
			$resource = Mage::getSingleton('core/resource');
			$connection = $resource->getConnection('core_write');	
			if($connection->isTableExists("webengage_settings"))
			{
				try
				{	
					$model = Mage::getModel('weplugin/settings');
					$model = $model->load(2);
					if($model->getId())
					{
						$widgetStatus = $model->getOptionValue();
					}
				}
				catch(Exception $e)
				{
					
				}
			}
			
			return $widgetStatus;
		}
		
		public function writeLicenseCodeToDb($licenseCode)
		{
			try 
			{
				$resource = Mage::getSingleton('core/resource');
				$connection = $resource->getConnection('core_write');	
				if($connection->isTableExists("webengage_settings"))
				{
					$model = Mage::getModel('weplugin/settings');
					$model = $model->load(1);
					if($model->getId())
					{
						$data = array('option_value' => $licenseCode);
					   	$model->addData($data)->setId(1)->save();
					}
				} 
			}
			catch (Exception $e) 
			{
			    return false;
			}
			
			return true;
		}
		
		public function writeWidgetStatusToDb($widgetStatus)
		{
			try 
			{
				$resource = Mage::getSingleton('core/resource');
				$connection = $resource->getConnection('core_write');	
				if($connection->isTableExists("webengage_settings"))
				{
					$model = Mage::getModel('weplugin/settings');
					$model = $model->load(2);
					if($model->getId())
					{
						$data = array('option_value' => $widgetStatus);
					   	$model->addData($data)->setId(2)->save();
					}
				} 
			}
			catch (Exception $e) 
			{
			    return false;
			}
			
			return true;
		}
		
		public function redirectToUrl($mRedirectUrl)
		{
			Mage::app()->getResponse()->setRedirect($mRedirectUrl);
		}
		
		public function convertWidgetStatusToMagento($widgetStatus)
		{
			if($widgetStatus == "ACTIVE" || $widgetStatus == "active")
			{
			  return 1;
			}
			else
			{
			  return 0;
			}
		}
		
		
		public function handleRequest($mReq) 
		{
			if(isset($mReq['weAction']))
			{
			    if ($mReq['weAction'] === 'wp-save') {
				$message = $this->update_webengage_options($mReq);
				Mage::helper('weplugin')->redirectToUrl(Mage::helper("adminhtml")->getUrl("weplugin/adminhtml_webengagemain/",array($message[0]=>$message[1]))); 
		
			    } else if ($mReq['weAction'] === 'reset') {
				$message = $this->reset_webengage_options($mReq);
				Mage::helper('weplugin')->redirectToUrl(Mage::helper("adminhtml")->getUrl("weplugin/adminhtml_webengagemain/",array($message[0]=>$message[1]))); 

			    } else if ($mReq['weAction'] === 'activate') {
				$message = $this->activate_we_widget($mReq);
				Mage::helper('weplugin')->redirectToUrl(Mage::helper("adminhtml")->getUrl("weplugin/adminhtml_webengagemain/",array($message[0]=>$message[1]))); 

			    } else if ($mReq['weAction'] === 'discardMessage') {
				$this->discard_status_message($mReq);
				Mage::helper('weplugin')->redirectToUrl(Mage::helper("adminhtml")->getUrl("weplugin/adminhtml_webengagemain/")); 
			    }
			 }
		}    
	
		// discarding the widget status message, in case user activated account on WebEngage Dashboard
		function discard_status_message ($mReq) {
		      Mage::helper('weplugin')->writeWidgetStatusToDb("ACTIVE");
		}
	
		// resetting webengage option
		function reset_webengage_options($mReq) {
			      Mage::helper('weplugin')->writeLicenseCodeToDb("");
			      Mage::helper('weplugin')->writeWidgetStatusToDb("");
			      return array("message", "Your WebEngage options are deleted. You can signup for a new account.");
		}

		// updating webengage option
		function update_webengage_options($mReq) {
		      $wlc = isset($mReq['webengage_license_code']) ? $mReq['webengage_license_code'] : "";
		      $vm = isset($mReq['verification_message']) ? $mReq['verification_message'] : "";
		      $wws = isset($mReq['webengage_widget_status']) ? $mReq['webengage_widget_status'] : "ACTIVE";
		      
		      if(!empty($wlc)) {
			      Mage::helper('weplugin')->writeLicenseCodeToDb(trim($wlc));
			      Mage::helper('weplugin')->writeWidgetStatusToDb($wws);
			      $msg = !empty($vm) ? $vm : "Your WebEngage widget license code has been updated.";
			      return array("message", $msg);
		      } else {
			      return array("error-message", "Please add a license code.");
		      }
		}

		// activating webengage widget
		function activate_we_widget($mReq){
		      $mLicenseCodeOld = Mage::helper('weplugin')->readLicenseCodeFromDb();
		      $mLicenseCodeNew = isset($mReq['webengage_license_code']) ? $mReq['webengage_license_code'] : "";
		      $wws = isset($mReq['webengage_widget_status']) ? $mReq['webengage_widget_status'] : "ACTIVE";
		      
		      if ($mLicenseCodeNew === $mLicenseCodeOld) {
			      Mage::helper('weplugin')->writeWidgetStatusToDb($wws);
			      $msg = "Your weplugin installation is complete. You can do further customizations from your WebEngage dashboard.";
			      return array("message", $msg);
		      } else {
			      $msg = "Unauthorized weplugin activation request";
			      return array("error-message", $msg);
		      }
		}
		
		function getUrlWrapper ($url) {
		      return rtrim($url, "/");
		}
	}
	
