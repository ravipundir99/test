<?php
	
class Report_Reportproblem_Block_Adminhtml_Reportproblem_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "problem_id";
				$this->_blockGroup = "reportproblem";
				$this->_controller = "adminhtml_reportproblem";
				$this->_updateButton("save", "label", Mage::helper("reportproblem")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("reportproblem")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("reportproblem")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("reportproblem_data") && Mage::registry("reportproblem_data")->getId() ){

				    return Mage::helper("reportproblem")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("reportproblem_data")->getName()));

				} 
				else{

				     return Mage::helper("reportproblem")->__("Add Item");

				}
		}
}