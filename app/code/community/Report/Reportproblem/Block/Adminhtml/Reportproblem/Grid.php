<?php

class Report_Reportproblem_Block_Adminhtml_Reportproblem_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("reportproblemGrid");
				$this->setDefaultSort("problem_id");
				$this->setDefaultDir("ASC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("reportproblem/reportproblem")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("problem_id", array(
				"header" => Mage::helper("reportproblem")->__("ID"),
				"align" =>"right",
				"width" => "50px",
				"index" => "problem_id",
				));
				$this->addColumn("name", array(
				"header" => Mage::helper("reportproblem")->__("Reportproblem Name"),
				"align" =>"left",
				"index" => "name",
				));


				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}

}