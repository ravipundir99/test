<?php
class Report_Reportproblem_Block_Adminhtml_Reportproblem_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("reportproblem_form", array("legend"=>Mage::helper("reportproblem")->__("Item information")));

				$fieldset->addField("name", "text", array(
				"label" => Mage::helper("reportproblem")->__("Reportproblem Name"),
				"class" => "required-entry",
				"required" => true,
				"name" => "name",
				));




				if (Mage::getSingleton("adminhtml/session")->getReportproblemData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getReportproblemData());
					Mage::getSingleton("adminhtml/session")->setReportproblemData(null);
				} 
				elseif(Mage::registry("reportproblem_data")) {
				    $form->setValues(Mage::registry("reportproblem_data")->getData());
				}
				return parent::_prepareForm();
		}
}
