<?php

class Printnow_Printnow_Adminhtml_PrintnowController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("printnow/printnow")->_addBreadcrumb(Mage::helper("adminhtml")->__("Printnow  Manager"),Mage::helper("adminhtml")->__("Printnow Manager"));
				return $this;
		}
		public function indexAction() 
		{
				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{
				$brandsId = $this->getRequest()->getParam("id");
				$brandsModel = Mage::getModel("printnow/printnow")->load($brandsId);
				if ($brandsModel->getId() || $brandsId == 0) {
					Mage::register("printnow_data", $brandsModel);
					$this->loadLayout();
					$this->_setActiveMenu("printnow/printnow");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Printnow Manager"), Mage::helper("adminhtml")->__("Printnow Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Printnow Description"), Mage::helper("adminhtml")->__("Printnow Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("printnow/adminhtml_printnow_edit"))->_addLeft($this->getLayout()->createBlock("printnow/adminhtml_printnow_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("printnow")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("printnow/printnow")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("printnow_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("printnow/printnow");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Printnow Manager"), Mage::helper("adminhtml")->__("Printnow Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Printnow Description"), Mage::helper("adminhtml")->__("Printnow Description"));


		$this->_addContent($this->getLayout()->createBlock("printnow/adminhtml_printnow_edit"))->_addLeft($this->getLayout()->createBlock("printnow/adminhtml_printnow_edit_tabs"));

		$this->renderLayout();

		       // $this->_forward("edit");
		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {

					try {
						
						
						if(isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') {
							try {  
									$uploader = new Varien_File_Uploader('photo');
									$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
									$uploader->setAllowRenameFiles(false);
									$uploader->setFilesDispersion(false);
									$path = Mage::getBaseDir('media/printnow') . DS ;
									$uploader->save($path, $_FILES['photo']['name']);
									$post_data['photo'] = $_FILES['photo']['name']; 	
									}
							catch (Exception $e) {
										 print_r($e);
										 die;
									 }  		
								}
						
						
						
						$brandsModel = Mage::getModel("printnow/printnow")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Printnow was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setPrintnowData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $brandsModel->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setPrintnowData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$brandsModel = Mage::getModel("printnow/printnow");
						$brandsModel->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}
}
