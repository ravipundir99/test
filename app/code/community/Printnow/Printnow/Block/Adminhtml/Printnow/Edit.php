<?php
	
class Printnow_Printnow_Block_Adminhtml_Printnow_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "printnow_id";
				$this->_blockGroup = "printnow";
				$this->_controller = "adminhtml_printnow";
				$this->_updateButton("save", "label", Mage::helper("printnow")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("printnow")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("printnow")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);
				
				$this->_addButton("Download", array(
					"label"     => Mage::helper("printnow")->__("Download File"),
					"onclick"   => "downlodimg()",//Mage::getModel(Mage_Core_Model_Store::URL_TYPE_WEB)
					"class"     => "save",
				), -100);
				$tid = Mage::registry("printnow_data")->getId();
				$targetfolder = 'printnow';
				$location = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)."downloadfile.php?dfile=".$this->_blockGroup."&did=".$tid."&folder=".$targetfolder;


				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
							function downlodimg(){
							location.href='$location';
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("printnow_data") && Mage::registry("printnow_data")->getId() ){

				    return Mage::helper("printnow")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("printnow_data")->getName()));

				} 
				else{

				     return Mage::helper("printnow")->__("Add Item");

				}
		}
}