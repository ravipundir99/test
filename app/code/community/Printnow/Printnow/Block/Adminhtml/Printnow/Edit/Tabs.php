<?php
class Printnow_Printnow_Block_Adminhtml_Printnow_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("printnow_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("printnow")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("printnow")->__("Item Information"),
				"title" => Mage::helper("printnow")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("printnow/adminhtml_printnow_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
