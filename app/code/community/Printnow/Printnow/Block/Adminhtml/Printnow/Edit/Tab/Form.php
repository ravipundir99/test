<?php
class Printnow_Printnow_Block_Adminhtml_Printnow_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("printnow_form", array("legend"=>Mage::helper("printnow")->__("Item information")));

				$fieldset->addField("name", "text", array(
				"label" => Mage::helper("printnow")->__("Name"),
				"class" => "required-entry",
				"required" => true,
				"name" => "name",
				));
				$fieldset->addField("email","text", array(
				'label'    => Mage::helper("printnow")->__("Email"),
				//'width'     => '150px',
				"name"     => "email",
				 ));
				 $fieldset->addField("phone","text", array(
				'label'    => Mage::helper("printnow")->__("Contact no"),
				//'width'     => '150px',
				"name"     => "phone",
				 ));
				 $fieldset->addField("quantity","text", array(
				'label'    => Mage::helper("printnow")->__("Quantity"),
				//'width'     => '150px',
				"name"     => "quantity",
				 ));
				 $fieldset->addField('content', 'editor', array(
				  'name'      => 'content',
				  'label'     => Mage::helper('printnow')->__('Content'),
				  'title'     => Mage::helper('printnow')->__('Content'),
				  'style'     => 'width:500px; height:300px;',
				  'wysiwyg'   => false,
				  'required'  => true,
				));
				/*$fieldset->addField('photo', 'image', array(
				 'label'     => Mage::helper('printnow')->__('File'),
				'required'  => false,
				'name'      => 'photo',
				));*/



				if (Mage::getSingleton("adminhtml/session")->getPrintnowData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getPrintnowData());
					Mage::getSingleton("adminhtml/session")->setPrintnowData(null);
				} 
				elseif(Mage::registry("printnow_data")) {
				    $form->setValues(Mage::registry("printnow_data")->getData());
				}
				return parent::_prepareForm();
		}
}
