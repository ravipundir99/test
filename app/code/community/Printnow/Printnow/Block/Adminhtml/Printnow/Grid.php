<?php

class Printnow_Printnow_Block_Adminhtml_Printnow_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("printnowGrid");
				$this->setDefaultSort("printnow_id");
				$this->setDefaultDir("ASC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("printnow/printnow")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("printnow_id", array(
				"header" => Mage::helper("printnow")->__("ID"),
				"align" =>"right",
				"width" => "50px",
				"index" => "printnow_id",
				));
				$this->addColumn("name", array(
				"header" => Mage::helper("printnow")->__("Printnow Name"),
				"align" =>"left",
				"index" => "name",
				));


				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}

}