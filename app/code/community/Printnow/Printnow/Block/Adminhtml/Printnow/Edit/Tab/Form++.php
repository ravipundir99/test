<?php
class Printnow_Printnow_Block_Adminhtml_Printnow_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("printnow_form", array("legend"=>Mage::helper("printnow")->__("Item information")));

				$fieldset->addField("name", "text", array(
				"label" => Mage::helper("printnow")->__("Printnow Name"),
				"class" => "required-entry",
				"required" => true,
				"name" => "name",
				));




				if (Mage::getSingleton("adminhtml/session")->getPrintnowData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getPrintnowData());
					Mage::getSingleton("adminhtml/session")->setPrintnowData(null);
				} 
				elseif(Mage::registry("printnow_data")) {
				    $form->setValues(Mage::registry("printnow_data")->getData());
				}
				return parent::_prepareForm();
		}
}
