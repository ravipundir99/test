<?php


class Printnow_Printnow_Block_Adminhtml_Printnow extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_printnow";
	$this->_blockGroup = "printnow";
	$this->_headerText = Mage::helper("printnow")->__("Printnow Manager");
	$this->_addButtonLabel = Mage::helper("printnow")->__("Add New Item");
	parent::__construct();

	}

}