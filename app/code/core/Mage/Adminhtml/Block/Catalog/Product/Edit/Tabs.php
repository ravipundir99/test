<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * admin product edit tabs
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Adminhtml_Block_Catalog_Product_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    protected $_attributeTabBlock = 'adminhtml/catalog_product_edit_tab_attributes';

    public function __construct()
    {
        parent::__construct();
        $this->setId('product_info_tabs');
        $this->setDestElementId('product_edit_form');
        $this->setTitle(Mage::helper('catalog')->__('Product Information'));
    }

    protected function _prepareLayout()
    {
        $product = $this->getProduct();
		
        if (!($setId = $product->getAttributeSetId())) {
            $setId = $this->getRequest()->getParam('set', null);
        }

        if ($setId) {
            $groupCollection = Mage::getResourceModel('eav/entity_attribute_group_collection')
                ->setAttributeSetFilter($setId)
                ->setSortOrder()
                ->load();

            foreach ($groupCollection as $group) {
                $attributes = $product->getAttributes($group->getId(), true);
				$asd = $group->getAttributeGroupName();	
				/*if($group->getAttributeGroupName()=='Upload Pdf')	
				{		
				$this->addTab('group_'.$group->getId(), array(
					'label'=> $asd,
					'content'   => $this->_translateHtml($this->getLayout()
					->createBlock('Pdf_Pdf_Block_Adminhtml_Pdf_Edit_Tab_Form')->toHtml()),
					));//Pdf_Pdf_Block_Adminhtml_Pdf_Edit_Tab_Form
				}*/
                // do not add groups without attributes
				//echo Mage::helper('catalog')->__($group->getAttributeGroupName());	
				//echo "<br>";Admintool								
                foreach ($attributes as $key => $attribute) {
                    if( !$attribute->getIsVisible() ) {
                        unset($attributes[$key]);
                    }
                }

                if (count($attributes)==0) {
                    continue;
                }

                ############################ New Tab  ##################################												
				
				if(Mage::helper('catalog')->__($group->getAttributeGroupName())=='Admintool')
				{
				$this->addTab('group_'.$group->getId(), array(
					'label'     => Mage::helper('catalog')->__($group->getAttributeGroupName()),
					//'url'      => //Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'customizer-v1/index.php', //array('_current' => false),
					//'class'     => 'ajax',
					//'content'   => $this->_newtabHtml(),
                ));
				
				}
				
				############################ New Tab end  ##################################
				
				else{
                $this->addTab('group_'.$group->getId(), array(
                    'label'     => Mage::helper('catalog')->__($group->getAttributeGroupName()),
                    'content'   => $this->_translateHtml($this->getLayout()->createBlock($this->getAttributeTabBlock(),
                        'adminhtml.catalog.product.edit.tab.attributes')->setGroup($group)
                            ->setGroupAttributes($attributes)
                            ->toHtml()),
                ));
				}
            }
			
			
			
			
			
			

            if (Mage::helper('core')->isModuleEnabled('Mage_CatalogInventory')) {
                $this->addTab('inventory', array(
                    'label'     => Mage::helper('catalog')->__('Inventory'),
                    'content'   => $this->_translateHtml($this->getLayout()
                        ->createBlock('adminhtml/catalog_product_edit_tab_inventory')->toHtml()),
                ));
            }

            /**
             * Don't display website tab for single mode
             */
            if (!Mage::app()->isSingleStoreMode()) {
                $this->addTab('websites', array(
                    'label'     => Mage::helper('catalog')->__('Websites'),
                    'content'   => $this->_translateHtml($this->getLayout()
                        ->createBlock('adminhtml/catalog_product_edit_tab_websites')->toHtml()),
                ));
            }

            $this->addTab('categories', array(
                'label'     => Mage::helper('catalog')->__('Categories'),
                'url'       => $this->getUrl('*/*/categories', array('_current' => true)),
                'class'     => 'ajax',
            ));

            $this->addTab('related', array(
                'label'     => Mage::helper('catalog')->__('Related Products'),
                'url'       => $this->getUrl('*/*/related', array('_current' => true)),
                'class'     => 'ajax',
            ));

            $this->addTab('upsell', array(
                'label'     => Mage::helper('catalog')->__('Up-sells'),
                'url'       => $this->getUrl('*/*/upsell', array('_current' => true)),
                'class'     => 'ajax',
            ));

            $this->addTab('crosssell', array(
                'label'     => Mage::helper('catalog')->__('Cross-sells'),
                'url'       => $this->getUrl('*/*/crosssell', array('_current' => true)),
                'class'     => 'ajax',
            ));

            $storeId = 0;
            if ($this->getRequest()->getParam('store')) {
                $storeId = Mage::app()->getStore($this->getRequest()->getParam('store'))->getId();
            }

            $alertPriceAllow = Mage::getStoreConfig('catalog/productalert/allow_price');
            $alertStockAllow = Mage::getStoreConfig('catalog/productalert/allow_stock');

            if (($alertPriceAllow || $alertStockAllow) && !$product->isGrouped()) {
                $this->addTab('productalert', array(
                    'label'     => Mage::helper('catalog')->__('Product Alerts'),
                    'content'   => $this->_translateHtml($this->getLayout()
                        ->createBlock('adminhtml/catalog_product_edit_tab_alerts', 'admin.alerts.products')->toHtml())
                ));
            }

            if( $this->getRequest()->getParam('id', false) ) {
                if (Mage::helper('catalog')->isModuleEnabled('Mage_Review')) {
                    if (Mage::getSingleton('admin/session')->isAllowed('admin/catalog/reviews_ratings')){
                        $this->addTab('reviews', array(
                            'label' => Mage::helper('catalog')->__('Product Reviews'),
                            'url'   => $this->getUrl('*/*/reviews', array('_current' => true)),
                            'class' => 'ajax',
                        ));
                    }
                }
                if (Mage::helper('catalog')->isModuleEnabled('Mage_Tag')) {
                    if (Mage::getSingleton('admin/session')->isAllowed('admin/catalog/tag')){
                        $this->addTab('tags', array(
                         'label'     => Mage::helper('catalog')->__('Product Tags'),
                         'url'   => $this->getUrl('*/*/tagGrid', array('_current' => true)),
                         'class' => 'ajax',
                        ));

                        $this->addTab('customers_tags', array(
                            'label'     => Mage::helper('catalog')->__('Customers Tagged Product'),
                            'url'   => $this->getUrl('*/*/tagCustomerGrid', array('_current' => true)),
                            'class' => 'ajax',
                        ));
                    }
                }

            }

            /**
             * Do not change this tab id
             * @see Mage_Adminhtml_Block_Catalog_Product_Edit_Tabs_Configurable
             * @see Mage_Bundle_Block_Adminhtml_Catalog_Product_Edit_Tabs
             */
            if (!$product->isGrouped()) {
                $this->addTab('customer_options', array(
                    'label' => Mage::helper('catalog')->__('Custom Options'),
                    'url'   => $this->getUrl('*/*/options', array('_current' => true)),
                    'class' => 'ajax',
                ));
            }

        }
        else {
            $this->addTab('set', array(
                'label'     => Mage::helper('catalog')->__('Settings'),
                'content'   => $this->_translateHtml($this->getLayout()
                    ->createBlock('adminhtml/catalog_product_edit_tab_settings')->toHtml()),
                'active'    => true
            ));
        }
        return parent::_prepareLayout();
    }

    /**
     * Retrive product object from object if not from registry
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        if (!($this->getData('product') instanceof Mage_Catalog_Model_Product)) {
            $this->setData('product', Mage::registry('product'));
        }
        return $this->getData('product');
    }

    /**
     * Getting attribute block name for tabs
     *
     * @return string
     */
    public function getAttributeTabBlock()
    {
        if (is_null(Mage::helper('adminhtml/catalog')->getAttributeTabBlock())) {
            return $this->_attributeTabBlock;
        }
        return Mage::helper('adminhtml/catalog')->getAttributeTabBlock();
    }

    public function setAttributeTabBlock($attributeTabBlock)
    {
        $this->_attributeTabBlock = $attributeTabBlock;
        return $this;
    }

    /**
     * Translate html content
     *
     * @param string $html
     * @return string
     */
    protected function _translateHtml($html)
    {
        Mage::getSingleton('core/translate_inline')->processResponseBody($html);
        return $html;
    }
	protected function _newtabHtml()
    {
			
        
		/*$product = $this->getProduct();
		
			if($product->getId())
				{ $productid = $product->getId(); }
			else { $productid = 0; }
			if($product->getAttributeId())
				{ $attributeId = $product->getAttributeId(); }
			else { $attributeId = 0; }
			if($product->getParent_id())
				{ $parent = $product->getParent_id(); }
			else { $parent = 0; }	
			if($product->design_id)
				{ $design = $product->design_id;
					if($product->getAttributeSetId() == 10)
					{ $isTemplate	= 1; }
				else{ $isTemplate	= 0; }
				}
			else { $design = 0;
					$isTemplate	= 0;
				}	
			if($product->getLayoutType())
				{ $layoutType = $product->getLayoutType();  }
			else { $layoutType = 0; }	
			if($product->getPreview())
				{ $preview = $product->getPreview();  }
			else { $preview = 0; }	
			if($product->getProduct_width())
				{ $width = $product->getProduct_width();  }
			else { $width = 0; }	
			if($product->getProduct_height())
				{ $height = $product->getProduct_height();  }
			else { $height = 0; }	
			if($product->getXtype())
				{ $xtype = $product->getXtype();  }
			else { $xtype = 'A'; }
			
			
	?>
	<script>
	function saveDesignComplete(productid,designid,preview,qty,superAttributes){
		window.location.href = "<?php echo Mage::getBaseUrl('web');?>"+"cart/?productId="+productid+"&designId="+designid+"&qty="+qty;
		console.log("Product Id: "+productid+"  DESIGN SAVED: "+designid+"     PREVIEW: "+preview+" qty:"+qty);
	}
</script>
	<?php
	
			
			
		$html = '<div id="contener">
		<iframe id="designer-iframe" scrolling="no" style="position: relative; border: 0px none; width: 100%; height: 770px; padding:10 auto;top:0px;" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'customizer-v1/index.php?ptype=0&attributeId='.$attributeId.'&product='.$productid.'&design='.$design.'&parent='.$parent.'&layoutType='.$layoutType.'&preview='.$preview.'&width='.$width.'&height='.$height.'&xtype='.$xtype.'&Utype=0&isTemplate='.$isTemplate.'"></iframe>  
				</div>';
		//$html .= $isTemplate;*/
        //return $html;
		return "AAAAA";
    }
	
	
}


