<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Catalog product controller
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Adminhtml_Catalog_ProductController extends Mage_Adminhtml_Controller_Action
{
    /**
     * The greatest value which could be stored in CatalogInventory Qty field
     */
    const MAX_QTY_VALUE = 99999999.9999;

    /**
     * Array of actions which can be processed without secret key validation
     *
     * @var array
     */
    protected $_publicActions = array('edit');

    protected function _construct()
    {
        // Define module dependent translate
        $this->setUsedModuleName('Mage_Catalog');
    }

    /**
     * Initialize product from request parameters
     *
     * @return Mage_Catalog_Model_Product
     */
    protected function _initProduct()
    {
        $this->_title($this->__('Catalog'))
             ->_title($this->__('Manage Products'));

        $productId  = (int) $this->getRequest()->getParam('id');
        $product    = Mage::getModel('catalog/product')
            ->setStoreId($this->getRequest()->getParam('store', 0));

        if (!$productId) {
            if ($setId = (int) $this->getRequest()->getParam('set')) {
                $product->setAttributeSetId($setId);
            }

            if ($typeId = $this->getRequest()->getParam('type')) {
                $product->setTypeId($typeId);
            }
        }

        $product->setData('_edit_mode', true);
        if ($productId) {
            try {
                $product->load($productId);
            } catch (Exception $e) {
                $product->setTypeId(Mage_Catalog_Model_Product_Type::DEFAULT_TYPE);
                Mage::logException($e);
            }
        }

        $attributes = $this->getRequest()->getParam('attributes');
        if ($attributes && $product->isConfigurable() &&
            (!$productId || !$product->getTypeInstance()->getUsedProductAttributeIds())) {
            $product->getTypeInstance()->setUsedProductAttributeIds(
                explode(",", base64_decode(urldecode($attributes)))
            );
        }

        // Required attributes of simple product for configurable creation
        if ($this->getRequest()->getParam('popup')
            && $requiredAttributes = $this->getRequest()->getParam('required')) {
            $requiredAttributes = explode(",", $requiredAttributes);
            foreach ($product->getAttributes() as $attribute) {
                if (in_array($attribute->getId(), $requiredAttributes)) {
                    $attribute->setIsRequired(1);
                }
            }
        }

        if ($this->getRequest()->getParam('popup')
            && $this->getRequest()->getParam('product')
            && !is_array($this->getRequest()->getParam('product'))
            && $this->getRequest()->getParam('id', false) === false) {

            $configProduct = Mage::getModel('catalog/product')
                ->setStoreId(0)
                ->load($this->getRequest()->getParam('product'))
                ->setTypeId($this->getRequest()->getParam('type'));

            /* @var $configProduct Mage_Catalog_Model_Product */
            $data = array();
            foreach ($configProduct->getTypeInstance()->getEditableAttributes() as $attribute) {

                /* @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
                if(!$attribute->getIsUnique()
                    && $attribute->getFrontend()->getInputType()!='gallery'
                    && $attribute->getAttributeCode() != 'required_options'
                    && $attribute->getAttributeCode() != 'has_options'
                    && $attribute->getAttributeCode() != $configProduct->getIdFieldName()) {
                    $data[$attribute->getAttributeCode()] = $configProduct->getData($attribute->getAttributeCode());
                }
            }

            $product->addData($data)
                ->setWebsiteIds($configProduct->getWebsiteIds());
        }

        Mage::register('product', $product);
        Mage::register('current_product', $product);
        Mage::getSingleton('cms/wysiwyg_config')->setStoreId($this->getRequest()->getParam('store'));
        return $product;
    }

    /**
     * Create serializer block for a grid
     *
     * @param string $inputName
     * @param Mage_Adminhtml_Block_Widget_Grid $gridBlock
     * @param array $productsArray
     * @return Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Ajax_Serializer
     */
    protected function _createSerializerBlock($inputName, Mage_Adminhtml_Block_Widget_Grid $gridBlock, $productsArray)
    {
        return $this->getLayout()->createBlock('adminhtml/catalog_product_edit_tab_ajax_serializer')
            ->setGridBlock($gridBlock)
            ->setProducts($productsArray)
            ->setInputElementName($inputName)
        ;
    }

    /**
     * Output specified blocks as a text list
     */
    protected function _outputBlocks()
    {
        $blocks = func_get_args();
        $output = $this->getLayout()->createBlock('adminhtml/text_list');
        foreach ($blocks as $block) {
            $output->insert($block, '', true);
        }
        $this->getResponse()->setBody($output->toHtml());
    }

    /**
     * Product list page
     */
    public function indexAction()
    {
        $this->_title($this->__('Catalog'))
             ->_title($this->__('Manage Products'));

        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Create new product page
     */
    public function newAction()
    {
        $product = $this->_initProduct();

        $this->_title($this->__('New Product'));

        Mage::dispatchEvent('catalog_product_new_action', array('product' => $product));

        if ($this->getRequest()->getParam('popup')) {
            $this->loadLayout('popup');
        } else {
            $_additionalLayoutPart = '';
            if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE
                && !($product->getTypeInstance()->getUsedProductAttributeIds()))
            {
                $_additionalLayoutPart = '_new';
            }
            $this->loadLayout(array(
                'default',
                strtolower($this->getFullActionName()),
                'adminhtml_catalog_product_'.$product->getTypeId() . $_additionalLayoutPart
            ));
            $this->_setActiveMenu('catalog/products');
        }

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $block = $this->getLayout()->getBlock('catalog.wysiwyg.js');
        if ($block) {
            $block->setStoreId($product->getStoreId());
        }

        $this->renderLayout();
    }

    /**
     * Product edit form
     */
    public function editAction()
    {
        $productId  = (int) $this->getRequest()->getParam('id');
        $product = $this->_initProduct();

        if ($productId && !$product->getId()) {
            $this->_getSession()->addError(Mage::helper('catalog')->__('This product no longer exists.'));
            $this->_redirect('*/*/');
            return;
        }

        $this->_title($product->getName());

        Mage::dispatchEvent('catalog_product_edit_action', array('product' => $product));

        $_additionalLayoutPart = '';
        if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE
            && !($product->getTypeInstance()->getUsedProductAttributeIds()))
        {
            $_additionalLayoutPart = '_new';
        }

        $this->loadLayout(array(
            'default',
            strtolower($this->getFullActionName()),
            'adminhtml_catalog_product_'.$product->getTypeId() . $_additionalLayoutPart
        ));

        $this->_setActiveMenu('catalog/products');

        if (!Mage::app()->isSingleStoreMode() && ($switchBlock = $this->getLayout()->getBlock('store_switcher'))) {
            $switchBlock->setDefaultStoreName($this->__('Default Values'))
                ->setWebsiteIds($product->getWebsiteIds())
                ->setSwitchUrl(
                    $this->getUrl('*/*/*', array('_current'=>true, 'active_tab'=>null, 'tab' => null, 'store'=>null))
                );
        }

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $block = $this->getLayout()->getBlock('catalog.wysiwyg.js');
        if ($block) {
            $block->setStoreId($product->getStoreId());
        }

        $this->renderLayout();
    }

    /**
     * WYSIWYG editor action for ajax request
     *
     */
    public function wysiwygAction()
    {
        $elementId = $this->getRequest()->getParam('element_id', md5(microtime()));
        $storeId = $this->getRequest()->getParam('store_id', 0);
        $storeMediaUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);

        $content = $this->getLayout()->createBlock('adminhtml/catalog_helper_form_wysiwyg_content', '', array(
            'editor_element_id' => $elementId,
            'store_id'          => $storeId,
            'store_media_url'   => $storeMediaUrl,
        ));
        $this->getResponse()->setBody($content->toHtml());
    }

    /**
     * Product grid for AJAX request
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Get specified tab grid
     */
    public function gridOnlyAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()
                ->createBlock('adminhtml/catalog_product_edit_tab_' . $this->getRequest()->getParam('gridOnlyBlock'))
                ->toHtml()
        );
    }

    /**
     * Get categories fieldset block
     *
     */
    public function categoriesAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Get options fieldset block
     *
     */
    public function optionsAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Get related products grid and serializer block
     */
    public function relatedAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.related')
            ->setProductsRelated($this->getRequest()->getPost('products_related', null));
        $this->renderLayout();
    }

    /**
     * Get upsell products grid and serializer block
     */
    public function upsellAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.upsell')
            ->setProductsUpsell($this->getRequest()->getPost('products_upsell', null));
        $this->renderLayout();
    }

    /**
     * Get crosssell products grid and serializer block
     */
    public function crosssellAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.crosssell')
            ->setProductsCrossSell($this->getRequest()->getPost('products_crosssell', null));
        $this->renderLayout();
    }

    /**
     * Get related products grid
     */
    public function relatedGridAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.related')
            ->setProductsRelated($this->getRequest()->getPost('products_related', null));
        $this->renderLayout();
    }

    /**
     * Get upsell products grid
     */
    public function upsellGridAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.upsell')
            ->setProductsRelated($this->getRequest()->getPost('products_upsell', null));
        $this->renderLayout();
    }

    /**
     * Get crosssell products grid
     */
    public function crosssellGridAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.crosssell')
            ->setProductsRelated($this->getRequest()->getPost('products_crosssell', null));
        $this->renderLayout();
    }

    /**
     * Get associated grouped products grid and serializer block
     */
    public function superGroupAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.super.group')
            ->setProductsGrouped($this->getRequest()->getPost('products_grouped', null));
        $this->renderLayout();
    }

    /**
     * Get associated grouped products grid only
     *
     */
    public function superGroupGridOnlyAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.super.group')
            ->setProductsGrouped($this->getRequest()->getPost('products_grouped', null));
        $this->renderLayout();
    }

    /**
     * Get product reviews grid
     *
     */
    public function reviewsAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('admin.product.reviews')
                ->setProductId(Mage::registry('product')->getId())
                ->setUseAjax(true);
        $this->renderLayout();
    }

    /**
     * Get super config grid
     *
     */
    public function superConfigAction()
    {
        $this->_initProduct();
        $this->loadLayout(false);
        $this->renderLayout();
    }

    /**
     * Deprecated since 1.2
     *
     */
    public function bundlesAction()
    {
        $product = $this->_initProduct();
        $this->getResponse()->setBody(
            $this->getLayout()
                ->createBlock('bundle/adminhtml_catalog_product_edit_tab_bundle', 'admin.product.bundle.items')
                ->setProductId($product->getId())
                ->toHtml()
        );
    }

    /**
     * Validate product
     *
     */
    public function validateAction()
    {
        $response = new Varien_Object();
        $response->setError(false);

        try {
            $productData = $this->getRequest()->getPost('product');

            if ($productData && !isset($productData['stock_data']['use_config_manage_stock'])) {
                $productData['stock_data']['use_config_manage_stock'] = 0;
            }
            /* @var $product Mage_Catalog_Model_Product */
            $product = Mage::getModel('catalog/product');
            $product->setData('_edit_mode', true);
            if ($storeId = $this->getRequest()->getParam('store')) {
                $product->setStoreId($storeId);
            }
            if ($setId = $this->getRequest()->getParam('set')) {
                $product->setAttributeSetId($setId);
            }
            if ($typeId = $this->getRequest()->getParam('type')) {
                $product->setTypeId($typeId);
            }
            if ($productId = $this->getRequest()->getParam('id')) {
                $product->load($productId);
            }

            $dateFields = array();
            $attributes = $product->getAttributes();
            foreach ($attributes as $attrKey => $attribute) {
                if ($attribute->getBackend()->getType() == 'datetime') {
                    if (array_key_exists($attrKey, $productData) && $productData[$attrKey] != ''){
                        $dateFields[] = $attrKey;
                    }
                }
            }
            $productData = $this->_filterDates($productData, $dateFields);

            $product->addData($productData);
            $product->validate();
            /**
             * @todo implement full validation process with errors returning which are ignoring now
             */
//            if (is_array($errors = $product->validate())) {
//                foreach ($errors as $code => $error) {
//                    if ($error === true) {
//                        Mage::throwException(Mage::helper('catalog')->__('Attribute "%s" is invalid.', $product->getResource()->getAttribute($code)->getFrontend()->getLabel()));
//                    }
//                    else {
//                        Mage::throwException($error);
//                    }
//                }
//            }
        }
        catch (Mage_Eav_Model_Entity_Attribute_Exception $e) {
            $response->setError(true);
            $response->setAttribute($e->getAttributeCode());
            $response->setMessage($e->getMessage());
        } catch (Mage_Core_Exception $e) {
            $response->setError(true);
            $response->setMessage($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
            $this->_initLayoutMessages('adminhtml/session');
            $response->setError(true);
            $response->setMessage($this->getLayout()->getMessagesBlock()->getGroupedHtml());
        }

        $this->getResponse()->setBody($response->toJson());
    }

    /**
     * Initialize product before saving
     */
    protected function _initProductSave()
    {
        $product     = $this->_initProduct();
        $productData = $this->getRequest()->getPost('product');
        if ($productData) {
            $this->_filterStockData($productData['stock_data']);
        }

        /**
         * Websites
         */
        if (!isset($productData['website_ids'])) {
            $productData['website_ids'] = array();
        }

        $wasLockedMedia = false;
        if ($product->isLockedAttribute('media')) {
            $product->unlockAttribute('media');
            $wasLockedMedia = true;
        }

        $product->addData($productData);

        if ($wasLockedMedia) {
            $product->lockAttribute('media');
        }

        if (Mage::app()->isSingleStoreMode()) {
            $product->setWebsiteIds(array(Mage::app()->getStore(true)->getWebsite()->getId()));
        }

        /**
         * Create Permanent Redirect for old URL key
         */
        if ($product->getId() && isset($productData['url_key_create_redirect']))
        // && $product->getOrigData('url_key') != $product->getData('url_key')
        {
            $product->setData('save_rewrites_history', (bool)$productData['url_key_create_redirect']);
        }

        /**
         * Check "Use Default Value" checkboxes values
         */
        if ($useDefaults = $this->getRequest()->getPost('use_default')) {
            foreach ($useDefaults as $attributeCode) {
                $product->setData($attributeCode, false);
            }
        }

        /**
         * Init product links data (related, upsell, crosssel)
         */
        $links = $this->getRequest()->getPost('links');
        if (isset($links['related']) && !$product->getRelatedReadonly()) {
            $product->setRelatedLinkData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($links['related']));
        }
        if (isset($links['upsell']) && !$product->getUpsellReadonly()) {
            $product->setUpSellLinkData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($links['upsell']));
        }
        if (isset($links['crosssell']) && !$product->getCrosssellReadonly()) {
            $product->setCrossSellLinkData(Mage::helper('adminhtml/js')
                ->decodeGridSerializedInput($links['crosssell']));
        }
        if (isset($links['grouped']) && !$product->getGroupedReadonly()) {
            $product->setGroupedLinkData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($links['grouped']));
        }

        /**
         * Initialize product categories
         */
        $categoryIds = $this->getRequest()->getPost('category_ids');
        if (null !== $categoryIds) {
            if (empty($categoryIds)) {
                $categoryIds = array();
            }
            $product->setCategoryIds($categoryIds);
        }

        /**
         * Initialize data for configurable product
         */
        if (($data = $this->getRequest()->getPost('configurable_products_data'))
            && !$product->getConfigurableReadonly()
        ) {
            $product->setConfigurableProductsData(Mage::helper('core')->jsonDecode($data));
        }
        if (($data = $this->getRequest()->getPost('configurable_attributes_data'))
            && !$product->getConfigurableReadonly()
        ) {
            $product->setConfigurableAttributesData(Mage::helper('core')->jsonDecode($data));
        }

        $product->setCanSaveConfigurableAttributes(
            (bool) $this->getRequest()->getPost('affect_configurable_product_attributes')
                && !$product->getConfigurableReadonly()
        );

        /**
         * Initialize product options
         */
        if (isset($productData['options']) && !$product->getOptionsReadonly()) {
            $product->setProductOptions($productData['options']);
        }

        $product->setCanSaveCustomOptions(
            (bool)$this->getRequest()->getPost('affect_product_custom_options')
            && !$product->getOptionsReadonly()
        );

        Mage::dispatchEvent(
            'catalog_product_prepare_save',
            array('product' => $product, 'request' => $this->getRequest())
        );

        return $product;
    }

    /**
     * Filter product stock data
     *
     * @param array $stockData
     */
    protected function _filterStockData(&$stockData) {
        if (!isset($stockData['use_config_manage_stock'])) {
            $stockData['use_config_manage_stock'] = 0;
        }
        if (isset($stockData['qty']) && (float)$stockData['qty'] > self::MAX_QTY_VALUE) {
            $stockData['qty'] = self::MAX_QTY_VALUE;
        }
        if (isset($stockData['min_qty']) && (int)$stockData['min_qty'] < 0) {
            $stockData['min_qty'] = 0;
        }
        if (!isset($stockData['is_decimal_divided']) || $stockData['is_qty_decimal'] == 0) {
            $stockData['is_decimal_divided'] = 0;
        }
    }

    public function categoriesJsonAction()
    {
        $product = $this->_initProduct();

        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('adminhtml/catalog_product_edit_tab_categories')
                ->getCategoryChildrenJson($this->getRequest()->getParam('category'))
        );
    }

    /**
     * Save product action
     */
    public function saveAction()
    {
        $storeId        = $this->getRequest()->getParam('store');
        $redirectBack   = $this->getRequest()->getParam('back', false);
        $productId      = $this->getRequest()->getParam('id');
        $isEdit         = (int)($this->getRequest()->getParam('id') != null);

        $data = $this->getRequest()->getPost();
        if ($data) {
            $this->_filterStockData($data['product']['stock_data']);

            $product = $this->_initProductSave();

            try {
                $product->save();
                $productId = $product->getId();

                /**
                 * Do copying data to stores
                 */
                if (isset($data['copy_to_stores'])) {
                    foreach ($data['copy_to_stores'] as $storeTo=>$storeFrom) {
                        $newProduct = Mage::getModel('catalog/product')
                            ->setStoreId($storeFrom)
                            ->load($productId)
                            ->setStoreId($storeTo)
                            ->save();
                    }
                }
				
				
				/////////////Call method to create designerTool file////////////
					if($product->getAttributeSetId() == 9 || $product->getAttributeSetId()==11 || $product->getAttributeSetId()==12){
							$this->generateJsnFILE($product);
					}else if($product->getAttributeSetId() == 10){
						$this->generateTemplateThumbnail($product);
					}else if($product->getAttributeSetId() == 13){
						/*   FOLDED PRODUCTS   */
						$this->generateFoldedProductFILE($product);
					}
					
					if($_FILES['pdf']['name']){
						$fpdf = Array();
						$fpdf = $_FILES['pdf'];
						$this->saveuploadpdf($fpdf,$product);
					}
			/*$fileData=$product->getAttributeSetId();
			$fileName = 'customizer-v1/xml/products/'.$product->getId().'.txt';
			
			//echo 'baseUrl'.$baseUrl.'-------------'.$fileData.'ddddddddddd'.$fileName; die('Please Wait...........');
			$fp = fopen($fileName, 'w');
			fwrite($fp, $fileData);
			fclose($fp);die('Wait..');*/
					
					
				/////////////////////////////////////////////////////////////// 
				
				Mage::getModel('catalogrule/rule')->applyAllRulesToProduct($productId);
                

                $this->_getSession()->addSuccess($this->__('The product has been saved.'));
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage())
                    ->setProductData($data);
                $redirectBack = true;
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage());
                $redirectBack = true;
            }
        }

        if ($redirectBack) {
            $this->_redirect('*/*/edit', array(
                'id'    => $productId,
                '_current'=>true
            ));
        } elseif($this->getRequest()->getParam('popup')) {
            $this->_redirect('*/*/created', array(
                '_current'   => true,
                'id'         => $productId,
                'edit'       => $isEdit
            ));
        } else {
            $this->_redirect('*/*/', array('store'=>$storeId));
        }
    }

    /**
     * Create product duplicate
     */
    public function duplicateAction()
    {
        $product = $this->_initProduct();
        try {
            $newProduct = $product->duplicate();
            $this->_getSession()->addSuccess($this->__('The product has been duplicated.'));
            $this->_redirect('*/*/edit', array('_current'=>true, 'id'=>$newProduct->getId()));
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
            $this->_redirect('*/*/edit', array('_current'=>true));
        }
    }

    /**
     * @deprecated since 1.4.0.0-alpha2
     */
    protected function _decodeInput($encoded)
    {
        parse_str($encoded, $data);
        foreach($data as $key=>$value) {
            parse_str(base64_decode($value), $data[$key]);
        }
        return $data;
    }

    /**
     * Delete product action
     */
    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            $product = Mage::getModel('catalog/product')
                ->load($id);
            $sku = $product->getSku();
            try {
                $product->delete();
                $this->_getSession()->addSuccess($this->__('The product has been deleted.'));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->getResponse()
            ->setRedirect($this->getUrl('*/*/', array('store'=>$this->getRequest()->getParam('store'))));
    }

    /**
     * Get tag grid
     */
    public function tagGridAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('admin.product.tags')
            ->setProductId($this->getRequest()->getParam('id'));
        $this->renderLayout();
    }

    /**
     * Get alerts price grid
     */
    public function alertsPriceGridAction()
    {
        $this->loadLayout(false);
        $this->renderLayout();
    }

    /**
     * Get alerts stock grid
     */
    public function alertsStockGridAction()
    {
        $this->loadLayout(false);
        $this->renderLayout();
    }

    /**
     * @deprecated since 1.5.0.0
     * @return Mage_Adminhtml_Catalog_ProductController
     */
    public function addCustomersToAlertQueueAction()
    {
        return $this;
    }

    public function addAttributeAction()
    {
        $this->_getSession()->addNotice(
            Mage::helper('catalog')->__('Please click on the Close Window button if it is not closed automatically.')
        );
        $this->loadLayout('popup');
        $this->_initProduct();
        $this->_addContent(
            $this->getLayout()->createBlock('adminhtml/catalog_product_attribute_new_product_created')
        );
        $this->renderLayout();
    }

    public function createdAction()
    {
        $this->_getSession()->addNotice(
            Mage::helper('catalog')->__('Please click on the Close Window button if it is not closed automatically.')
        );
        $this->loadLayout('popup');
        $this->_addContent(
            $this->getLayout()->createBlock('adminhtml/catalog_product_created')
        );
        $this->renderLayout();
    }

    public function massDeleteAction()
    {
        $productIds = $this->getRequest()->getParam('product');
        if (!is_array($productIds)) {
            $this->_getSession()->addError($this->__('Please select product(s).'));
        } else {
            if (!empty($productIds)) {
                try {
                    foreach ($productIds as $productId) {
                        $product = Mage::getSingleton('catalog/product')->load($productId);
                        Mage::dispatchEvent('catalog_controller_product_delete', array('product' => $product));
                        $product->delete();
                    }
                    $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) have been deleted.', count($productIds))
                    );
                } catch (Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                }
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * Update product(s) status action
     *
     */
    public function massStatusAction()
    {
        $productIds = (array)$this->getRequest()->getParam('product');
        $storeId    = (int)$this->getRequest()->getParam('store', 0);
        $status     = (int)$this->getRequest()->getParam('status');

        try {
            $this->_validateMassStatus($productIds, $status);
            Mage::getSingleton('catalog/product_action')
                ->updateAttributes($productIds, array('status' => $status), $storeId);

            $this->_getSession()->addSuccess(
                $this->__('Total of %d record(s) have been updated.', count($productIds))
            );
        }
        catch (Mage_Core_Model_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()
                ->addException($e, $this->__('An error occurred while updating the product(s) status.'));
        }

        $this->_redirect('*/*/', array('store'=> $storeId));
    }

    /**
     * Validate batch of products before theirs status will be set
     *
     * @throws Mage_Core_Exception
     * @param  array $productIds
     * @param  int $status
     * @return void
     */
    public function _validateMassStatus(array $productIds, $status)
    {
        if ($status == Mage_Catalog_Model_Product_Status::STATUS_ENABLED) {
            if (!Mage::getModel('catalog/product')->isProductsHasSku($productIds)) {
                throw new Mage_Core_Exception(
                    $this->__('Some of the processed products have no SKU value defined. Please fill it prior to performing operations on these products.')
                );
            }
        }
    }

    /**
     * Get tag customer grid
     *
     */
    public function tagCustomerGridAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('admin.product.tags.customers')
                ->setProductId($this->getRequest()->getParam('id'));
        $this->renderLayout();
    }

    public function quickCreateAction()
    {
        $result = array();

        /* @var $configurableProduct Mage_Catalog_Model_Product */
        $configurableProduct = Mage::getModel('catalog/product')
            ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)
            ->load($this->getRequest()->getParam('product'));

        if (!$configurableProduct->isConfigurable()) {
            // If invalid parent product
            $this->_redirect('*/*/');
            return;
        }

        /* @var $product Mage_Catalog_Model_Product */

        $product = Mage::getModel('catalog/product')
            ->setStoreId(0)
            ->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE)
            ->setAttributeSetId($configurableProduct->getAttributeSetId());


        foreach ($product->getTypeInstance()->getEditableAttributes() as $attribute) {
            if ($attribute->getIsUnique()
                || $attribute->getAttributeCode() == 'url_key'
                || $attribute->getFrontend()->getInputType() == 'gallery'
                || $attribute->getFrontend()->getInputType() == 'media_image'
                || !$attribute->getIsVisible()) {
                continue;
            }

            $product->setData(
                $attribute->getAttributeCode(),
                $configurableProduct->getData($attribute->getAttributeCode())
            );
        }

        $product->addData($this->getRequest()->getParam('simple_product', array()));
        $product->setWebsiteIds($configurableProduct->getWebsiteIds());

        $autogenerateOptions = array();
        $result['attributes'] = array();

        foreach ($configurableProduct->getTypeInstance()->getConfigurableAttributes() as $attribute) {
            $value = $product->getAttributeText($attribute->getProductAttribute()->getAttributeCode());
            $autogenerateOptions[] = $value;
            $result['attributes'][] = array(
                'label'         => $value,
                'value_index'   => $product->getData($attribute->getProductAttribute()->getAttributeCode()),
                'attribute_id'  => $attribute->getProductAttribute()->getId()
            );
        }

        if ($product->getNameAutogenerate()) {
            $product->setName($configurableProduct->getName() . '-' . implode('-', $autogenerateOptions));
        }

        if ($product->getSkuAutogenerate()) {
            $product->setSku($configurableProduct->getSku() . '-' . implode('-', $autogenerateOptions));
        }

        if (is_array($product->getPricing())) {
           $result['pricing'] = $product->getPricing();
           $additionalPrice = 0;
           foreach ($product->getPricing() as $pricing) {
               if (empty($pricing['value'])) {
                   continue;
               }

               if (!empty($pricing['is_percent'])) {
                   $pricing['value'] = ($pricing['value']/100)*$product->getPrice();
               }

               $additionalPrice += $pricing['value'];
           }

           $product->setPrice($product->getPrice() + $additionalPrice);
           $product->unsPricing();
        }

        try {
            /**
             * @todo implement full validation process with errors returning which are ignoring now
             */
//            if (is_array($errors = $product->validate())) {
//                $strErrors = array();
//                foreach($errors as $code=>$error) {
//                    $codeLabel = $product->getResource()->getAttribute($code)->getFrontend()->getLabel();
//                    $strErrors[] = ($error === true)? Mage::helper('catalog')->__('Value for "%s" is invalid.', $codeLabel) : Mage::helper('catalog')->__('Value for "%s" is invalid: %s', $codeLabel, $error);
//                }
//                Mage::throwException('data_invalid', implode("\n", $strErrors));
//            }

            $product->validate();
            $product->save();
            $result['product_id'] = $product->getId();
            $this->_getSession()->addSuccess(Mage::helper('catalog')->__('The product has been created.'));
            $this->_initLayoutMessages('adminhtml/session');
            $result['messages']  = $this->getLayout()->getMessagesBlock()->getGroupedHtml();
        } catch (Mage_Core_Exception $e) {
            $result['error'] = array(
                'message' =>  $e->getMessage(),
                'fields'  => array(
                    'sku'  =>  $product->getSku()
                )
            );

        } catch (Exception $e) {
            Mage::logException($e);
            $result['error'] = array(
                'message'   =>  $this->__('An error occurred while saving the product. ') . $e->getMessage()
             );
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * Check for is allowed
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('catalog/products');
    }

    /**
     * Show item update result from updateAction
     * in Wishlist and Cart controllers.
     *
     */
    public function showUpdateResultAction()
    {
        $session = Mage::getSingleton('adminhtml/session');
        if ($session->hasCompositeProductResult() && $session->getCompositeProductResult() instanceof Varien_Object){
            /* @var $helper Mage_Adminhtml_Helper_Catalog_Product_Composite */
            $helper = Mage::helper('adminhtml/catalog_product_composite');
            $helper->renderUpdateResult($this, $session->getCompositeProductResult());
            $session->unsCompositeProductResult();
        } else {
            $session->unsCompositeProductResult();
            return false;
        }
    }
	public function generateTemplateThumbnail($oproduct){
		
			$previewImage = Mage::getBaseDir()."/customizer-v1/images/templates/".$oproduct->design_id."_1_PREVIEW.png";
			
			if(file_exists($previewImage)){
				$replaced = false;
				
				if($oproduct->replace_thumbnail){
					$oriproattributes = $oproduct->getTypeInstance()->getSetAttributes();
					if (isset($oriproattributes['media_gallery'])) {
						$gallery = $oriproattributes['media_gallery'];
						//Get the images
						$galleryData = $oproduct->getMediaGallery();
						foreach($galleryData['images'] as $image){
							//If image exists
							$arr = explode('_',$image['file']);
							if(count($arr)>2){
								$flag = explode('.',$arr[2]);
								
								if ($gallery->getBackend()->getImage($oproduct, $image['file']) && $flag[0]=="PREVIEW") {
									$gallery->getBackend()->removeImage($oproduct, $image['file']);
									$replaced = true;
								}
							}
						}
					}
				}
				if($replaced || $oproduct->replace_thumbnail){
					$oproduct->setData('replace_thumbnail',0);
					$oproduct->save();
				
					$oproduct->setMediaGallery (array('images'=>array (), 'values'=>array ()));
					$oproduct->addImageToMediaGallery ($previewImage, array ('thumbnail','small_image','image'), false, false);
					$oproduct->save();
				}
			}
	}
	public function generateJsnFILE($oproduct){
		$magick = "/usr/bin/convert";
		$side = '';
		
		
		$summaryData = Mage::getModel('review/review_summary')->load($oproduct->getId());
						$reviewsCount = $summaryData->getReviewsCount();
						if($summaryData->getRatingSummary()){
						$ratingSummary = $summaryData->getRatingSummary(); 
						}
						else{
						$ratingSummary = 0; 
						}
						
		$attributes = $oproduct->getAttributes();
		
		$fileData='{';
			$fileData.='"id":'.$oproduct->getId().',';
			$fileData.='"title":'.'"'.$oproduct->name.'",';
			$fileData.='"rating":"'.$ratingSummary.'",';
			//$fileData.='"desc":'.'"'.$oproduct->getShort_description().'",';
			$fileData.='"width":7,';
			$fileData.='"height":7,';
			$fileData.='"margin":'.$oproduct->product_margin.',';
			if($oproduct->getAttributeSetId() == 9 || $oproduct->getAttributeSetId() == 11 || $oproduct->getAttributeSetId() == 12){
			$fileData.='"type":"'.$attributes['product_measurement_unit']->getFrontend()->getValue($oproduct).'",';
			}
			if($oproduct->getStockItem()){
				if($oproduct->getStockItem()->getMinSaleQty()){
					$fileData.='"minQty":'.$oproduct->getStockItem()->getMinSaleQty() .',';
				}else{
					$fileData.='"minQty":1,';
				}
				if($oproduct->getStockItem()->getMaxSaleQty()){
					$fileData.='"maxQty":'.$oproduct->getStockItem()->getMaxSaleQty() .',';
				}else{
					$fileData.='"maxQty":0,';
				}
			}else{
				$fileData.='"minQty":1,';
				$fileData.='"maxQty":0,';
			}
			
			//Tier Price Begin
			if($oproduct->tier_price){
				$tier_price=$oproduct->tier_price;
				$fileData.='"tier":[';
				$countTp=count($tier_price);
				$tp=1;
				foreach($tier_price as $tprice ){
					$fileData.='{"qty":'.$tprice['price_qty'].', "price":'.$tprice['price'].'}';
					$fileData.= $countTp > $tp ? ',' : '';
					$tp++;
				}
			$fileData.='],';
			}else{
			$fileData.='"tier":[],';
			}
			//Tier Price End
			
			if($oproduct->getAttributeSetId() == 9 || $oproduct->getAttributeSetId() == 11 || $oproduct->getAttributeSetId() == 12){
				$previewLayout = $attributes['preview_layout']->getFrontend()->getValue($oproduct);
				$preview=$attributes['preview_position']->getFrontend()->getValue($oproduct);
				$fileData.= '"position":"'.$preview.'",'; //preview position up/down
				$fileData.= '"layout":"'.$previewLayout.'",';
			}
			$product_price = $oproduct->price ? $oproduct->price : '0';
			$fileData.='"price":'.$product_price.',';
			if($oproduct->parent_id>0){
				$product = Mage::getModel("catalog/product")->load($oproduct->parent_id);
				$attributes = $product->getAttributes();
			}else{
				$product = $oproduct;
			}
			if($oproduct->getAttributeSetId() == 9 || $oproduct->getAttributeSetId() == 11 || $oproduct->getAttributeSetId() == 12){
				if($oproduct->getAttributeSetId() == 12){
					$numberOfViews = 1;
				}else{
					$numberOfViews = $attributes['no_of_views']->getFrontend()->getValue($product);
				}
				if($numberOfViews !=  ''){
					$fileData.='"view":1,';
					$fileData.='"views":';
					$fileData.='[';
					for($i=1; $i<=$numberOfViews; $i++){					
					//	$view_flag = 'view_'.$i.'_flag';
						$view_xywh = 'view_'.$i.'_xywh';
						$view_print_wh = 'view_'.$i.'_print_wh';
						$view_label = 'view_'.$i.'_label';
						$view_tool_image = 'view_'.$i.'_tool_image';
						
					//	$source = Mage::getBaseDir('media').'/catalog/product'.$attributes[$view_tool_image]->getFrontend()->getValue($product);
					//	$extension = substr($source, strrpos($source, '.'));
						
						
					//	$thumbnail = Mage::getBaseDir()."/customizer-v1/images/products/".$product->getId().'_'.$product->$view_label."_small".$extension;
					//	$regular = Mage::getBaseDir()."/customizer-v1/images/products/".$product->getId().'_'.$product->$view_label."_regular".$extension;
						
					//	exec("$magick $source -thumbnail 400x400  $regular");
					//	exec("$magick $source -thumbnail 65x65  $thumbnail");
						
					//	if($product->$view_flag){
							$areaWH = explode(",",$product->$view_print_wh);
							$W = $areaWH[0];
							$H = $areaWH[1];
							$fileData.='{';
								$fileData.='"view":'.$i.',';
								$fileData.='"width":'.$W.',';
								$fileData.='"height":'.$H.',';
								$fileData.='"title":"'.ucfirst($product->$view_label).'",';
								
								$fileData.='"thumbnail":"'.'images/products/'.$product->getId().'_'.$product->$view_label."_small".$extension.'",';
								$fileData.='"mask":"",';
								$fileData.='"vsplit":0,';
								$fileData.='"hsplit":1,';
								
								$fileData.='"data":[],';
								$fileData.='"options":[]';
								
								
								
							if($i==$numberOfViews){
								$fileData.='}]';
							}else{
								$fileData.='},';
							}
					//	}
					}
				}
			}
			$fileData .= ',"preview":';
			//Preview Section Begin
			$numberOfPreview = $attributes['no_of_preview_sides']->getFrontend()->getValue($product);
			if($numberOfPreview){ //&& $numberOfViews ==1
				$side ='[';
				$ctr=0;
				for($i=1; $i<=$numberOfPreview; $i++){
					if($oproduct->getAttributeSetId() != 12){
						$view_code = $i;
					}else{
						$view_code = 1;
					}
					$imgpreview_side='preview_side_'.$i;
					$preview_positions='preview_'.$i.'_positions';
					$source1 = Mage::getBaseDir('media').'/catalog/product'.$attributes[$imgpreview_side]->getFrontend()->getValue($product);										 
					$extensionpreview = substr($source1, strrpos($source1, '.'));
					$thumbnail1 = Mage::getBaseDir()."/customizer-v1/images/products/".$product->getId().'_preview_'.$i.$extensionpreview;
					
					copy($source1,$thumbnail1);
					//exec("$magick $source1 -thumbnail 65x65  $thumbnail1");
					
					if($ctr>0){
						$side .=  ',';
					}
					$ctr++;
					
					if($attributes['preview_arc_down']){
						$arcdown = '"arcdown":'.$oproduct->preview_arc_down.',';
					}else{
						$arcdown = '';
					}
					$side .= '{"view":'.$view_code.',"image":"'.$product->getId().'_preview_'.$i.$extensionpreview.'",'.$arcdown.'"position":['.$attributes[$preview_positions]->getFrontend()->getValue($product).']}';
				}
				$side.=']';
			}
			//Preview Section End
			$fileData .= $side;
		//	echo '</pre>';
		//	print_r($product->hasOptions());
		//	die;
			$fileData.=',"options":[';
			if($product->hasOptions()) {
					$j=1;
					$ctr1 = 0;
					$ctr2 = 0;
					foreach ($product->getOptions() as $o) {
						$values = $o->getValues();
						if($o->getType()=="radio" || $o->getType()=="checkbox"){
							if($ctr1>0){
								$fileData.=',';
							}
							$ctr1 = 1;
							
							$fileData.='{"id":"'.$o->getId().'","type":"label","value":"'.$o->getTitle().'","fields":[';
							
							$ctr = 0;
							foreach ($values as $v) {
								if($ctr>0){
									$fileData.=',';
								}
								
								$ctr = 1;
								
								if($v->getData(price_type)=="percent"){								
									$optionPrice= $product->price ? $product->price*$v->getData(price)/100 : '0';								
								}else{
									$optionPrice=$v->getData(price) ? $v->getData(price) : '0';
								}
								$fileData.='{"id":"'.$v->getData('option_type_id').'","type":"'.$o->getType().'","group":"group'.$j.'","value":'.$optionPrice.',"label":"'.$v->getData(title).'"}';
							}
							$fileData.=']}';
						}
						if($o->getType()=="drop_down"){
							if($ctr2>0){
								$fileData.=',';
							}
							$ctr2 = 1;
							$fileData.='{"id":"'.$o->getId().'","type":"label","value":"'.$o->getTitle().'","fields":[';
							$fileData.='{"id":"select1","type":"select","OPTIONS":[';
							$ctr = 0;
							foreach ($values as $v) {
								if($ctr>0){
									$fileData.=',';
								}
								$ctr = 1;
								if($v->getData(price_type)=="percent"){
									$optionPrice=$product->price ? $product->price*$v->getData(price)/100 : '0';								
								}else{
									$optionPrice=$v->getData(price) ? $v->getData(price) : '0';
								}
								
								$fileData.='{"id":"'.$v->getData('option_type_id').'","op":"'.$v->getData(title).'","val":'.$optionPrice.'}';
							}
							$fileData.=']}]}';
						}
						$j++;
					}
					//substr($fileData, 0, -1);
			}
			$fileData.=']';
			
			
			if($product->getTypeId() == "configurable"){
				$fileData.=',"attributes":{';
				$associatedProducts=$product->getTypeInstance()->getUsedProducts();
				
				$fileData.='"configuration":[';
				
				$ctr = 0;
				$configurableAttributeCollection = $product->getTypeInstance()->getConfigurableAttributes();
				foreach($configurableAttributeCollection as $attribute){
					if($ctr>0){
						$fileData.=',';
					}
					$ctr = 1;
				
					$attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($attribute->getProductAttribute()->getAttributeCode())->getFirstItem()->setEntity($product->getResource());
					$attributeOptions = $attributeInfo->getSource()->getAllOptions(false);
					$fileData.='{"id":'.$attributeInfo->getAttributeId().',"code":"'.$attributeInfo->getAttributeCode().'","label":"'.$attributeInfo->getFrontend()->getLabel($product).'","options":[';
					
					
					$ctr2 = 0;
					foreach($attributeOptions as $attributeOption){
						foreach($associatedProducts as $associatedProduct){
							if($associatedProduct[$attributeInfo->getAttributeCode()] == $attributeOption['value']){
								$visible = '1';
							}
						//	$productArr .= $associatedProduct[$attributeInfo->getAttributeCode()].' == '.$attributeOption['value'];//$associatedProduct->getId();
						}
						if($visible>0){
							if($ctr2>0){
								$fileData.=',';
							}
							$ctr2 = 1;
							$fileData.='{"id":'.$attributeOption['value'].',"label":"'. $attributeOption['label'].'","price":0}';
						}
					}
					$fileData .=']}';
				}
				$fileData.='],"combinations":[';
				
				$combinations = "";
				$ctr = 0;
				foreach($associatedProducts as $associatedProduct){
					if($ctr>0){
						$combinations .= ',';
					}
					$ctr = 1;
					$combination = '{"ids":[';
					
					$ctr2 = 0;
					foreach($configurableAttributeCollection as $attribute){
						$attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($attribute->getProductAttribute()->getAttributeCode())->getFirstItem()->setEntity($product->getResource());
					//	foreach($attributeOptions as $attributeOption){
					//	if($associatedProduct[$attributeInfo->getAttributeCode()] == $attributeOption['value']){
							if($ctr2>0){
								$combination .= ',';
							}
							$ctr2 = 1;
							$combination .= $associatedProduct[$attributeInfo->getAttributeCode()];	
					//	}
					}
					$combination .= '],"product":'.$associatedProduct->getId().',"price":'.$associatedProduct->getPrice().'}';
					$combinations .= $combination;
				//	$productArr .= $associatedProduct[$attributeInfo->getAttributeCode()].' == '.$attributeOption['value'];//$associatedProduct->getId();
				}
				$fileData .= $combinations;
				$fileData .= ']';
				$fileData.='}';
			}
			
			$fileData.='}'; 
			
			//$baseUrl = Mage::getBaseUrl('web');
			
			
			$fileName = 'customizer-v1/xml/products/'.$oproduct->getId().'.txt';
			
			//echo 'baseUrl'.$baseUrl.'-------------'.$fileData.'ddddddddddd'.$fileName; die('Please Wait...........');
			$fp = fopen($fileName, 'w');
			fwrite($fp, $fileData);
			fclose($fp);
			unset($fileName);
			
		}
		public function generateFoldedProductFILE($oproduct){
			$magick = "/usr/bin/convert";
			$side = '';
		
			
			$summaryData = Mage::getModel('review/review_summary')->load($oproduct->getId());
			$reviewsCount = $summaryData->getReviewsCount();
			if($summaryData->getRatingSummary()){
				$ratingSummary = $summaryData->getRatingSummary(); 
			}
			else{
				$ratingSummary = 0; 
			}
						
			$attributes = $oproduct->getAttributes();
		
			$fileData='{';
			$fileData.='"id":'.$oproduct->getId().',';
			$fileData.='"title":'.'"'.$oproduct->name.'",';
			$fileData.='"rating":"'.$ratingSummary.'",';
			$fileData.='"width":7,';
			$fileData.='"height":7,';
			$fileData.='"margin":'.(($oproduct->product_margin)?$oproduct->product_margin:0).',';
			$fileData.='"bleedMargin":'.(($oproduct->product_bleed_margin)?$oproduct->product_bleed_margin:0).',';
			$fileData.='"type":"'.$attributes['product_measurement_unit']->getFrontend()->getValue($oproduct).'",';
			
			if($oproduct->getStockItem()){
				if($oproduct->getStockItem()->getMinSaleQty()){
					$fileData.='"minQty":'.$oproduct->getStockItem()->getMinSaleQty() .',';
				}else{
					$fileData.='"minQty":1,';
				}
				if($oproduct->getStockItem()->getMaxSaleQty()){
					$fileData.='"maxQty":'.$oproduct->getStockItem()->getMaxSaleQty() .',';
				}else{
					$fileData.='"maxQty":0,';
				}
			}else{
				$fileData.='"minQty":1,';
				$fileData.='"maxQty":0,';
			}
			
			//Tier Price Begin
			if($oproduct->tier_price){
				$tier_price=$oproduct->tier_price;
				$fileData.='"tier":[';
				$countTp=count($tier_price);
				$tp=1;
				foreach($tier_price as $tprice ){
					$fileData.='{"qty":'.$tprice['price_qty'].', "price":'.$tprice['price'].'}';
					$fileData.= $countTp > $tp ? ',' : '';
					$tp++;
				}
				$fileData.='],';
			}else{
				$fileData.='"tier":[],';
			}
			
			//Tier Price End
			
			$previewPosition = $attributes['preview_position']->getFrontend()->getValue($oproduct);
			$fileData.= '"position":"'.$previewPosition.'",'; // up/down
			
			$previewLayout = $attributes['preview_layout']->getFrontend()->getValue($oproduct);
			$fileData.= '"layout":"'.$previewLayout.'",';
			
			$product_price = $oproduct->price ? $oproduct->price : '0';
			$fileData.='"price":'.$product_price.',';
			
			$numberOfViews = 2;
			
			$fileData.='"view":1,';
			$fileData.='"views":';
			$fileData.='[';
			for($i=1; $i<=$numberOfViews; $i++){					
				$view_print_wh = 'view_'.$i.'_print_wh';
				$view_label = 'view_'.$i.'_label';
				
				$areaWH = explode(",",$oproduct->$view_print_wh);
				$W = $areaWH[0];
				$H = $areaWH[1];
				$fileData.='{';
				$fileData.='"view":'.$i.',';
				$fileData.='"width":'.$W.',';
				$fileData.='"height":'.$H.',';
				$fileData.='"title":"'.ucfirst($oproduct->$view_label).'",';
				
				$fileData.='"mask":"",';
				$fileData.='"vsplit":0,';
				$fileData.='"hsplit":1,';
				
				$fileData.='"data":[],';
				$fileData.='"options":[]';
					
				if($i==$numberOfViews){
					$fileData.='}]';
				}else{
					$fileData.='},';
				}
			}
			
			$fileData .= ',"preview":';
			//Preview Section Begin
			$numberOfPreview = 2;
			if($numberOfPreview){
				$side ='[';
				$ctr=0;
				for($i=1; $i<=$numberOfViews; $i++){
					for($j=1; $j<=$numberOfPreview; $j++){
						$view_i_preview_j_image = 'view_'.$i.'_preview_'.$j.'_image';
						$view_i_preview_j_positions = 'view_'.$i.'_preview_'.$j.'_positions';
						$source = Mage::getBaseDir('media').'/catalog/product'.$attributes[$view_i_preview_j_image]->getFrontend()->getValue($oproduct);										 
						$extensionpreview = substr($source, strrpos($source, '.'));
						$thumbnail = Mage::getBaseDir()."/customizer-v1/images/products/".$oproduct->getId().'_view_'.$i.'_preview_'.$j.$extensionpreview;
						
						copy($source,$thumbnail);
						
						if($ctr>0){
							$side .=  ',';
						}
						$ctr++;
						
						$arcdown = '';
						$side .= '{"view":'.$i.',"image":"'.$oproduct->getId().'_view_'.$i.'_preview_'.$j.$extensionpreview.'",'.$arcdown.'"position":['.$attributes[$view_i_preview_j_positions]->getFrontend()->getValue($oproduct).']}';
					}
				}
				$side.=']';
			}
			//Preview Section End
			$fileData .= $side;
			
			$fileData.=',"options":[';
			if($oproduct->hasOptions()) {
					$j=1;
					$ctr1 = 0;
					$ctr2 = 0;
					foreach ($oproduct->getOptions() as $o) {
						$values = $o->getValues();
						if($o->getType()=="radio" || $o->getType()=="checkbox"){
							if($ctr1>0){
								$fileData.=',';
							}
							$ctr1 = 1;
							
							$fileData.='{"id":"'.$o->getId().'","type":"label","value":"'.$o->getTitle().'","fields":[';
							
							$ctr = 0;
							foreach ($values as $v) {
								if($ctr>0){
									$fileData.=',';
								}
								
								$ctr = 1;
								
								if($v->getData(price_type)=="percent"){								
									$optionPrice= $oproduct->price ? $oproduct->price*$v->getData(price)/100 : '0';								
								}else{
									$optionPrice=$v->getData(price) ? $v->getData(price) : '0';
								}
								$fileData.='{"id":"'.$v->getData('option_type_id').'","type":"'.$o->getType().'","group":"group'.$j.'","value":'.$optionPrice.',"label":"'.$v->getData(title).'"}';
							}
							$fileData.=']}';
						}
						if($o->getType()=="drop_down"){
							if($ctr2>0){
								$fileData.=',';
							}
							$ctr2 = 1;
							$fileData.='{"id":"'.$o->getId().'","type":"label","value":"'.$o->getTitle().'","fields":[';
							$fileData.='{"id":"select1","type":"select","OPTIONS":[';
							$ctr = 0;
							foreach ($values as $v) {
								if($ctr>0){
									$fileData.=',';
								}
								$ctr = 1;
								if($v->getData(price_type)=="percent"){
									$optionPrice=$oproduct->price ? $oproduct->price*$v->getData(price)/100 : '0';								
								}else{
									$optionPrice=$v->getData(price) ? $v->getData(price) : '0';
								}
								
								$fileData.='{"id":"'.$v->getData('option_type_id').'","op":"'.$v->getData(title).'","val":'.$optionPrice.'}';
							}
							$fileData.=']}]}';
						}
						$j++;
					}
					//substr($fileData, 0, -1);
			}
			$fileData.=']';
			
			$fileData.='}'; 
			
			$fileName = 'customizer-v1/xml/products/'.$oproduct->getId().'.txt';
			
			$fp = fopen($fileName, 'w');
			fwrite($fp, $fileData);
			fclose($fp);
			unset($fileName);
			
		}
		protected function saveuploadpdf($file,$prod){
		
				$pdfid =null;
				$value = $prod->getId();
				$sql = "SELECT * FROM pdf WHERE name=$value";
				$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
				foreach ($connection->fetchAll($sql) as $arr_row) {
					$pdfid = $arr_row['pdf_id'];
					}
				$pdfid." sdsdfs";
				
				if($pdfid){
					$pdf = Mage::getModel('pdf/pdf')->loadByAttribute('name', $prod->getId());
				}
				else{
					$pdf = Mage::getModel('pdf/pdf');
				}
				$fname = $file['name'];
				$uploader = new Varien_File_Uploader('pdf');
				$uploader->setAllowedExtensions(array('pdf'));
				$uploader->setAllowRenameFiles(false);
				$uploader->setFilesDispersion(false);
				$extention = $uploader->getFileExtension();
				$fname = $value.'.'.$extention;
				$path = Mage::getBaseDir() . DS . 'customizer-v1/library/background'; // .'fonts/';
				$_FILES['filename']['name'] = $fname;  
				$uploader->save($path,$_FILES['filename']['name']);
				$pdf->setName($prod->getId());
				$pdf->setPdf('customizer-v1/library/background/'.$fname);
				$pdf->save();
		}
		
}