<?php

/**

 * Magento

 *

 * NOTICE OF LICENSE

 *

 * This source file is subject to the Open Software License (OSL 3.0)

 * that is bundled with this package in the file LICENSE.txt.

 * It is also available through the world-wide-web at this URL:

 * http://opensource.org/licenses/osl-3.0.php

 * If you did not receive a copy of the license and are unable to

 * obtain it through the world-wide-web, please send an email

 * to license@magentocommerce.com so we can send you a copy immediately.

 *

 * DISCLAIMER

 *

 * Do not edit or add to this file if you wish to upgrade Magento to newer

 * versions in the future. If you wish to customize Magento for your

 * needs please refer to http://www.magentocommerce.com for more information.

 *

 * @category    Mage

 * @package     Mage_Sales

 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)

 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)

 */



/**

 * Sales Order Invoice PDF model

 *

 * @category   Mage

 * @package    Mage_Sales

 * @author     Magento Core Team <core@magentocommerce.com>

 */

class Mage_Sales_Model_Order_Pdf_Invoice extends Mage_Sales_Model_Order_Pdf_Abstract

{

    /**

     * Draw header for item table

     *

     * @param Zend_Pdf_Page $page

     * @return void

     */

    protected function _drawHeader(Zend_Pdf_Page $page)

    {

        /* Add table head */

        $this->_setFontRegular($page, 10);

        $page->setFillColor(new Zend_Pdf_Color_RGB(0.93, 0.92, 0.92));

        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));

        $page->setLineWidth(0.5);

        $page->drawRectangle(25, $this->y, 570, $this->y -15);

        $this->y -= 10;

        $page->setFillColor(new Zend_Pdf_Color_RGB(0, 0, 0));



        //columns headers

        $lines[0][] = array(

            'text' => Mage::helper('sales')->__('Products'),

            'feed' => 35

        );
		
		 $lines[0][] = array(

            'text' => Mage::helper('sales')->__('Product Image'),

             'feed'  => 200,
			 'align' => 'right'

        );




        $lines[0][] = array(

            'text'  => Mage::helper('sales')->__('SKU'),

            'feed'  => 250,

            'align' => 'right'

        );



        $lines[0][] = array(

            'text'  => Mage::helper('sales')->__('Qty'),

            'feed'  => 440,

            'align' => 'right'

        );



        $lines[0][] = array(

            'text'  => Mage::helper('sales')->__('Price'),

            'feed'  => 390,

            'align' => 'right'

        );



        $lines[0][] = array(

            'text'  => Mage::helper('sales')->__('Tax'),

            'feed'  => 486,

            'align' => 'right'

        );



        $lines[0][] = array(

            'text'  => Mage::helper('sales')->__('Subtotal'),

            'feed'  => 565,

            'align' => 'right'

        );



        $lineBlock = array(

            'lines'  => $lines,

            'height' => 5

        );



        $this->drawLineBlocks($page, array($lineBlock), array('table_header' => true));

        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

        $this->y -= 20;

    }



    /**

     * Return PDF document

     *

     * @param  array $invoices

     * @return Zend_Pdf

     */

    public function getPdf($invoices = array())

    {

        $this->_beforeGetPdf();

        $this->_initRenderer('invoice');



        $pdf = new Zend_Pdf();

        $this->_setPdf($pdf);

        $style = new Zend_Pdf_Style();

        $this->_setFontBold($style, 10);



        foreach ($invoices as $invoice) {

            if ($invoice->getStoreId()) {

                Mage::app()->getLocale()->emulate($invoice->getStoreId());

                Mage::app()->setCurrentStore($invoice->getStoreId());

            }

            $page  = $this->newPage();

            $order = $invoice->getOrder();

            /* Add image */

            $this->insertLogo($page, $invoice->getStore());

            /* Add address */

            $this->insertAddress($page, $invoice->getStore());

            /* Add head */

            $this->insertOrder(

                $page,

                $order,

                Mage::getStoreConfigFlag(self::XML_PATH_SALES_PDF_INVOICE_PUT_ORDER_ID, $order->getStoreId())

            );

            /* Add document text and number */

            $this->insertDocumentNumber(

                $page,

                Mage::helper('sales')->__('Invoice # ') ."PL". $invoice->getIncrementId()

            );

            /* Add table */

            $this->_drawHeader($page);

            /* Add body */
			
			$this->y -=15;

            foreach ($invoice->getAllItems() as $item){
				
				
				if ($this->y < 15) {
					$page = $this->newPage(array('table_header' => true));
				}

                if ($item->getOrderItem()->getParentItem()) {

                    continue;

                }

                /* Draw item */

             	$page= $this->_drawItem($item, $page, $order);
				
				
				
				 $productId = $item->getOrderItem()->getProductId();
				//echo "dfdf".$productId;
			//	die("ppp");
				$image = Mage::getModel('catalog/product')->load($productId);
				//print_r($image);
				//echo "ddd".$image;
				//die();
				//$this->insertImage($image, 245, 50, 310, 50, 200, 200, $page);
				$this->insertImage($image, 120, (int)($this->y + 15), 210, (int)($this->y+45), $width, $height, $page);
							

                $page = end($pdf->pages);

            }
			
			
			
			
			
			
			
			
			
			
			

			//$condition = "adsaa";

			// $this->condition = $this->_termCondition();

			

            /* Add totals */

            $this->insertTotals($page, $invoice);

            if ($invoice->getStoreId()) {

                Mage::app()->getLocale()->revert();

            }

			$this->_contactusmail($page);

			$coupontitle = $order->getData();

			if($coupontitle['discount_description']){

			$this->_descountoffer($page,$order);

			}

			$this->_invoiceurl($page);

			$this->_termConditionheading($page);

			$this->_termCondition1($page);

			$this->_termCondition2($page);

			$this->_termCondition3($page);

			$this->_termCondition4($page);

			$this->_termCondition5($page);

			

			

        }

        $this->_afterGetPdf();

        return $pdf;

    }



    /**

     * Create new page and assign to PDF object

     *

     * @param  array $settings

     * @return Zend_Pdf_Page

     */

    public function newPage(array $settings = array())

    {

        /* Add new table head */

        $page = $this->_getPdf()->newPage(Zend_Pdf_Page::SIZE_A4);

        $this->_getPdf()->pages[] = $page;

        $this->y = 800;

        if (!empty($settings['table_header'])) {

            $this->_drawHeader($page);

        }

        return $page;

    }

}

