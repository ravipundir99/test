<?php

class Company_Web_Block_Adminhtml_Web_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

//$dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);

  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('web_form', array('legend'=>Mage::helper('web')->__('Item information')));
     
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('web')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));
		/*
      $fieldset->addField('filename', 'file', array(
          'label'     => Mage::helper('web')->__('File'),
          'required'  => false,
          'name'      => 'filename',
	  ));
		*/
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('web')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('web')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('web')->__('Disabled'),
              ),
          ),
      ));
     
	 $category_model = Mage::getModel('catalog/category');
	 $_category = $category_model->load(2);
	 $all_child_categories = $category_model->getResource()->getAllChildren($_category);
	 $catarr = array();
	 foreach($all_child_categories as $subacat)
	 {
	 
	 $_categ = $category_model->load($subacat);
	 if($_categ->getId() != 2)
	 {
	
	 $catarr[] = array('value'=> $_categ->getId(), 'label'=> Mage::helper('web')->__($_categ->getName()),);
	 
	 }
	 }
	 
	 $fieldset->addField('category', 'select', array(
          'label'     => Mage::helper('web')->__('Category'),
          'name'      => 'category',
          'values'    => $catarr,
		
			));
	 
	 
	 
     /* $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('web')->__('Content'),
          'title'     => Mage::helper('web')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
          'required'  => true,
      ));*/
	  
	    $dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
			$fieldset->addField('from_date', 'date', array(
			'name'   => 'from_date',
            'label'  => Mage::helper('catalogrule')->__('Event Date'),
             'title'  => Mage::helper('catalogrule')->__('Event Date'),
             'image'  => $this->getSkinUrl('images/grid-cal.gif'),
             'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
             'format'       => $dateFormatIso
			//�required� => true,
			));
			
     
      if ( Mage::getSingleton('adminhtml/session')->getWebData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getWebData());
          Mage::getSingleton('adminhtml/session')->setWebData(null);
      } elseif ( Mage::registry('web_data') ) {
          $form->setValues(Mage::registry('web_data')->getData());
      }
      return parent::_prepareForm();
  }
}