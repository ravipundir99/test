<?php
class Fonts_Fonts_Block_Fonts extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getFonts()     
     { 
        if (!$this->hasData('fonts')) {
            $this->setData('fonts', Mage::registry('fonts'));
        }
        return $this->getData('fonts');
        
    }
}