<?php
class Fonts_Fonts_Block_Adminhtml_Fonts extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_fonts';
    $this->_blockGroup = 'fonts';
    $this->_headerText = Mage::helper('fonts')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('fonts')->__('Add Item');
    parent::__construct();
  }
}