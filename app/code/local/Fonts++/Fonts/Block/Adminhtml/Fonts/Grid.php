<?php

class Fonts_Fonts_Block_Adminhtml_Fonts_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('fontsGrid');
      $this->setDefaultSort('fonts_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('fonts/fonts')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('fonts_id', array(
          'header'    => Mage::helper('fonts')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'fonts_id',
      ));

	  $this->addColumn('title1', array(
          'header'    => Mage::helper('fonts')->__('Title'),
          'align'     =>'left',
          'index'     => 'title1',
      ));

	  
      $this->addColumn('title', array(
          'header'    => Mage::helper('fonts')->__('Font Type'),
          'align'     =>'left',
          'index'     => 'title',
      ));

	  	 $this->addColumn('fontimg', array(
            'header'=>Mage::helper('fonts')->__('Gallery Image'),
            'index'=>'fontimg',
            'align' => 'center',
			 'type'      => 'imag',
             
               
        )); 
		
	  
	  
	  /*
      $this->addColumn('content', array(
			'header'    => Mage::helper('fonts')->__('Item Content'),
			'width'     => '150px',
			'index'     => 'content',
      ));
	  */

      $this->addColumn('status', array(
          'header'    => Mage::helper('fonts')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('fonts')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('fonts')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('fonts')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('fonts')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('fonts_id');
        $this->getMassactionBlock()->setFormFieldName('fonts');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('fonts')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('fonts')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('fonts/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('fonts')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('fonts')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}