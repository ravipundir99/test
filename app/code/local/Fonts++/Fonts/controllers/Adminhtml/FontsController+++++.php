<?php

class Fonts_Fonts_Adminhtml_FontsController extends Mage_Adminhtml_Controller_action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('fonts/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('fonts/fonts')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('fonts_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('fonts/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('fonts/adminhtml_fonts_edit'))
				->_addLeft($this->getLayout()->createBlock('fonts/adminhtml_fonts_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('fonts')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
			$id   = $this->getRequest()->getParam("id");
			$model  = Mage::getModel('fonts/fonts')->load($id);
			
			
			require "ycTIN_TTF.php";
			$ttf = new ycTIN_TTF();
			//$path = $_SERVER['DOCUMENT_ROOT'].'/test/printland/customizer-v1/fonts/';
			$path = Mage::getBaseDir() . DS .'printland'. DS .'customizer-v1'. DS .'fonts'. DS;
			
			###################################  normal ####################################
			$flname = null;
			$flag = true;
			if(isset($_FILES['normal']['name']) && $_FILES['normal']['name'] != '') {
				try {	
						
					$flag = false;
					/* Starting upload */	
					$uploader = new Varien_File_Uploader('normal');
					
					// Any extention would work
	           		$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png','swf','ttf','TTF','otf','OTF'));
					$uploader->setAllowRenameFiles(true);
					
					// Set the file upload mode 
					// false -> get the file directly in the specified folder
					// true -> get the file in the product like folders 
					//	(file.jpg will go in something like /media/f/i/file.jpg)
					$uploader->setFilesDispersion(false);
					$extension = pathinfo($_FILES['normal']['name'], PATHINFO_EXTENSION);
					if($extension == 'TTF'){
					$extension = 'ttf';
					}
					$time = time();
					$file_name = $time.".".$extension; 
					//$path = Mage::getBaseDir('media') . DS;		
					// We set media as the upload dir
					// $path = Mage::getBaseDir('media') . DS .'fonts/';
					//$path = Mage::getBaseDir('media') . DS; // .'fonts
					
					
					
					$_FILES['normal']['name'] = $file_name;  
					
					$tmp_name = $_FILES['normal']['tmp_name'];
					move_uploaded_file($tmp_name, $path.$_FILES['normal']['name']);
					
					//$uploader->save($path, $_FILES['normal']['name']);
					if($extension == 'otf'){
					//$extension ='ttf';
						$result = shell_exec('/usr/bin/fontforge -script '.$path.'otf2ttf.sh '.$path.$_FILES['normal']['name']);
						$extension = pathinfo($path.$_FILES['normal']['name'], PATHINFO_EXTENSION);
					}
					$flname = $file_name;
					
					
						//require "ycTIN_TTF.php";
						//$ttf = new ycTIN_TTF();
						
						$fileTTF = $path.$flname;
						$fontInfo = $ttf->getStyle($fileTTF);
						
					
					require "TtfToImage.php";
					$image = new TFT_image();
					$path.$file_name;
					
					//$medi = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
				
					$image->makeimage($time,$path.$file_name,$fontInfo,$path);
                    //$imagefile = $time.".png";
					
					//$_FILES['filename']['name']    =   $imagefile;  
					
					
					
					
				} catch (Exception $e) {
		      
		        }
	        
		        //this way the name is saved in DB
	  			$data['normal'] = $file_name;
			}
			else{
			$data['normal'] = $model->getNormal();
			$flname = $model->getNormal();
			}
			
			
			if($data['style'] == '' && $flag){
				$fileTTF = $path.$flname;
				$fontInfo = $ttf->getStyle($fileTTF);
				$data['style'] = $fontInfo;
			}
			
	  		###################################  end normal ####################################	
	  		
			
			
			###################################  bold ####################################
			if(isset($_FILES['bold']['name']) && $_FILES['bold']['name'] != '') {
				try {	
					/* Starting upload */	
					$uploader = new Varien_File_Uploader('bold');
					
					// Any extention would work
	           		$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png','swf','ttf','TTF'));
					$uploader->setAllowRenameFiles(false);
					
					// Set the file upload mode 
					// false -> get the file directly in the specified folder
					// true -> get the file in the product like folders 
					//	(file.jpg will go in something like /media/f/i/file.jpg)
					$uploader->setFilesDispersion(false);
							
					// We set media as the upload dir
					 //$path = Mage::getBaseDir('media') . DS .'DS/';
					$time = time();
					$extension = pathinfo($_FILES['bold']['name'], PATHINFO_EXTENSION);
					if($extension == 'TTF'){
					$extension = 'ttf';
					}
					$file_name = $time."_bold.".$extension; 
					//$path = Mage::getBaseDir('media') . DS;// .'fonts';
					
					if(!is_dir($path))
					{
						mkdir($path);
					}
					
					
					$_FILES['bold']['name']    =   $imagefile;              
					
					
					$uploader->save($path, $file_name);
					if($extension == 'otf'){
					//$extension ='ttf';
						$result = shell_exec('/usr/bin/fontforge -script '.$path.'otf2ttf.sh '.$path.$_FILES['normal']['name']);
						$extension = pathinfo($path.$_FILES['normal']['name'], PATHINFO_EXTENSION);
					}
				}
				catch (Exception $e) {
		      
		        }
				
		        //this way the name is saved in DB
	  			$data['bold'] = $file_name;
			}
			else{
			$data['bold'] = $model->getBold();
			}
			################################### End bold ####################################
			
			###################################  italic ####################################
			if(isset($_FILES['italic']['name']) && $_FILES['italic']['name'] != '') {
				try {	
					/* Starting upload */	
					$uploader = new Varien_File_Uploader('italic');
					
					// Any extention would work
	           		$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png','swf','ttf','TTF'));
					$uploader->setAllowRenameFiles(false);
					
					// Set the file upload mode 
					// false -> get the file directly in the specified folder
					// true -> get the file in the product like folders 
					//	(file.jpg will go in something like /media/f/i/file.jpg)
					$uploader->setFilesDispersion(false);
							
					// We set media as the upload dir
					 //$path = Mage::getBaseDir('media') . DS .'DS/';
					$time = time();
					$extension = pathinfo($_FILES['italic']['name'], PATHINFO_EXTENSION);
					if($extension == 'TTF'){
					$extension = 'ttf';
					}
					$file_name = $time."_italic.".$extension; 
				//	$path = Mage::getBaseDir('media') . DS;// .'fonts';
					if(!is_dir($path))
					{
						mkdir($path);
					}
                    
					
					$_FILES['italic']['name']    =   $file_name;                   
					$uploader->save($path, $_FILES['italic']['name'] );
					if($extension == 'otf'){
						//$extension ='ttf';
							$result = shell_exec('/usr/bin/fontforge -script '.$path.'otf2ttf.sh '.$path.$_FILES['normal']['name']);
							$extension = pathinfo($path.$_FILES['normal']['name'], PATHINFO_EXTENSION);
						}

				} catch (Exception $e) {
		      
		        }
				
		        //this way the name is saved in DB
	  			$data['italic'] = $_FILES['italic']['name'];
			}
			else{
			$data['italic'] = $model->getItalic();
			}
			################################### End italic ####################################
				
			################################### Bold italic ####################################
			if(isset($_FILES['bolditalic']['name']) && $_FILES['bolditalic']['name'] != '') {
				try {	
					/* Starting upload */	
					$uploader = new Varien_File_Uploader('bolditalic');
					
					// Any extention would work
	           		$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png','swf','ttf','TTF'));
					$uploader->setAllowRenameFiles(false);
					
					// Set the file upload mode 
					// false -> get the file directly in the specified folder
					// true -> get the file in the product like folders 
					//	(file.jpg will go in something like /media/f/i/file.jpg)
					$uploader->setFilesDispersion(false);
							
					// We set media as the upload dir
					 //$path = Mage::getBaseDir('media') . DS .'DS/';
					$time = time();
					$extension = pathinfo($_FILES['bolditalic']['name'], PATHINFO_EXTENSION);
					if($extension == 'TTF'){
					$extension = 'ttf';
					}
					$file_name = $time."_bolditalic.".$extension; 
				//	$path = Mage::getBaseDir('media') . DS;// .'fonts';
					if(!is_dir($path))
					{
						mkdir($path);
					}
                    
					
					$_FILES['bolditalic']['name']    =   $file_name;                   
					$uploader->save($path, $_FILES['bolditalic']['name'] );
					if($extension == 'otf'){
						$result = shell_exec('/usr/bin/fontforge -script '.$path.'otf2ttf.sh '.$path.$_FILES['normal']['name']);
						$extension = pathinfo($path.$_FILES['normal']['name'], PATHINFO_EXTENSION);
					}

				} catch (Exception $e) {
		      
		        }
				
		        //this way the name is saved in DB
	  			$data['bolditalic'] = $_FILES['bolditalic']['name'];
			}
			else{
			$data['bolditalic'] = $model->getBolditalic();
			}
			
		
			
			
			################################### End Bold italic ####################################	
				
			//print_r($data);
			//exit;
			$model = Mage::getModel('fonts/fonts');		
			$model->setStyle($fontInfo);
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			
			
			try {
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
						->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}	
				
				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('fonts')->__('Item was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				/*
				 $str1 .='<?xml version="1.0" encoding="utf-8"?>';
				$str1 .='<xml>';
				 $str1 .='<style>style.swf</style>';
				   $str1 .=' <fonts default="'.$model->getId().'">';
				   $dbread = Mage::getSingleton('core/resource')->getConnection('core_read');
					
					$result = $dbread->query("SELECT * FROM fonts WHERE status='1' ");
				    foreach($result->fetchAll() as $data)
					{ 
					   $pathswf= "fonts/".$data['filename'];
					 $str1 .='<font id="'.$data['fonts_id'].'" thumbnail="'.$data['fontimg'].'" bold="'.$data['bold'].'" italic="'.$data['italic'].'" url="'. $pathswf.'" style="'.$data['style'].'" ttf="'.$data['title'].'"/>';
					}
				$str1 .='</fonts>'; 
				$str1 .='</xml>';
				$fileName = 'php/xml/data.xml';
						$fp = fopen($fileName, 'w');
						fwrite($fp, $str1);
						fclose($fp);
				}
				*/
				// END HERE
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('fonts')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}
	
	public function deleteAction() {
	
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('fonts/fonts');
				 
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
					 
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		// CREATE XML 
		    

			$dbc_collect_order = Mage::getSingleton('core/resource')->getConnection('core_read');
			$items_collect_order = $dbc_collect_order->fetchAll("SELECT * FROM fonts  WHERE status='1'");
              $last_main_order_id = $items_collect_order['0']['fonts_id']; 
		
		
		          $str2 .='<?xml version="1.0" encoding="utf-8"?>';
				$str2 .='<xml>';
				 $str2 .='<style>style.swf</style>';
				   $str2 .=' <fonts default="'.$last_main_order_id.'">';
				   $dbread = Mage::getSingleton('core/resource')->getConnection('core_read');
					
					$result = $dbread->query("SELECT * FROM fonts WHERE status='1' ");
				    foreach($result->fetchAll() as $data)
					{ 
					
					
					   $pathswf= "fonts/".$data['filename'];
					 $str2 .='<font id="'.$data['fonts_id'].'" thumbnail="'.$data['fontimg'].'" bold="'.$data['bold'].'" italic="'.$data['italic'].'" url="'. $pathswf.'" style="'.$data['style'].'" ttf="'.$data['title'].'"/>';
					}
				$str2 .='</fonts>'; 
				$str2 .='</xml>';
				$fileName = 'php/xml/data.xml';
						$fp = fopen($fileName, 'w');
						fwrite($fp, $str2);
						fclose($fp);
				//END HERE
		
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
	/*echo "<pre>";	print_r($_REQUEST);
	exit;*/
        $fontsIds = $this->getRequest()->getParam('fonts');
        if(!is_array($fontsIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($fontsIds as $fontsId) {
                    $fonts = Mage::getModel('fonts/fonts')->load($fontsId);
                    $fonts->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($fontsIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
		// CREATE XML 
		    

			$dbc_collect_order = Mage::getSingleton('core/resource')->getConnection('core_read');
			$items_collect_order = $dbc_collect_order->fetchAll("SELECT * FROM fonts  WHERE status='1'");
              $last_main_order_id = $items_collect_order['0']['fonts_id']; 
		
		
		          $str2 .='<?xml version="1.0" encoding="utf-8"?>';
				$str2 .='<xml>';
				 $str2 .='<style>style.swf</style>';
				   $str2 .=' <fonts default="'.$last_main_order_id.'">';
				   $dbread = Mage::getSingleton('core/resource')->getConnection('core_read');
					
					$result = $dbread->query("SELECT * FROM fonts WHERE status='1' ");
				    foreach($result->fetchAll() as $data)
					{ 
					
					
					   $pathswf= "fonts/".$data['filename'];
					 $str2 .='<font id="'.$data['fonts_id'].'" thumbnail="'.$data['fontimg'].'" bold="'.$data['bold'].'" italic="'.$data['italic'].'" url="'. $pathswf.'" style="'.$data['style'].'" ttf="'.$data['title'].'"/>';
					}
				$str2 .='</fonts>'; 
				$str2 .='</xml>';
				$fileName = 'php/xml/data.xml';
						$fp = fopen($fileName, 'w');
						fwrite($fp, $str2);
						fclose($fp);
				//END HERE
		
		
		
		
        $this->_redirect('*/*/index');
    }
	
    public function massStatusAction()
    {
        $fontsIds = $this->getRequest()->getParam('fonts');
        if(!is_array($fontsIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($fontsIds as $fontsId) {
                    $fonts = Mage::getSingleton('fonts/fonts')
                        ->load($fontsId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($fontsIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
// CREATE XML 
		    

			$dbc_collect_order = Mage::getSingleton('core/resource')->getConnection('core_read');
			$items_collect_order = $dbc_collect_order->fetchAll("SELECT * FROM fonts  WHERE status='1'");
              $last_main_order_id = $items_collect_order['0']['fonts_id']; 
		
		
		          $str2 .='<?xml version="1.0" encoding="utf-8"?>';
				$str2 .='<xml>';
				 $str2 .='<style>style.swf</style>';
				   $str2 .=' <fonts default="'.$last_main_order_id.'">';
				   $dbread = Mage::getSingleton('core/resource')->getConnection('core_read');
					
					$result = $dbread->query("SELECT * FROM fonts WHERE status='1' ");
				    foreach($result->fetchAll() as $data)
					{ 
					
					
					   $pathswf= "fonts/".$data['filename'];
					 $str2 .='<font id="'.$data['fonts_id'].'" thumbnail="'.$data['fontimg'].'" bold="'.$data['bold'].'" italic="'.$data['italic'].'" url="'. $pathswf.'" style="'.$data['style'].'" ttf="'.$data['title'].'"/>';
					}
				$str2 .='</fonts>'; 
				$str2 .='</xml>';
				$fileName = 'php/xml/data.xml';
						$fp = fopen($fileName, 'w');
						fwrite($fp, $str2);
						fclose($fp);
				//END HERE
		
        $this->_redirect('*/*/index');
    }
  
    public function exportCsvAction()
    {
        $fileName   = 'fonts.csv';
        $content    = $this->getLayout()->createBlock('fonts/adminhtml_fonts_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'fonts.xml';
        $content    = $this->getLayout()->createBlock('fonts/adminhtml_fonts_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
	
}