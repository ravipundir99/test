<?php

class Fonts_Fonts_Model_Mysql4_Fonts extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the fonts_id refers to the key field in your database table.
        $this->_init('fonts/fonts', 'fonts_id');
    }
}