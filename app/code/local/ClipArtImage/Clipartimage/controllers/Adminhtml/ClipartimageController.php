<?php

class ClipArtImage_Clipartimage_Adminhtml_ClipartimageController extends Mage_Adminhtml_Controller_action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('clipartimage/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('clipartimage/clipartimage')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('clipartimage_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('clipartimage/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('clipartimage/adminhtml_clipartimage_edit'))
				->_addLeft($this->getLayout()->createBlock('clipartimage/adminhtml_clipartimage_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('clipartimage')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {
	     /*echo "<pre>";
		 print_r($_REQUEST);
		 print_r($_FILES);
		 exit;*/
		$id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel('clipartimage/clipartimage')->load($id);
		if ($data = $this->getRequest()->getPost()) {
		
		if($_FILES['filename']['name']!='')
         {
		    
			function getExtension($str)
			{
				$i = strrpos($str,".");
				if (!$i)
				{
					return "";
				}
				$l = strlen($str) - $i;
				$ext = substr($str,$i+1,$l);
				return $ext;
			}

				$file_name=$_FILES['filename']['name'];

				$extension = strtolower(getExtension($file_name));
				 
				$imageId = time();
				$image = $imageId.".".$extension;
			if(isset($image) && $image != '') {
				try {	
					/* Starting upload */	
					$uploader = new Varien_File_Uploader('filename');
					
					// Any extention would work
	           		$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
					$uploader->setAllowRenameFiles(false);
					
					// Set the file upload mode 
					// false -> get the file directly in the specified folder
					// true -> get the file in the product like folders 
					//	(file.jpg will go in something like /media/f/i/file.jpg)
					$uploader->setFilesDispersion(false);
							
					// We set media as the upload dir
					// $siteurl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
		            //$path = images.'/'.cliparts ;
					$path = Mage::getBaseDir() . DS . 'customizer-v1/library/images/cliparts'; // .'fonts/';
					// $path = Mage::getBaseDir('images') . DS . 'cliparts' . DS ;
					//$path = Mage::getBaseDir('media') . DS ;
					//this way the name is saved in DB
			         //	$regular =$imageId."_regular.".$extension;
                     //  $thumbnail =$imageId."_small.".$extension;
				     //$uploader->save($path, $regular);
				     // $uploader->save($path, $thumbnail);
					$_FILES['filename']['name'] = $image;  
					 
				    $uploader->save($path,$_FILES['filename']['name']);
	        } catch (Exception $e) {
		      
		        }
				  $destination =  "images/cliparts/".$image;
				  $regular = "images/cliparts/".$imageId."_regular.".$extension;
				  $thumbnail = "images/cliparts/".$imageId."_small.".$extension; 
               
			define ("MAX_SIZE","400");

				$errors=0;
			 
			if($image) 
			{

				if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
				{
					echo ' Unknown Image extension ';
					$errors=1;
				}
				else
				{
					
					if($extension=="jpg" || $extension=="jpeg" )
					{
						$src = imagecreatefromjpeg($destination);
					}
					else if($extension=="png")
					{
						$src = imagecreatefrompng($destination);
					}
					else 
					{
						$src = imagecreatefromgif($destination);
					}
					list($width,$height)=getimagesize($destination);
			if($width>$height){
				$newwidth=250;
				$newheight=($height/$width)*$newwidth;
			}else{
				$newheight=250;
				$newwidth=($width/$height)*$newheight;
			}
			$tmp=imagecreatetruecolor($newwidth,$newheight);
			if($extension=="png" || $extension=="gif"){
				imagecolortransparent($tmp, imagecolorallocatealpha($tmp, 0, 0, 0, 127));
				imagealphablending($tmp, false);
				imagesavealpha($tmp, true);
			}
			
			if($width>$height){
				$newwidth1=80;
				$newheight1=($height/$width)*$newwidth1;
			}else{
				$newheight1=80;
				$newwidth1=($width/$height)*$newheight1;
			}
			
			$tmp1=imagecreatetruecolor($newwidth1,$newheight1);
			if($extension=="png" || $extension=="gif"){
				imagecolortransparent($tmp1, imagecolorallocatealpha($tmp1, 0, 0, 0, 127));
				imagealphablending($tmp1, false);
				imagesavealpha($tmp1, true);
			}
			
			imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
			imagecopyresampled($tmp1,$src,0,0,0,0,$newwidth1,$newheight1,$width,$height);

			if($extension=="png" || $extension=="gif"){
				imagepng($tmp,$regular);
				imagepng($tmp1,$thumbnail);
			}else{
				imagejpeg($tmp,$regular,100);
				imagejpeg($tmp1,$thumbnail,100);
			}

					imagedestroy($src);
					imagedestroy($tmp);
					imagedestroy($tmp1);
				}
			}
	  		 	$data['filename'] = $image;
			 	$data['thumbnail']=$imageId."_small.".$extension;
			 	$data['regular']=$imageId."_regular.".$extension;
				$thumb = $imageId."_small.".$extension;
	           
			}
	  	
         }
		 else{
		$data['filename'] = $model->getFilename();
			//$flname = $model->getFilename();	
	  			}
		//echo $data['filename'];
				//	 exit;		
	$filepath = Mage::getBaseDir() . DS . 'customizer-v1/library/images/cliparts/'.$data['filename'];	
	list($width, $height, $type, $attr) = getimagesize($filepath);	
			$data['height']	=  $height;
			
			$data['width']	=  $width;
				
			$model = Mage::getModel('clipartimage/clipartimage');		
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			 // FOR GENERATE XML
			
			try {
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
						->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}	
				 
				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('clipartimage')->__('Item was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				//if($_FILES['filename']['name']!=''){
			               
			/* echo "<pre>";
			 echo $model->getId();
		 print_r($_REQUEST);
		 print_r($_FILES);
		 echo $image;*/
		// exit;
		/*  list($width, $height, $type, $attr) = getimagesize($image);// 
			 $width = $width;
			$height = $height;
			$type =$type;
			$cat_id = $_REQUEST['categoryId'];
			//echo "SELECT * FROM clipartcategory where clipartcategory_id ='".$cat_id."'";
		$sql_select_catename = mysql_query("SELECT * FROM clipartcategory where clipartcategory_id ='".$cat_id."'");
		 $result_cate = mysql_fetch_array($sql_select_catename);
		 $_SESSION['sess']="vijay kumar";
		//$_SESSION['sess']=$product;
		//$productId = $product->getId();
		if($result_cate['status']=='1'){
		$sql_select = mysql_query("SELECT a.*, b.* FROM clipartcategory as a ,clipartimage as b where a.clipartcategory_id =b.categoryId  ORDER BY RAND() LIMIT 0,6 ");
		while($result_name = mysql_fetch_array($sql_select)){
		$str .= '<cliparts>';
		  $str . = "<items id=$result_name['clipartimage_id'] thumbnail="images/cliparts/".$result_name['thumbnail']/>";
		$str .= '</cliparts>';
		}
	
	
		$fileName = 'php/xml/library.xml';
		$fp = fopen($fileName, 'w');
		fwrite($fp, $str);
		fclose($fp);
			} 
			 }*/
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
		       
				
       
		}
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('clipartimage')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}
 	
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('clipartimage/clipartimage');
				 
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
					 
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
			
		}
		  
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
        $clipartimageIds = $this->getRequest()->getParam('clipartimage');
        if(!is_array($clipartimageIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($clipartimageIds as $clipartimageId) {
                    $clipartimage = Mage::getModel('clipartimage/clipartimage')->load($clipartimageId);
                    $clipartimage->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($clipartimageIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
    public function massStatusAction()
    {
        $clipartimageIds = $this->getRequest()->getParam('clipartimage');
        if(!is_array($clipartimageIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($clipartimageIds as $clipartimageId) {
                    $clipartimage = Mage::getSingleton('clipartimage/clipartimage')
                        ->load($clipartimageId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($clipartimageIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
  
    public function exportCsvAction()
    {
        $fileName   = 'clipartimage.csv';
        $content    = $this->getLayout()->createBlock('clipartimage/adminhtml_clipartimage_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'clipartimage.xml';
        $content    = $this->getLayout()->createBlock('clipartimage/adminhtml_clipartimage_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}