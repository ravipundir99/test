<?php

class ClipArtImage_Clipartimage_Model_Clipartimage extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('clipartimage/clipartimage');
    }
}