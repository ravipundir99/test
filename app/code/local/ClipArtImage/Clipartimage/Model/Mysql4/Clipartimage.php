<?php

class ClipArtImage_Clipartimage_Model_Mysql4_Clipartimage extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the clipartimage_id refers to the key field in your database table.
        $this->_init('clipartimage/clipartimage', 'clipartimage_id');
    }
}