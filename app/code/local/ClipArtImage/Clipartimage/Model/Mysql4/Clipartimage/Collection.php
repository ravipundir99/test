<?php

class ClipArtImage_Clipartimage_Model_Mysql4_Clipartimage_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('clipartimage/clipartimage');
    }
}