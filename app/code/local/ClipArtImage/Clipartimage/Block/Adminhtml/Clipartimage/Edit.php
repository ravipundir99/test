<?php
class ClipArtImage_Clipartimage_Block_Adminhtml_Clipartimage_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'clipartimage';
        $this->_controller = 'adminhtml_clipartimage';
        
        $this->_updateButton('save', 'label', Mage::helper('clipartimage')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('clipartimage')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('clipartimage_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'clipartimage_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'clipartimage_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('clipartimage_data') && Mage::registry('clipartimage_data')->getId() ) {
            return Mage::helper('clipartimage')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('clipartimage_data')->getTitle()));
        } else {
            return Mage::helper('clipartimage')->__('Add Item');
        }
    }
}