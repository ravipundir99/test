<?php

class ClipArtImage_Clipartimage_Block_Adminhtml_Clipartimage_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('clipartimage_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('clipartimage')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('clipartimage')->__('Item Information'),
          'title'     => Mage::helper('clipartimage')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('clipartimage/adminhtml_clipartimage_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}