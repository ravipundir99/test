<?php

class ClipArtImage_Clipartimage_Block_Adminhtml_Clipartimage_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
 
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('clipartimage_form', array('legend'=>Mage::helper('clipartimage')->__('Item information')));
           
     $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('clipartimage')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));
	    
	
	    
	 
      $fieldset->addField('filename', 'file', array(
          'label'     => Mage::helper('clipartimage')->__('Front Image'),
           'class'     => 'required-entry',         
		 'name'      => 'filename',
		  'required'  => true,
	  ));
	  
	
	    $dbread = Mage::getSingleton('core/resource')->getConnection('core_read');
		$result = $dbread->query('SELECT * FROM clipartcat where status = 1');
		
		$options = array();
		foreach($result->fetchAll() as $data){
			$options[] = array('label'=>$data['title1'],'value'=>$data['clipartcat_id']);
		}
		
		
		$fieldset->addField('categoryId', 'select', array(
          'label'     => Mage::helper('clipartimage')->__('Category'),
          'name'      => 'categoryId',
          'values'    => $options,
      ));
		
		
		
   $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('clipartimage')->__('Status'),
          'name'      => 'status',
          'values'    => array(
		  
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('clipartimage')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('clipartimage')->__('Disabled'),
              ),
          ),
      ));
	   $fieldset->addField('cost', 'text', array(
          'label'     => Mage::helper('clipartimage')->__('Cost'),
          
          
          'name'      => 'cost',
      ));
     
      /*$fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('clipartimage')->__('Content'),
          'title'     => Mage::helper('clipartimage')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
          'required'  => true,
      ));*/
     
      if ( Mage::getSingleton('adminhtml/session')->getClipartimageData() )
      {
	  
          $form->setValues(Mage::getSingleton('adminhtml/session')->getClipartimageData());
          Mage::getSingleton('adminhtml/session')->setClipartimageData(null);
      } elseif ( Mage::registry('clipartimage_data') ) {
	    
          $form->setValues(Mage::registry('clipartimage_data')->getData());
      }
      return parent::_prepareForm();
  }
}