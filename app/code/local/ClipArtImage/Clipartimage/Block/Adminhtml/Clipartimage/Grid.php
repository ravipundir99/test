<?php

class ClipArtImage_Clipartimage_Block_Adminhtml_Clipartimage_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('clipartimageGrid');
      $this->setDefaultSort('clipartimage_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('clipartimage/clipartimage')->getCollection();
	   //we changed mysql query, we added inner join to order item table
        $collection->join('clipartcat/clipartcat', 'clipartcat_id=categoryId', array('title1'=>'title1'), null,'left');
   
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('clipartimage_id', array(
          'header'    => Mage::helper('clipartimage')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'clipartimage_id',
      ));

      $this->addColumn('title', array(
          'header'    => Mage::helper('clipartimage')->__('Title'),
          'align'     =>'left',
          'index'     => 'title',
      ));
	  
	  	
	 $this->addColumn('image', array(
            'header' =>Mage::helper('clipartimage')->__('Gallery Image'),
            'index'  =>'image',
            'align'  =>'center',
			'type'   =>'image',
			//'renderer' => 'ClipArtImage_Clipartimage_Block_Adminhtml_Clipartimage_Renderer_Image'
             
               
        )); 
	    
		 
	    $dbread = Mage::getSingleton('core/resource')->getConnection('core_read');
		$result = $dbread->query('SELECT * FROM clipartcat');
		
		$options = array();
		foreach($result->fetchAll() as $data){
			$options[$data['title1']] = $data['title1'];
		}
	   
	       $this->addColumn('title1', array(
            'header'    => Mage::helper('clipartcat')->__('Category Name'),
            'index'     => 'title1',
            'type' => 'options',
			'options'   => $options,
        ));

	  /*
      $this->addColumn('content', array(
			'header'    => Mage::helper('clipartimage')->__('Item Content'),
			'width'     => '150px',
			'index'     => 'content',
      ));
	  */

      $this->addColumn('status', array(
          'header'    => Mage::helper('clipartimage')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('clipartimage')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('clipartimage')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('clipartimage')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('clipartimage')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('clipartimage_id');
        $this->getMassactionBlock()->setFormFieldName('clipartimage');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('clipartimage')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('clipartimage')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('clipartimage/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('clipartimage')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('clipartimage')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}