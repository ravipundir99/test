<?php
class ClipArtImage_Clipartimage_Block_Adminhtml_Clipartimage extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_clipartimage';
    $this->_blockGroup = 'clipartimage';
    $this->_headerText = Mage::helper('clipartimage')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('clipartimage')->__('Add Item');
    parent::__construct();
  }
}