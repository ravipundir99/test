<?php
class ClipArtImage_Clipartimage_Block_Clipartimage extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getClipartimage()     
     { 
        if (!$this->hasData('clipartimage')) {
            $this->setData('clipartimage', Mage::registry('clipartimage'));
        }
        return $this->getData('clipartimage');
        
    }
}