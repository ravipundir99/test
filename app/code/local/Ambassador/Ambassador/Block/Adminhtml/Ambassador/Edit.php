<?php
	
class Ambassador_Ambassador_Block_Adminhtml_Ambassador_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "ambassador_id";
				$this->_blockGroup = "ambassador";
				$this->_controller = "adminhtml_ambassador";
				$this->_updateButton("save", "label", Mage::helper("ambassador")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("ambassador")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("ambassador")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);
				
				$this->_addButton("Download", array(
					"label"     => Mage::helper("ambassador")->__("Download File"),
					"onclick"   => "downlodimg()",//Mage::getModel(Mage_Core_Model_Store::URL_TYPE_WEB)
					"class"     => "save",
				), -100);
								$targetfolder = 'carrerAmbesder';
				$tid = Mage::registry("ambassador_data")->getId();
				$location = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)."downloadfile.php?dfile=".$this->_blockGroup."&did=".$tid."&folder=".$targetfolder;

				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
							function downlodimg(){
							location.href='$location';
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("ambassador_data") && Mage::registry("ambassador_data")->getId() ){

				    return Mage::helper("ambassador")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("ambassador_data")->getId()));

				} 
				else{

				     return Mage::helper("ambassador")->__("Add Item");

				}
		}
}