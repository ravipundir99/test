<?php

class Ambassador_Ambassador_Block_Adminhtml_Ambassador_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("ambassadorGrid");
				$this->setDefaultSort("ambassador_id");
				$this->setDefaultDir("ASC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("ambassador/ambassador")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("ambassador_id", array(
				"header" => Mage::helper("ambassador")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "ambassador_id",
				));
                
				$this->addColumn("name", array(
				"header" => Mage::helper("ambassador")->__("Name"),
				"index" => "name",
				));
				$this->addColumn("course", array(
				"header" => Mage::helper("ambassador")->__("Course"),
				"index" => "course",
				));
				$this->addColumn("mobile", array(
				"header" => Mage::helper("ambassador")->__("Mobile"),
				"index" => "mobile",
				));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('ambassador_id');
			$this->getMassactionBlock()->setFormFieldName('ambassador_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_ambassador', array(
					 'label'=> Mage::helper('ambassador')->__('Remove Ambassador'),
					 'url'  => $this->getUrl('*/adminhtml_ambassador/massRemove'),
					 'confirm' => Mage::helper('ambassador')->__('Are you sure?')
				));
			return $this;
		}
			

}