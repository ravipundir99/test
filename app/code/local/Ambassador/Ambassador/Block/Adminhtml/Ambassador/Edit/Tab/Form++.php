<?php
class Ambassador_Ambassador_Block_Adminhtml_Ambassador_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("ambassador_form", array("legend"=>Mage::helper("ambassador")->__("Item information")));

				
						$fieldset->addField("name", "text", array(
						"label" => Mage::helper("ambassador")->__("Name"),
						"name" => "name",
						));
					
						$fieldset->addField("course", "text", array(
						"label" => Mage::helper("ambassador")->__("Course"),
						"name" => "course",
						));
					
						$fieldset->addField("mobile", "text", array(
						"label" => Mage::helper("ambassador")->__("Mobile"),
						"name" => "mobile",
						));
					

				if (Mage::getSingleton("adminhtml/session")->getAmbassadorData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getAmbassadorData());
					Mage::getSingleton("adminhtml/session")->setAmbassadorData(null);
				} 
				elseif(Mage::registry("ambassador_data")) {
				    $form->setValues(Mage::registry("ambassador_data")->getData());
				}
				return parent::_prepareForm();
		}
}
