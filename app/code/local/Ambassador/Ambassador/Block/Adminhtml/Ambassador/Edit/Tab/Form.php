<?php
class Ambassador_Ambassador_Block_Adminhtml_Ambassador_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("ambassador_form", array("legend"=>Mage::helper("ambassador")->__("Item information")));

				
						$fieldset->addField("name", "text", array(
						"label" => Mage::helper("ambassador")->__("Name"),
						"name" => "name",
						));
						
						$fieldset->addField("collegename", "text", array(
						"label" => Mage::helper("ambassador")->__("College Name "),
						"name" => "collegename",
						));
						
						$fieldset->addField("course", "text", array(
						"label" => Mage::helper("ambassador")->__("Course"),
						"name" => "course",
						));
						
						$fieldset->addField("fathersname", "text", array(
						"label" => Mage::helper("ambassador")->__("Fathers Name "),
						"name" => "fathersname",
						));
						
						$fieldset->addField("mobile", "text", array(
						"label" => Mage::helper("ambassador")->__("Mobile"),
						"name" => "mobile",
						));
						
						$fieldset->addField("hometelno", "text", array(
						"label" => Mage::helper("ambassador")->__("Home Tel. No "),
						"name" => "hometelno",
						));
						
						$fieldset->addField("localaddress", "text", array(
						"label" => Mage::helper("ambassador")->__("Local Address"),
						"name" => "localaddress",
						));
						
						$fieldset->addField("localcity", "text", array(
						"label" => Mage::helper("ambassador")->__("City "),
						"name" => "localcity",
						));
						
						$fieldset->addField("lastate", "text", array(
						"label" => Mage::helper("ambassador")->__("State "),
						"name" => "lastate",
						));
						
						$fieldset->addField("localpincode", "text", array(
						"label" => Mage::helper("ambassador")->__("Pin Code "),
						"name" => "localpincode",
						));
						
						$fieldset->addField("localphoneno", "text", array(
						"label" => Mage::helper("ambassador")->__("Phone No "),
						"name" => "localphoneno",
						));
						
						$fieldset->addField("emailid", "text", array(
						"label" => Mage::helper("ambassador")->__("Email ID"),
						"name" => "emailid",
						));
						
						$fieldset->addField("permanentaddress", "text", array(
						"label" => Mage::helper("ambassador")->__("Permanent Address"),
						"name" => "permanentaddress",
						));
						
						$fieldset->addField("permanentcity", "text", array(
						"label" => Mage::helper("ambassador")->__("City "),
						"name" => "permanentcity",
						));
						
						$fieldset->addField("pastate", "text", array(
						"label" => Mage::helper("ambassador")->__("State "),
						"name" => "pastate",
						));
						
						$fieldset->addField("permanentpincode", "text", array(
						"label" => Mage::helper("ambassador")->__("Pin Code "),
						"name" => "permanentpincode",
						));
						
						$fieldset->addField("permanentphonepo", "text", array(
						"label" => Mage::helper("ambassador")->__("Phone No."),
						"name" => "permanentphonepo",
						));
						$fieldset->addField("hobbies", "text", array(
						"label" => Mage::helper("ambassador")->__("Hobbies "),
						"name" => "hobbies",
						));
						
						$fieldset->addField("facebookid", "text", array(
						"label" => Mage::helper("ambassador")->__("Facebook ID "),
						"name" => "facebookid",
						));
						
						/*$fieldset->addField('photo', 'file', array(
						 'label'     => Mage::helper('ambassador')->__('File'),
						'required'  => false,
						'name'      => 'photo',
						));*/
					

				if (Mage::getSingleton("adminhtml/session")->getAmbassadorData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getAmbassadorData());
					Mage::getSingleton("adminhtml/session")->setAmbassadorData(null);
				} 
				elseif(Mage::registry("ambassador_data")) {
				    $form->setValues(Mage::registry("ambassador_data")->getData());
				}
				return parent::_prepareForm();
		}
}
