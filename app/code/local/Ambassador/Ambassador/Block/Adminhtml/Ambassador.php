<?php


class Ambassador_Ambassador_Block_Adminhtml_Ambassador extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_ambassador";
	$this->_blockGroup = "ambassador";
	$this->_headerText = Mage::helper("ambassador")->__("Ambassador Manager");
	$this->_addButtonLabel = Mage::helper("ambassador")->__("Add New Item");
	parent::__construct();
	
	}

}