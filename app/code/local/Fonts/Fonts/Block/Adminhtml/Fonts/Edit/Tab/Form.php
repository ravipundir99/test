<?php

class Fonts_Fonts_Block_Adminhtml_Fonts_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('fonts_form', array('legend'=>Mage::helper('fonts')->__('Item information')));
		$dbread = Mage::getSingleton('core/resource')->getConnection('core_read');
		$result = $dbread->query('SELECT * FROM fontcategory where status = 1');
		
		$options = array();
		foreach($result->fetchAll() as $data){
			$options[] = array('label'=>$data['name'],'value'=>$data['fontcategory_id']);
		}		
	  
	 $fieldset->addField('title1', 'text', array(
          'label'     => Mage::helper('fonts')->__('Title'),
          'required'  => true,
          'name'      => 'title1',
      ));
	  
	  $fieldset->addField('category', 'select', array(
          'label'     => Mage::helper('fonts')->__('Category'),
          'name'      => 'category',
          'values'    => $options,
      ));
	  
       $fieldset->addField('normal', 'file', array(
          'label'     => Mage::helper('fonts')->__('File ttf(Normal)'),
          'required'  => false,
          'name'      => 'normal',
      ));
	  
	$fieldset->addField('bold', 'file', array(
          'label'     => Mage::helper('fonts')->__('File ttf (Bold)'),
          'required'  => false,
          'name'      => 'bold',
	  ));
	  
	  $fieldset->addField('italic', 'file', array(
          'label'     => Mage::helper('fonts')->__('File ttf (Italic)'),
          'required'  => false,
		  'value'	  => '1',
          'name'      => 'italic',
      ));
	  $fieldset->addField('bolditalic', 'file', array(
          'label'     => Mage::helper('fonts')->__('File ttf (Bold Italic)'),
          'required'  => false,
		  'value'	  => '1',
          'name'      => 'bolditalic',
      ));
	  $fieldset->addField('style', 'text', array(
          'label'     => Mage::helper('fonts')->__('Style'),
          'required'  => false,
		  'value'	  => '1',
          'name'      => 'style',
      ));
	  
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('fonts')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('fonts')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('fonts')->__('Disabled'),
              ),
          ),
      ));
      
      /*$fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('fonts')->__('Content'),
          'title'     => Mage::helper('fonts')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
          'required'  => true,
      ));*/
     
      if ( Mage::getSingleton('adminhtml/session')->getFontsData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getFontsData());
          Mage::getSingleton('adminhtml/session')->setFontsData(null);
      } elseif ( Mage::registry('fonts_data') ) {
          $form->setValues(Mage::registry('fonts_data')->getData());
      }
      return parent::_prepareForm();
  }
}