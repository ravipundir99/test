<?php

class Fonts_Fonts_Block_Adminhtml_Fonts_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'fonts';
        $this->_controller = 'adminhtml_fonts';
        
        $this->_updateButton('save', 'label', Mage::helper('fonts')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('fonts')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('fonts_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'fonts_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'fonts_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('fonts_data') && Mage::registry('fonts_data')->getId() ) {
            return Mage::helper('fonts')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('fonts_data')->getTitle()));
        } else {
            return Mage::helper('fonts')->__('Add Item');
        }
    }
}