<?php

class Fonts_Fonts_Model_Mysql4_Fonts_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('fonts/fonts');
    }
}