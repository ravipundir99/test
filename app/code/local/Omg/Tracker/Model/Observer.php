<?php
class Omg_Tracker_Model_Observer
{
    /**
     * Check for inbound affiliate link data
     *      To set cookie for selective tracking pixel display on success.phtml
     * @param type $observer
     */
    public function inboundAffiliateTracking($observer)
    {
        $request = $observer->getEvent()->getData('front')->getRequest();
        if ( isset($request->utm_source) ){
            //using Google Analytics tracking source variable it identify affilaite network
            if ( 'OMG' == $request->utm_source ){
                Mage::getModel('core/cookie')->set('affiliatenetwork', $request->utm_source, 86400*45);//45 day cookie
            }
			elseif ( 'vcom' == $request->utm_source ){
                Mage::getModel('core/cookie')->set('affiliatenetwork', $request->utm_source, 3600,'/');//one hour
            }
			
			elseif ( 'coupondunia' == $request->utm_source ){
                Mage::getModel('core/cookie')->set('affiliatenetwork', $request->utm_source, 86400*45);//45 day cookie
            }
			
			elseif ( 'cuelinks' == $request->utm_source ){
                Mage::getModel('core/cookie')->set('affiliatenetwork', $request->utm_source, 86400*45);//45 day cookie
            }
			elseif ( 'freekaamaal' == $request->utm_source ){
                Mage::getModel('core/cookie')->set('affiliatenetwork', $request->utm_source, 86400*45);//45 day cookie
            }
			
						
			elseif ( 'icubes' == $request->utm_source ){
                Mage::getModel('core/cookie')->set('affiliatenetwork', $request->utm_source, 86400*45);//45 day cookie
            }
			
			elseif ( 'getit' == $request->utm_source ){
                Mage::getModel('core/cookie')->set('affiliatenetwork', $request->utm_source, 86400*45);//45 day cookie
            }
	              elseif ( 'arthsalutions' == $request->utm_source ){
                Mage::getModel('core/cookie')->set('affiliatenetwork', $request->utm_source, 86400*45);//45 day cookie
            }
                     elseif ( 'networkplay' == $request->utm_source ){
                Mage::getModel('core/cookie')->set('affiliatenetwork', $request->utm_source, 3600,'/');//one hour
            }

        }
    }
//end class   
}
?>