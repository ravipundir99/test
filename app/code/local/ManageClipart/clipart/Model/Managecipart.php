<?php
/**
 * ManageClipart_clipart extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	ManageClipart
 * @package		ManageClipart_clipart
 * @copyright  	Copyright (c) 2012
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Manage Clipart model
 *
 * @category	ManageClipart
 * @package		ManageClipart_clipart
 * @author Ultimate Module Creator
 */
class ManageClipart_clipart_Model_Managecipart extends Mage_Core_Model_Abstract{
	/**
	 * constructor
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function _construct(){
		parent::_construct();
		$this->_init('clipart/managecipart');
	}
	/**
	 * before save manage clipart
	 * @access protected
	 * @return ManageClipart_clipart_Model_Managecipart
	 * @author Ultimate Module Creator
	 */
	protected function _beforeSave(){
		parent::_beforeSave();
		$now = Mage::getSingleton('core/date')->gmtDate();
		if ($this->isObjectNew()){
			$this->setCreatedAt($now);
		}
		$this->setUpdatedAt($now);
		return $this;
	}
}