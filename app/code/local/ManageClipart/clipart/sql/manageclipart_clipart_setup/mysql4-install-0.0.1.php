<?php
/**
 * ManageClipart_clipart extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	ManageClipart
 * @package		ManageClipart_clipart
 * @copyright  	Copyright (c) 2012
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * clipart module install script
 *
 * @category	ManageClipart
 * @package		ManageClipart_clipart
 * @author Ultimate Module Creator
 */
$this->startSetup();
$this->run("
	CREATE TABLE IF NOT EXISTS `{$this->getTable('clipart/managecipart')}` (
		`entity_id` int(10) unsigned NOT NULL auto_increment,
		`title` varchar(255) NOT NULL default '',
		`clipartimage` varchar(255) NULL default '',
		`category` tinyint(1) NOT NULL default '1',
		`cast` int(10) NULL default '0',
		`status` tinyint(1) NULL default '1',
		`created_at` datetime NULL,
		`updated_at` datetime NULL,
	PRIMARY KEY (`entity_id`)) 
	ENGINE=InnoDB DEFAULT CHARSET=utf8;
"); 
$this->endSetup();
