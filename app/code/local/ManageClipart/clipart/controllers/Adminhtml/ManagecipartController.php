<?php
/**
 * ManageClipart_clipart extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	ManageClipart
 * @package		ManageClipart_clipart
 * @copyright  	Copyright (c) 2012
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Manage Clipart admin controller
 *
 * @category	ManageClipart
 * @package		ManageClipart_clipart
 * @author Ultimate Module Creator
 */
class ManageClipart_clipart_Adminhtml_ManagecipartController extends ManageClipart_clipart_Controller_Adminhtml_clipart{
 	/**
	 * default action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function indexAction() {
		$this->loadLayout()->renderLayout();
	}
	/**
	 * grid action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function gridAction() {
		$this->loadLayout()->renderLayout();
	}
	/**
	 * edit manage clipart - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function editAction() {
		$managecipartId	= $this->getRequest()->getParam('id');
		$managecipart  	= Mage::getModel('clipart/managecipart')->load($managecipartId);
		if ($managecipartId && !$managecipart->getId()) {
			$this->_getSession()->addError(Mage::helper('clipart')->__('This manage clipart no longer exists.'));
			$this->_redirect('*/*/');
			return;
		}
		$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
		if (!empty($data)) {
			$managecipart->setData($data);
		}
		Mage::register('managecipart_data', $managecipart);
		$this->loadLayout();
		if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) { 
			$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true); 
		}
		$this->renderLayout();
	}
	/**
	 * new manage clipart action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function newAction() {
		$this->_forward('edit');
	}
	/**
	 * save manage clipart - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
			try {
				$managecipart = Mage::getModel('clipart/managecipart');		
				$managecipart->setData($data)->setId($this->getRequest()->getParam('id'));
				$clipartimageName = $this->_uploadAndGetName('clipartimage', Mage::helper('clipart/managecipart_image')->getImageBaseDir(), $data);
				$managecipart->setData('clipartimage', $clipartimageName);
				$managecipart->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('clipart')->__('Manage Clipart was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);
				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $managecipart->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
			} 
			catch (Mage_Core_Exception $e){
				if (isset($data['clipartimage']['value'])){
					$data['clipartimage'] = $data['clipartimage']['value'];
				}
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setFormData($data);
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
			catch (Exception $e) {
				if (isset($data['clipartimage']['value'])){
					$data['clipartimage'] = $data['clipartimage']['value'];
				}
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('clipart')->__('There was a problem saving the manage clipart.'));
				Mage::getSingleton('adminhtml/session')->setFormData($data);
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('clipart')->__('Unable to find manage clipart to save.'));
		$this->_redirect('*/*/');
	}
	/**
	 * delete manage clipart - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0) {
			try {
				$managecipart = Mage::getModel('clipart/managecipart');
				$managecipart->setId($this->getRequest()->getParam('id'))->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('clipart')->__('Manage Clipart was successfully deleted.'));
				$this->_redirect('*/*/');
				return; 
			}
			catch (Mage_Core_Exception $e){
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
			catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('clipart')->__('There was an error deleteing manage clipart.'));
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('clipart')->__('Could not find manage clipart to delete.'));
		$this->_redirect('*/*/');
	}
	/**
	 * mass delete manage clipart - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function massDeleteAction() {
		$managecipartIds = $this->getRequest()->getParam('managecipart');
		if(!is_array($managecipartIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('clipart')->__('Please select managecipart to delete.'));
		}
		else {
			try {
				foreach ($managecipartIds as $managecipartId) {
					$managecipart = Mage::getModel('clipart/managecipart');
					$managecipart->setId($managecipartId)->delete();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('clipart')->__('Total of %d managecipart were successfully deleted.', count($managecipartIds)));
			}
			catch (Mage_Core_Exception $e){
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
			catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('clipart')->__('There was an error deleteing managecipart.'));
			}
		}
		$this->_redirect('*/*/index');
	}
	/**
	 * mass status change - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function massStatusAction(){
		$managecipartIds = $this->getRequest()->getParam('managecipart');
		if(!is_array($managecipartIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('clipart')->__('Please select managecipart.'));
		} 
		else {
			try {
				foreach ($managecipartIds as $managecipartId) {
				$managecipart = Mage::getSingleton('clipart/managecipart')->load($managecipartId)
							->setStatus($this->getRequest()->getParam('status'))
							->setIsMassupdate(true)
							->save();
				}
				$this->_getSession()->addSuccess($this->__('Total of %d managecipart were successfully updated.', count($managecipartIds)));
			}
			catch (Mage_Core_Exception $e){
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
			catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('clipart')->__('There was an error updating managecipart.'));
			}
		}
		$this->_redirect('*/*/index');
	}
	/**
	 * mass Category change - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function massCategoryAction(){
		$managecipartIds = $this->getRequest()->getParam('managecipart');
		if(!is_array($managecipartIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('clipart')->__('Please select managecipart.'));
		} 
		else {
			try {
				foreach ($managecipartIds as $managecipartId) {
				$managecipart = Mage::getSingleton('clipart/managecipart')->load($managecipartId)
							->setCategory($this->getRequest()->getParam('flag_category'))
							->setIsMassupdate(true)
							->save();
				}
				$this->_getSession()->addSuccess($this->__('Total of %d managecipart were successfully updated.', count($managecipartIds)));
			}
			catch (Mage_Core_Exception $e){
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
			catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('clipart')->__('There was an error updating managecipart.'));
			}
		}
		$this->_redirect('*/*/index');
	}
	/**
	 * export as csv - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function exportCsvAction(){
		$fileName   = 'managecipart.csv';
		$content	= $this->getLayout()->createBlock('clipart/adminhtml_managecipart_grid')->getCsv();
		$this->_prepareDownloadResponse($fileName, $content);
	}
	/**
	 * export as MsExcel - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function exportExcelAction(){
		$fileName   = 'managecipart.xls';
		$content	= $this->getLayout()->createBlock('clipart/adminhtml_managecipart_grid')->getExcelFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}
	/**
	 * export as xml - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function exportXmlAction(){
		$fileName   = 'managecipart.xml';
		$content	= $this->getLayout()->createBlock('clipart/adminhtml_managecipart_grid')->getXml();
		$this->_prepareDownloadResponse($fileName, $content);
	}
}