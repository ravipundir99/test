<?php
/**
 * ManageClipart_clipart extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	ManageClipart
 * @package		ManageClipart_clipart
 * @copyright  	Copyright (c) 2012
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Manage Clipart admin edit tabs
 *
 * @category	ManageClipart
 * @package		ManageClipart_clipart
 * @author Ultimate Module Creator
 */
class ManageClipart_clipart_Block_Adminhtml_Managecipart_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs{
	/**
	 * constructor
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function __construct(){
		parent::__construct();
		$this->setId('managecipart_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('clipart')->__('Manage Clipart'));
	}
	/**
	 * before render html
	 * @access protected
	 * @return ManageClipart_clipart_Block_Adminhtml_Managecipart_Edit_Tabs
	 * @author Ultimate Module Creator
	 */
	protected function _beforeToHtml(){
		$this->addTab('form_section', array(
			'label'		=> Mage::helper('clipart')->__('Manage Clipart'),
			'title'		=> Mage::helper('clipart')->__('Manage Clipart'),
			'content' 	=> $this->getLayout()->createBlock('clipart/adminhtml_managecipart_edit_tab_form')->toHtml(),
		));
		return parent::_beforeToHtml();
	}
}