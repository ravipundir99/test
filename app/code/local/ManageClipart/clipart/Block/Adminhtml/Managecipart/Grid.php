<?php
/**
 * ManageClipart_clipart extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	ManageClipart
 * @package		ManageClipart_clipart
 * @copyright  	Copyright (c) 2012
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Manage Clipart admin grid block
 *
 * @category	ManageClipart
 * @package		ManageClipart_clipart
 * @author Ultimate Module Creator
 */
class ManageClipart_clipart_Block_Adminhtml_Managecipart_Grid extends Mage_Adminhtml_Block_Widget_Grid{
	/**
	 * constructor
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function __construct(){
		parent::__construct();
		$this->setId('managecipartGrid');
		$this->setDefaultSort('entity_id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
		$this->setUseAjax(true);
	}
	/**
	 * prepare collection
	 * @access protected
	 * @return ManageClipart_clipart_Block_Adminhtml_Managecipart_Grid
	 * @author Ultimate Module Creator
	 */
	protected function _prepareCollection(){
		$collection = Mage::getModel('clipart/managecipart')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}
	/**
	 * prepare grid collection
	 * @access protected
	 * @return ManageClipart_clipart_Block_Adminhtml_Managecipart_Grid
	 * @author Ultimate Module Creator
	 */
	protected function _prepareColumns(){
		$this->addColumn('entity_id', array(
			'header'	=> Mage::helper('clipart')->__('Id'),
			'index'		=> 'entity_id',
			'type'		=> 'number'
		));
		$this->addColumn('title', array(
			'header'=> Mage::helper('clipart')->__('Title'),
			'index' => 'title',
			'type'	 	=> 'text',

		));
		$this->addColumn('status', array(
			'header'	=> Mage::helper('clipart')->__('Status'),
			'index'		=> 'status',
			'type'		=> 'options',
			'options'	=> array(
				'1' => Mage::helper('clipart')->__('Enabled'),
				'0' => Mage::helper('clipart')->__('Disabled'),
			)
		));
		$this->addColumn('created_at', array(
			'header'	=> Mage::helper('clipart')->__('Created at'),
			'index' 	=> 'created_at',
			'width' 	=> '120px',
			'type'  	=> 'datetime',
		));
		$this->addColumn('updated_at', array(
			'header'	=> Mage::helper('clipart')->__('Updated at'),
			'index' 	=> 'updated_at',
			'width' 	=> '120px',
			'type'  	=> 'datetime',
		));
		$this->addColumn('action',
			array(
				'header'=>  Mage::helper('clipart')->__('Action'),
				'width' => '100',
				'type'  => 'action',
				'getter'=> 'getId',
				'actions'   => array(
					array(
						'caption'   => Mage::helper('clipart')->__('Edit'),
						'url'   => array('base'=> '*/*/edit'),
						'field' => 'id'
					)
				),
				'filter'=> false,
				'is_system'	=> true,
				'sortable'  => false,
		));
		$this->addExportType('*/*/exportCsv', Mage::helper('clipart')->__('CSV'));
		$this->addExportType('*/*/exportExcel', Mage::helper('clipart')->__('Excel'));
		$this->addExportType('*/*/exportXml', Mage::helper('clipart')->__('XML'));
		return parent::_prepareColumns();
	}	/**
	 * prepare mass action
	 * @access protected
	 * @return ManageClipart_clipart_Block_Adminhtml_Managecipart_Grid
	 * @author Ultimate Module Creator
	 */
	protected function _prepareMassaction(){
		$this->setMassactionIdField('entity_id');
		$this->getMassactionBlock()->setFormFieldName('managecipart');
		$this->getMassactionBlock()->addItem('delete', array(
			'label'=> Mage::helper('clipart')->__('Delete'),
			'url'  => $this->getUrl('*/*/massDelete'),
			'confirm'  => Mage::helper('clipart')->__('Are you sure?')
		));
		$this->getMassactionBlock()->addItem('status', array(
			'label'=> Mage::helper('clipart')->__('Change status'),
			'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
			'additional' => array(
				'status' => array(
						'name' => 'status',
						'type' => 'select',
						'class' => 'required-entry',
						'label' => Mage::helper('clipart')->__('Status'),
						'values' => array(
								'1' => Mage::helper('clipart')->__('Enabled'),
								'0' => Mage::helper('clipart')->__('Disabled'),
						)
				)
			)
		));
		$this->getMassactionBlock()->addItem('category', array(
			'label'=> Mage::helper('clipart')->__('Change Category'),
			'url'  => $this->getUrl('*/*/massCategory', array('_current'=>true)),
			'additional' => array(
				'flag_category' => array(
						'name' => 'flag_category',
						'type' => 'select',
						'class' => 'required-entry',
						'label' => Mage::helper('clipart')->__('Category'),
						'values' => array(
								'1' => Mage::helper('clipart')->__('Yes'),
								'0' => Mage::helper('clipart')->__('No'),
						)
				)
			)
		));
		return $this;
	}
	/**
	 * get the row url
	 * @access public
	 * @param ManageClipart_clipart_Model_Managecipart
	 * @return string
	 * @author Ultimate Module Creator
	 */
	public function getRowUrl($row){
		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}
	/**
	 * get the grid url
	 * @access public
	 * @return string
	 * @author Ultimate Module Creator
	 */
	public function getGridUrl(){
		return $this->getUrl('*/*/grid', array('_current'=>true));
	}
}