<?php
/**
 * ManageClipart_clipart extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	ManageClipart
 * @package		ManageClipart_clipart
 * @copyright  	Copyright (c) 2012
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Manage Clipart edit form tab
 *
 * @category	ManageClipart
 * @package		ManageClipart_clipart
 * @author Ultimate Module Creator
 */
class ManageClipart_clipart_Block_Adminhtml_Managecipart_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form{	
	/**
	 * prepare the form
	 * @access protected
	 * @return clipart_Managecipart_Block_Adminhtml_Managecipart_Edit_Tab_Form
	 * @author Ultimate Module Creator
	 */
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('managecipart_form', array('legend'=>Mage::helper('clipart')->__('Manage Clipart')));
		$fieldset->addType('image', Mage::getConfig()->getBlockClassName('clipart/adminhtml_managecipart_helper_image'));
		$wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();

		$fieldset->addField('title', 'text', array(
			'label' => Mage::helper('clipart')->__('Title'),
			'name'  => 'title',
			'required'  => true,
			'class' => 'required-entry',


		));

		$fieldset->addField('clipartimage', 'image', array(
			'label' => Mage::helper('clipart')->__('Clipart Image'),
			'name'  => 'clipartimage',


		));

		$fieldset->addField('category', 'select', array(
			'label' => Mage::helper('clipart')->__('Category'),
			'name'  => 'category',
			'required'  => true,
			'class' => 'required-entry',

			'values'=> array(
				array(
					'value' => 1,
					'label' => Mage::helper('clipart')->__('Yes'),
				),
				array(
					'value' => 0,
					'label' => Mage::helper('clipart')->__('No'),
				),
			),

		));

		$fieldset->addField('cast', 'text', array(
			'label' => Mage::helper('clipart')->__('Cast'),
			'name'  => 'cast',


		));
		$fieldset->addField('status', 'select', array(
			'label' => Mage::helper('clipart')->__('Status'),
			'name'  => 'status',
			'values'=> array(
				array(
					'value' => 1,
					'label' => Mage::helper('clipart')->__('Enabled'),
				),
				array(
					'value' => 0,
					'label' => Mage::helper('clipart')->__('Disabled'),
				),
			),
		));
		if (Mage::getSingleton('adminhtml/session')->getManagecipartData()){
			$form->setValues(Mage::getSingleton('adminhtml/session')->getManagecipartData());
			Mage::getSingleton('adminhtml/session')->setManagecipartData(null);
		}
		elseif (Mage::registry('managecipart_data')){
			$form->setValues(Mage::registry('managecipart_data')->getData());
		}
		return parent::_prepareForm();
	}
}