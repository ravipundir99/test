<?php
/**
 * ManageClipart_clipart extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	ManageClipart
 * @package		ManageClipart_clipart
 * @copyright  	Copyright (c) 2012
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Manage Clipart admin edit block
 *
 * @category	ManageClipart
 * @package		ManageClipart_clipart
 * @author Ultimate Module Creator
 */
class ManageClipart_clipart_Block_Adminhtml_Managecipart_Edit extends Mage_Adminhtml_Block_Widget_Form_Container{
	/**
	 * constuctor
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function __construct(){
		parent::__construct();
		$this->_blockGroup = 'clipart';
		$this->_controller = 'adminhtml_managecipart';
		$this->_updateButton('save', 'label', Mage::helper('clipart')->__('Save Manage Clipart'));
		$this->_updateButton('delete', 'label', Mage::helper('clipart')->__('Delete Manage Clipart'));
		$this->_addButton('saveandcontinue', array(
			'label'		=> Mage::helper('clipart')->__('Save And Continue Edit'),
			'onclick'	=> 'saveAndContinueEdit()',
			'class'		=> 'save',
		), -100);
		$this->_formScripts[] = "
			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
	}
	/**
	 * get the edit form header
	 * @access public
	 * @return string
	 * @author@author Ultimate Module Creator
	 */
	public function getHeaderText(){
		if( Mage::registry('managecipart_data') && Mage::registry('managecipart_data')->getId() ) {
			return Mage::helper('clipart')->__("Edit Manage Clipart '%s'", $this->htmlEscape(Mage::registry('managecipart_data')->getTitle()));
		} 
		else {
			return Mage::helper('clipart')->__('Add Manage Clipart');
		}
	}
}