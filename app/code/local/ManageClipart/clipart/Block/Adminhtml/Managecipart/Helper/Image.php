<?php
/**
 * ManageClipart_clipart extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	ManageClipart
 * @package		ManageClipart_clipart
 * @copyright  	Copyright (c) 2012
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Manage Clipart image field renderer helper
 *
 * @category	ManageClipart
 * @package		ManageClipart_clipart
 * @author Ultimate Module Creator
 */
class ManageClipart_clipart_Block_Adminhtml_Managecipart_Helper_Image extends Varien_Data_Form_Element_Image{
	/**
	 * get the url of the image
	 * @access protected
	 * @return string
	 * @author Ultimate Module Creator
	 */
	protected function _getUrl(){
		$url = false;
		if ($this->getValue()) {
			$url = Mage::helper('clipart/managecipart_image')->getImageBaseUrl().$this->getValue();
		}
		return $url;
	}
}
 