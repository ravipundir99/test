<?php
class Designer_Designertool_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      
	  $this->loadLayout();   
	  if(isset($_REQUEST['productId'])){		$product = Mage::getModel('catalog/product')->load($_REQUEST['productId']);				  $this->getLayout()->getBlock("head")->setTitle($this->__($product->getName()));		}else{		 $this->getLayout()->getBlock("head")->setTitle($this->__('Designer'));		}
	        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
		   ));

      $breadcrumbs->addCrumb("titlename", array(
                "label" => $this->__("Designer"),
                "title" => $this->__("Designer")
		   ));

      $this->renderLayout(); 
	  
    }
}