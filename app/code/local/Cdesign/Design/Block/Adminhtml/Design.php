<?php


class Cdesign_Design_Block_Adminhtml_Design extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_design";
	$this->_blockGroup = "design";
	$this->_headerText = Mage::helper("design")->__("Design Manager");
	$this->_addButtonLabel = Mage::helper("design")->__("Add New Item");
	parent::__construct();

	}

}