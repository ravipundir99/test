<?php

class Cdesign_Design_Block_Adminhtml_Design_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("designGrid");
				$this->setDefaultSort("design_id");
				$this->setDefaultDir("ASC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("design/design")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("design_id", array(
				"header" => Mage::helper("design")->__("ID"),
				"align" =>"right",
				"width" => "50px",
				"index" => "design_id",
				));
				$this->addColumn("name", array(
				"header" => Mage::helper("design")->__("Design Name"),
				"align" =>"left",
				"index" => "name",
				));


				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}

}