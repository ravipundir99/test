<?php
class Cdesign_Design_Block_Adminhtml_Design_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("design_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("design")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("design")->__("Item Information"),
				"title" => Mage::helper("design")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("design/adminhtml_design_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
