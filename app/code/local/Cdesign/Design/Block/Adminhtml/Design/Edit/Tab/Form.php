<?php
class Cdesign_Design_Block_Adminhtml_Design_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("design_form", array("legend"=>Mage::helper("design")->__("Item information")));

				$fieldset->addField("name", "text", array(
				"label" => Mage::helper("design")->__("Design Name"),
				"class" => "required-entry",
				"required" => true,
				"name" => "name",
				));




				if (Mage::getSingleton("adminhtml/session")->getDesignData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getDesignData());
					Mage::getSingleton("adminhtml/session")->setDesignData(null);
				} 
				elseif(Mage::registry("design_data")) {
				    $form->setValues(Mage::registry("design_data")->getData());
				}
				return parent::_prepareForm();
		}
}
