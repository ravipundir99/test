<?php
	
class Cdesign_Design_Block_Adminhtml_Design_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "design_id";
				$this->_blockGroup = "design";
				$this->_controller = "adminhtml_design";
				$this->_updateButton("save", "label", Mage::helper("design")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("design")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("design")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("design_data") && Mage::registry("design_data")->getId() ){

				    return Mage::helper("design")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("design_data")->getName()));

				} 
				else{

				     return Mage::helper("design")->__("Add Item");

				}
		}
}