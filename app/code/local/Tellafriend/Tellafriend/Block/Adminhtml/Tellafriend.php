<?php


class Tellafriend_Tellafriend_Block_Adminhtml_Tellafriend extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_tellafriend";
	$this->_blockGroup = "tellafriend";
	$this->_headerText = Mage::helper("tellafriend")->__("Tellafriend Manager");
	$this->_addButtonLabel = Mage::helper("tellafriend")->__("Add New Item");
	parent::__construct();
	
	}

}