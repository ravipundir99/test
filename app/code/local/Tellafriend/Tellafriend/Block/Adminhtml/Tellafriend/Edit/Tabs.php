<?php
class Tellafriend_Tellafriend_Block_Adminhtml_Tellafriend_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("tellafriend_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("tellafriend")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("tellafriend")->__("Item Information"),
				"title" => Mage::helper("tellafriend")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("tellafriend/adminhtml_tellafriend_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
