<?php
	
class Tellafriend_Tellafriend_Block_Adminhtml_Tellafriend_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "tellafriend_id";
				$this->_blockGroup = "tellafriend";
				$this->_controller = "adminhtml_tellafriend";
				$this->_updateButton("save", "label", Mage::helper("tellafriend")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("tellafriend")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("tellafriend")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("tellafriend_data") && Mage::registry("tellafriend_data")->getId() ){

				    return Mage::helper("tellafriend")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("tellafriend_data")->getId()));

				} 
				else{

				     return Mage::helper("tellafriend")->__("Add Item");

				}
		}
}