<?php
class Tellafriend_Tellafriend_Block_Adminhtml_Tellafriend_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("tellafriend_form", array("legend"=>Mage::helper("tellafriend")->__("Item information")));

				
						$fieldset->addField("name", "text", array(
						"label" => Mage::helper("tellafriend")->__("Name"),
						"name" => "name",
						));
					
						$fieldset->addField("friebdsemail", "text", array(
						"label" => Mage::helper("tellafriend")->__("Friends Email"),
						"name" => "friebdsemail",
						));
					
						$fieldset->addField("message", "editor", array(
						"label" => Mage::helper("tellafriend")->__("Message"),
					    'title'     => Mage::helper('tellafriend')->__('Message'),
					    'style'     => 'width:400px; height:200px;',
					    'wysiwyg'   => false,
					    'required'  => true,
						"name" => "message"
						));
					

				if (Mage::getSingleton("adminhtml/session")->getTellafriendData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getTellafriendData());
					Mage::getSingleton("adminhtml/session")->setTellafriendData(null);
				} 
				elseif(Mage::registry("tellafriend_data")) {
				    $form->setValues(Mage::registry("tellafriend_data")->getData());
				}
				return parent::_prepareForm();
		}
}
