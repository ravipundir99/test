<?php

class Tellafriend_Tellafriend_Block_Adminhtml_Tellafriend_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("tellafriendGrid");
				$this->setDefaultSort("tellafriend_id");
				$this->setDefaultDir("ASC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("tellafriend/tellafriend")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("tellafriend_id", array(
				"header" => Mage::helper("tellafriend")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "tellafriend_id",
				));
                
				$this->addColumn("name", array(
				"header" => Mage::helper("tellafriend")->__("Name"),
				"index" => "name",
				));
				$this->addColumn("friebdsemail", array(
				"header" => Mage::helper("tellafriend")->__("Friends Email"),
				"index" => "friebdsemail",
				));
				$this->addColumn("message", array(
				"header" => Mage::helper("tellafriend")->__("Message"),
				"index" => "message",
				));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('tellafriend_id');
			$this->getMassactionBlock()->setFormFieldName('tellafriend_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_tellafriend', array(
					 'label'=> Mage::helper('tellafriend')->__('Remove Tellafriend'),
					 'url'  => $this->getUrl('*/adminhtml_tellafriend/massRemove'),
					 'confirm' => Mage::helper('tellafriend')->__('Are you sure?')
				));
			return $this;
		}
			

}