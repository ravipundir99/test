<?php


class Bulkorder_Bulkorder_Block_Adminhtml_Bulkorder extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_bulkorder";
	$this->_blockGroup = "bulkorder";
	$this->_headerText = Mage::helper("bulkorder")->__("Bulkorder Manager");
	$this->_addButtonLabel = Mage::helper("bulkorder")->__("Add New Item");
	parent::__construct();

	}

}