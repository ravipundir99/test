<?php
class Bulkorder_Bulkorder_Block_Adminhtml_Bulkorder_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("bulkorder_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("bulkorder")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("bulkorder")->__("Item Information"),
				"title" => Mage::helper("bulkorder")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("bulkorder/adminhtml_bulkorder_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
