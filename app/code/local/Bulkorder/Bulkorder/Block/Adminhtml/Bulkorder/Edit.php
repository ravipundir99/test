<?php
	
class Bulkorder_Bulkorder_Block_Adminhtml_Bulkorder_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "bulkorder_id";
				$this->_blockGroup = "bulkorder";
				$this->_controller = "adminhtml_bulkorder";
				$this->_updateButton("save", "label", Mage::helper("bulkorder")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("bulkorder")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("bulkorder")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);

				
				/*$this->_addButton("Download", array(
					"label"     => Mage::helper("bulkorder")->__("Download File"),
					"onclick"   => "downlodimg()",//Mage::getModel(Mage_Core_Model_Store::URL_TYPE_WEB)
					"class"     => "save",
				), -100);*/
				
				$tid = Mage::registry("bulkorder_data")->getId();				
				$targetfolder = 'bulkorder';
				//$location = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)."downloadfile.php?dfile=".$this->_blockGroup."&did=".$tid."&folder=".$targetfolder;
				

				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
							
							function downlodimg(){
							location.href='$location';
							}
							
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("bulkorder_data") && Mage::registry("bulkorder_data")->getId() ){

				    return Mage::helper("bulkorder")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("bulkorder_data")->getName()));

				} 
				else{

				     return Mage::helper("bulkorder")->__("Add Item");

				}
		}
}