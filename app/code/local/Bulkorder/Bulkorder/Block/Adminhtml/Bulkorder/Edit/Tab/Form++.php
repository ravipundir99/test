<?php
class Bulkorder_Bulkorder_Block_Adminhtml_Bulkorder_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("bulkorder_form", array("legend"=>Mage::helper("bulkorder")->__("Item information")));

				$fieldset->addField("name", "text", array(
				"label" => Mage::helper("bulkorder")->__("Bulkorder Name"),
				"class" => "required-entry",
				"required" => true,
				"name" => "name",
				));




				if (Mage::getSingleton("adminhtml/session")->getBulkorderData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getBulkorderData());
					Mage::getSingleton("adminhtml/session")->setBulkorderData(null);
				} 
				elseif(Mage::registry("bulkorder_data")) {
				    $form->setValues(Mage::registry("bulkorder_data")->getData());
				}
				return parent::_prepareForm();
		}
}
