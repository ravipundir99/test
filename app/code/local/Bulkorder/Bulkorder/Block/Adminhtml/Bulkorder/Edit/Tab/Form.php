<?php

class Bulkorder_Bulkorder_Block_Adminhtml_Bulkorder_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form

{

		protected function _prepareForm()

		{



				$form = new Varien_Data_Form();

				$this->setForm($form);

				$fieldset = $form->addFieldset("bulkorder_form", array("legend"=>Mage::helper("bulkorder")->__("Item information")));



				$fieldset->addField("name", "text", array(

				"label" => Mage::helper("bulkorder")->__("Bulkorder Name"),

				"class" => "required-entry",

				"required" => true,

				"name" => "name",

				));

				$fieldset->addField("contact","text", array(

				'label'    => Mage::helper("bulkorder")->__("Contact"),

				//'width'     => '150px',

				"name"     => "contact",

				 ));

				

				$fieldset->addField("email","text", array(

				'label'    => Mage::helper("bulkorder")->__("Email"),

				//'width'     => '150px',

				"name"     => "email",

				 ));

				 

				 $fieldset->addField('product', 'text', array(

				  'name'      => 'product',

				  'label'     => Mage::helper('bulkorder')->__('Product'),

				  "class" => "required-entry",

				  'required'  => true,

				));

				 $fieldset->addField('content', 'editor', array(

				  'name'      => 'content',

				  'label'     => Mage::helper('bulkorder')->__('Content'),

				  'title'     => Mage::helper('bulkorder')->__('Content'),

				  'style'     => 'width:300px; height:50px;',

				  'wysiwyg'   => false,

				  'required'  => true,

				));
				
				 $fieldset->addField('comment', 'editor', array(

				  'name'      => 'comment',

				  'label'     => Mage::helper('bulkorder')->__('Comment'),

				  'title'     => Mage::helper('bulkorder')->__('Comment'),

				  'style'     => 'width:300px; height:50px;',

				  'wysiwyg'   => false,

				  'required'  => true,

				));
				
				 $fieldset->addField('status', 'select', array(

          'label'     => Mage::helper('bulkorder')->__('Status'),

          'name'      => 'status',

          'values'    => array(

              array(

                  'value'     => 'Not Relevant',

                  'label'     => Mage::helper('bulkorder')->__('Not Relevant'),

              ),



              array(

                  'value'     => 'Quotation Sent',

                  'label'     => Mage::helper('bulkorder')->__('Quotation Sent'),

              ),
			  
			   array(

                  'value'     => 'Sample Required/Sent',

                  'label'     => Mage::helper('bulkorder')->__('Sample Required/Sent'),

              ),
			  
			  array(

                  'value'     => 'Under Negotiation',

                  'label'     => Mage::helper('bulkorder')->__('Under Negotiation'),

              ),
			   array(

                  'value'     => 'Receive',

                  'label'     => Mage::helper('bulkorder')->__('Receive'),

              ),
			  
			   array(

                  'value'     => 'Lost Due to Price/Delivery',

                  'label'     => Mage::helper('bulkorder')->__('Lost Due to Price/Delivery'),

              ),
			   array(

                  'value'     => 'Just Recieved',

                  'label'     => Mage::helper('bulkorder')->__('Just Recieved'),

              ),

          ),

      ));

				/*$fieldset->addField('photo', 'image', array(

				 'label'     => Mage::helper('bulkorder')->__('File'),

				 "class" => "required-entry",

				'required'  => true,

				'name'      => 'photo',

				));
*/










				if (Mage::getSingleton("adminhtml/session")->getBulkorderData())

				{

					$form->setValues(Mage::getSingleton("adminhtml/session")->getBulkorderData());

					Mage::getSingleton("adminhtml/session")->setBulkorderData(null);

				} 

				elseif(Mage::registry("bulkorder_data")) {

				    $form->setValues(Mage::registry("bulkorder_data")->getData());

				}

				return parent::_prepareForm();

		}

}

