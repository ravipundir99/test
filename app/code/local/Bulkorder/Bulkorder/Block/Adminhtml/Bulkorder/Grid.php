<?php
class Bulkorder_Bulkorder_Block_Adminhtml_Bulkorder_Grid extends Mage_Adminhtml_Block_Widget_Grid

{



		public function __construct()

		{

				parent::__construct();

				$this->setId("bulkorderGrid");

				$this->setDefaultSort("bulkorder_id");

				$this->setDefaultDir("ASC");

				$this->setSaveParametersInSession(true);

		}



		protected function _prepareCollection()

		{

				$collection = Mage::getModel("bulkorder/bulkorder")->getCollection();

				$this->setCollection($collection);

				return parent::_prepareCollection();

		}

		protected function _prepareColumns()

		{

				$this->addColumn("bulkorder_id", array(

				"header" => Mage::helper("bulkorder")->__("ID"),

				"align" =>"right",

				"width" => "50px",

				"index" => "bulkorder_id",

				));

				$this->addColumn("name", array(

				"header" => Mage::helper("bulkorder")->__("Bulkorder Name"),

				"align" =>"left",

				"index" => "name",

				));

				$this->addColumn("email", array(

				"header" => Mage::helper("bulkorder")->__("Email"),

				"align" =>"left",

				"index" => "email",

				));
				
				$this->addColumn("contact", array(

				"header" => Mage::helper("bulkorder")->__("Contact No."),

				"align" =>"left",

				"index" => "contact",

				));

				$this->addColumn("photo", array(

				"header" => Mage::helper("bulkorder")->__("Attachment<br>http://printland.in/enquiry_attach/"),

				"align" =>"left",

				"index" => "photo",

				));

				$this->addColumn("edate", array(

				"header" => Mage::helper("bulkorder")->__("Date"),

				"align" =>"left",

				"index" => "edate",

				));
				
				$this->addColumn("comment", array(

				"header" => Mage::helper("bulkorder")->__("Sales Comment"),

				"align" =>"left",

				"index" => "comment",

				));
				
				$this->addColumn("status", array(

				"header" => Mage::helper("bulkorder")->__("Status"),

				"align" =>"left",

				"index" => "status",

				));
				
				 
				 
				 $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('bulkorder')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('bulkorder')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
				return parent::_prepareColumns();

		}

		 

		public function getRowUrl($row)

		{

			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}



}