<?php

class Bulkorder_Bulkorder_Adminhtml_BulkorderController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("bulkorder/bulkorder")->_addBreadcrumb(Mage::helper("adminhtml")->__("Bulkorder  Manager"),Mage::helper("adminhtml")->__("Bulkorder Manager"));
				return $this;
		}
		public function indexAction() 
		{
				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{
				$brandsId = $this->getRequest()->getParam("id");
				$brandsModel = Mage::getModel("bulkorder/bulkorder")->load($brandsId);
				if ($brandsModel->getId() || $brandsId == 0) {
					Mage::register("bulkorder_data", $brandsModel);
					$this->loadLayout();
					$this->_setActiveMenu("bulkorder/bulkorder");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Bulkorder Manager"), Mage::helper("adminhtml")->__("Bulkorder Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Bulkorder Description"), Mage::helper("adminhtml")->__("Bulkorder Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("bulkorder/adminhtml_bulkorder_edit"))->_addLeft($this->getLayout()->createBlock("bulkorder/adminhtml_bulkorder_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("bulkorder")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("bulkorder/bulkorder")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("bulkorder_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("bulkorder/bulkorder");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Bulkorder Manager"), Mage::helper("adminhtml")->__("Bulkorder Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Bulkorder Description"), Mage::helper("adminhtml")->__("Bulkorder Description"));


		$this->_addContent($this->getLayout()->createBlock("bulkorder/adminhtml_bulkorder_edit"))->_addLeft($this->getLayout()->createBlock("bulkorder/adminhtml_bulkorder_edit_tabs"));

		$this->renderLayout();

		       // $this->_forward("edit");
		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {

					try {

						$brandsModel = Mage::getModel("bulkorder/bulkorder")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Bulkorder was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setBulkorderData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $brandsModel->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setBulkorderData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$brandsModel = Mage::getModel("bulkorder/bulkorder");
						$brandsModel->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}
}
