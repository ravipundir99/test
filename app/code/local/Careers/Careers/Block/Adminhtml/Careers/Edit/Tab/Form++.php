<?php
class Careers_Careers_Block_Adminhtml_Careers_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("careers_form", array("legend"=>Mage::helper("careers")->__("Item information")));

				
						$fieldset->addField("name", "text", array(
						"label" => Mage::helper("careers")->__("Name"),
						"name" => "name",
						));
					
						$fieldset->addField("email", "text", array(
						"label" => Mage::helper("careers")->__("Email"),
						"name" => "email",
						));
					
						$fieldset->addField("position", "text", array(
						"label" => Mage::helper("careers")->__("Position"),
						"name" => "position",
						));
					

				if (Mage::getSingleton("adminhtml/session")->getCareersData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getCareersData());
					Mage::getSingleton("adminhtml/session")->setCareersData(null);
				} 
				elseif(Mage::registry("careers_data")) {
				    $form->setValues(Mage::registry("careers_data")->getData());
				}
				return parent::_prepareForm();
		}
}
