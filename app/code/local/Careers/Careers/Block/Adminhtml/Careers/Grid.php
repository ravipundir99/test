<?php

class Careers_Careers_Block_Adminhtml_Careers_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("careersGrid");
				$this->setDefaultSort("careers_id");
				$this->setDefaultDir("ASC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("careers/careers")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("careers_id", array(
				"header" => Mage::helper("careers")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "careers_id",
				));
                
				$this->addColumn("name", array(
				"header" => Mage::helper("careers")->__("Name"),
				"index" => "name",
				));
				$this->addColumn("email", array(
				"header" => Mage::helper("careers")->__("Email"),
				"index" => "email",
				));
				$this->addColumn("position", array(
				"header" => Mage::helper("careers")->__("Position"),
				"index" => "position",
				));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('careers_id');
			$this->getMassactionBlock()->setFormFieldName('careers_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_careers', array(
					 'label'=> Mage::helper('careers')->__('Remove Careers'),
					 'url'  => $this->getUrl('*/adminhtml_careers/massRemove'),
					 'confirm' => Mage::helper('careers')->__('Are you sure?')
				));
			return $this;
		}
			

}