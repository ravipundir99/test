<?php
	
class Careers_Careers_Block_Adminhtml_Careers_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "careers_id";
				$this->_blockGroup = "careers";
				$this->_controller = "adminhtml_careers";
				$this->_updateButton("save", "label", Mage::helper("careers")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("careers")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("careers")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);

				$this->_addButton("Download", array(
					"label"     => Mage::helper("careers")->__("Download File"),
					"onclick"   => "downlodimg()",//Mage::getModel(Mage_Core_Model_Store::URL_TYPE_WEB)
					"class"     => "save",
				), -100);				$targetfolder = 'carrer_documents';
				$tid = Mage::registry("careers_data")->getId();
				$location = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)."downloadfile.php?dfile=".$this->_blockGroup."&did=".$tid."&folder=".$targetfolder;

				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
							
							function downlodimg(){
							location.href='$location';
							}
							
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("careers_data") && Mage::registry("careers_data")->getId() ){

				    return Mage::helper("careers")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("careers_data")->getId()));

				} 
				else{

				     return Mage::helper("careers")->__("Add Item");

				}
		}
}