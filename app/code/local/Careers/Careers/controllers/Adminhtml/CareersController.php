<?php

class Careers_Careers_Adminhtml_CareersController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("careers/careers")->_addBreadcrumb(Mage::helper("adminhtml")->__("Careers  Manager"),Mage::helper("adminhtml")->__("Careers Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Careers"));
			    $this->_title($this->__("Manager Careers"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{			    
			    $this->_title($this->__("Careers"));
				$this->_title($this->__("Careers"));
			    $this->_title($this->__("Edit Item"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("careers/careers")->load($id);
				if ($model->getId()) {
					Mage::register("careers_data", $model);
					$this->loadLayout();
					$this->_setActiveMenu("careers/careers");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Careers Manager"), Mage::helper("adminhtml")->__("Careers Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Careers Description"), Mage::helper("adminhtml")->__("Careers Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("careers/adminhtml_careers_edit"))->_addLeft($this->getLayout()->createBlock("careers/adminhtml_careers_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("careers")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

		$this->_title($this->__("Careers"));
		$this->_title($this->__("Careers"));
		$this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("careers/careers")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("careers_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("careers/careers");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Careers Manager"), Mage::helper("adminhtml")->__("Careers Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Careers Description"), Mage::helper("adminhtml")->__("Careers Description"));


		$this->_addContent($this->getLayout()->createBlock("careers/adminhtml_careers_edit"))->_addLeft($this->getLayout()->createBlock("careers/adminhtml_careers_edit_tabs"));

		$this->renderLayout();

		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {

					try {
						
						
						if(isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') {
							try {  
									$uploader = new Varien_File_Uploader('photo');
									//$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
									$uploader->setAllowedExtensions(array('doc','pdf','txt','docx'));
									$uploader->setAllowRenameFiles(false);
									$uploader->setFilesDispersion(false);
									//$path = Mage::getBaseDir('media') . "resume";//DS ;
									$path = Mage::getBaseDir('media').DS.'carrer_documents'.DS;
									$uploader->save($path, $_FILES['photo']['name']);
									$post_data['photo'] = $_FILES['photo']['name']; 	
									}
							catch (Exception $e) {
										 print_r($e);
										 die;
									 }  		
								}

						$model = Mage::getModel("careers/careers")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Careers was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setCareersData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $model->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setCareersData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$model = Mage::getModel("careers/careers");
						$model->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}

		
		public function massRemoveAction()
		{
			try {
				$ids = $this->getRequest()->getPost('careers_ids', array());
				foreach ($ids as $id) {
                      $model = Mage::getModel("careers/careers");
					  $model->setId($id)->delete();
				}
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
			}
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
			}
			$this->_redirect('*/*/');
		}
			
		/**
		 * Export order grid to CSV format
		 */
		public function exportCsvAction()
		{
			$fileName   = 'careers.csv';
			$grid       = $this->getLayout()->createBlock('careers/adminhtml_careers_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
		} 
		/**
		 *  Export order grid to Excel XML format
		 */
		public function exportExcelAction()
		{
			$fileName   = 'careers.xml';
			$grid       = $this->getLayout()->createBlock('careers/adminhtml_careers_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
		}
}
