<?php
class Resetpassword_Resetpassword_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      
	  $this->loadLayout();   
	  $this->getLayout()->getBlock("head")->setTitle($this->__("Resetpassword"));
	        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
		   ));

      $breadcrumbs->addCrumb("resetpassword", array(
                "label" => $this->__("Resetpassword"),
                "title" => $this->__("Resetpassword")
		   ));

      $this->renderLayout(); 
	  
    }
}