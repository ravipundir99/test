<?php
class Contact_Contact_Block_Adminhtml_Contact_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("contact_form", array("legend"=>Mage::helper("contact")->__("Item information")));

				$fieldset->addField("name", "text", array(
				"label" => Mage::helper("contact")->__("Contact Name"),
				"class" => "required-entry",
				"required" => true,
				"name" => "name",
				));
				$fieldset->addField("email", "text", array(
				"label" => Mage::helper("contact")->__("Email"),
				"class" => "required-entry",
				"required" => true,
				"name" => "email",
				));
				$fieldset->addField("phone", "text", array(
				"label" => Mage::helper("contact")->__("Phone No"),
				"class" => "required-entry",
				"required" => true,
				"name" => "phone",
				));





                            $fieldset->addField("assigned_to", "select", array(
				"label" => Mage::helper("contact")->__("Assigned To"),
				"class" => "required-entry",
				"required" => true,
				"name" => "assigned_to",
				'options' => array("Manish","Deepak","Divya","Neetu","Pranav","Suresh Dhiman","Suneel","Sheraz","Rajat Singh"),
				
				));
				
				
				
				$fieldset->addField("status", "select", array(
				"label" => Mage::helper("contact")->__("Status"),
				"class" => "required-entry",
				"required" => true,
				"name" => "status",
				'options' => array("Yet to Respond","blank","Customer Not Contactable ","Quotation Submitted","Order Entered","Responded to the customer","Others"),
				
				));
				



				
				$fieldset->addField('content', 'editor', array(
				  'name'      => 'content',
				  'label'     => Mage::helper('contact')->__('Content'),
				  'title'     => Mage::helper('contact')->__('Content'),
				  'style'     => 'width:500px; height:300px;',
				  'wysiwyg'   => false,
				  'required'  => true,
			  ));



				if (Mage::getSingleton("adminhtml/session")->getContactData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getContactData());
					Mage::getSingleton("adminhtml/session")->setContactData(null);
				} 
				elseif(Mage::registry("contact_data")) {
				    $form->setValues(Mage::registry("contact_data")->getData());
				}
				return parent::_prepareForm();
		}
}
