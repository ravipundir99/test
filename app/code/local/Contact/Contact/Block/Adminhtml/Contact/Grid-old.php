<?php

class Contact_Contact_Block_Adminhtml_Contact_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("contactGrid");
				$this->setDefaultSort("contact_id");
				$this->setDefaultDir("ASC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("contact/contact")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("contact_id", array(
				"header" => Mage::helper("contact")->__("ID"),
				"align" =>"right",
				"width" => "50px",
				"index" => "contact_id",
				));
				
				$this->addColumn("name", array(
				"header" => Mage::helper("contact")->__("Contact Name"),
				"align" =>"left",
				"index" => "name",
				));
				$this->addColumn("email", array(
				"header" => Mage::helper("contact")->__("Email"),
				"align" =>"left",
				"index" => "email",
				));
				$this->addColumn("phone", array(
				"header" => Mage::helper("contact")->__("Phone No"),
				"align" =>"left",
				"index" => "phone",
				));


				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}

}