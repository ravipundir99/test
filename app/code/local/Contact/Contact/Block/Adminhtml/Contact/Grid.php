<?php

class Contact_Contact_Block_Adminhtml_Contact_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("contactGrid");
				$this->setDefaultSort("contact_id");
				$this->setDefaultDir("ASC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("contact/contact")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("contact_id", array(
				"header" => Mage::helper("contact")->__("ID"),
				"align" =>"right",
				"width" => "50px",
				"index" => "contact_id",
				"filter_index" => 'main_table.contact_id',
				));
				
				$this->addColumn("cdate", array(
				"header" => Mage::helper("contact")->__("Date"),
				"align" =>"left",
				"index" => "cdate",
				));
				
				$this->addColumn("name", array(
				"header" => Mage::helper("contact")->__("Contact Name"),
				"align" =>"left",
				"index" => "name",
				));
				$this->addColumn("email", array(
				"header" => Mage::helper("contact")->__("Email"),
				"align" =>"left",
				"index" => "email",
				));
				$this->addColumn("phone", array(
				"header" => Mage::helper("contact")->__("Phone No"),
				"align" =>"left",
				"index" => "phone",
				));
				
				$this->addColumn("ctype", array(
				"header" => Mage::helper("contact")->__("Type"),
				"align" =>"left",
				"index" => "ctype",
				));


                           $this->addColumn("assigned_to", array(
				"header" => Mage::helper("contact")->__("Assigned To"),
				"align" =>"left",
				"index" => "assigned_to",
				'type'  => 'options',
				'options' => array("Manish","Deepak","Divya","Neetu","Pranav","Suresh Dhiman","Suneel","Sheraz","Rajat Singh"),
				));
				
				$this->addColumn("status", array(
				"header" => Mage::helper("contact")->__("Status"),
				"align" =>"left",
				"index" => "status",
				'type'  => 'options',
				'options' => array("Yet to Respond","blank","Customer Not Contactable ","Quotation Submitted","Order Entered ","Responded to the customer","Others"),
				));
			$this->addColumn('action', array(
                'header'    =>  Mage::helper('contact')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('contact')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));





				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}

}