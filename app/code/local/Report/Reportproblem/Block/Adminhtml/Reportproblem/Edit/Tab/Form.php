<?php
class Report_Reportproblem_Block_Adminhtml_Reportproblem_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("reportproblem_form", array("legend"=>Mage::helper("reportproblem")->__("Item information")));

				$fieldset->addField("name", "text", array(
				"label" => Mage::helper("reportproblem")->__("Name"),
				"class" => "required-entry",
				"required" => true,
				"name" => "name",
				));
				
				$fieldset->addField("contact","text", array(
				'label'    => Mage::helper("reportproblem")->__("Contact"),
				//'width'     => '150px',
				"name"     => "contact",
				 ));
				
				$fieldset->addField("email","text", array(
				'label'    => Mage::helper("reportproblem")->__("Email"),
				//'width'     => '150px',
				"name"     => "email",
				 ));
				 
				 $fieldset->addField('problemnature', 'editor', array(
				  'name'      => 'problemnature',
				  'label'     => Mage::helper('reportproblem')->__('Nature of problem'),
				  'title'     => Mage::helper('reportproblem')->__('Nature of problem'),
				  'style'     => 'width:500px; height:200px;',
				  'wysiwyg'   => false,
				  'required'  => true,
				));
				 $fieldset->addField('message', 'editor', array(
				  'name'      => 'message',
				  'label'     => Mage::helper('reportproblem')->__('Content'),
				  'title'     => Mage::helper('reportproblem')->__('Content'),
				  'style'     => 'width:500px; height:300px;',
				  'wysiwyg'   => false,
				  'required'  => true,
				));




				if (Mage::getSingleton("adminhtml/session")->getReportproblemData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getReportproblemData());
					Mage::getSingleton("adminhtml/session")->setReportproblemData(null);
				} 
				elseif(Mage::registry("reportproblem_data")) {
				    $form->setValues(Mage::registry("reportproblem_data")->getData());
				}
				return parent::_prepareForm();
		}
}
