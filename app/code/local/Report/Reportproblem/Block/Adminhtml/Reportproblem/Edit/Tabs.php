<?php
class Report_Reportproblem_Block_Adminhtml_Reportproblem_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("reportproblem_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("reportproblem")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("reportproblem")->__("Item Information"),
				"title" => Mage::helper("reportproblem")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("reportproblem/adminhtml_reportproblem_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
