<?php


class Report_Reportproblem_Block_Adminhtml_Reportproblem extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_reportproblem";
	$this->_blockGroup = "reportproblem";
	$this->_headerText = Mage::helper("reportproblem")->__("Reportproblem Manager");
	$this->_addButtonLabel = Mage::helper("reportproblem")->__("Add New Item");
	parent::__construct();

	}

}