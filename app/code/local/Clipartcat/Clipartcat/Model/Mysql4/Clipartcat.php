<?php

class Clipartcat_Clipartcat_Model_Mysql4_Clipartcat extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the clipartcat_id refers to the key field in your database table.
        $this->_init('clipartcat/clipartcat', 'clipartcat_id');
    }
}