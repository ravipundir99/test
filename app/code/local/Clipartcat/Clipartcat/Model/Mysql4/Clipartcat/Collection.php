<?php

class Clipartcat_Clipartcat_Model_Mysql4_Clipartcat_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('clipartcat/clipartcat');
    }
}