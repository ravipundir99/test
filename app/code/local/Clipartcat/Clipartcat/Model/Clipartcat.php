<?php

class Clipartcat_Clipartcat_Model_Clipartcat extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('clipartcat/clipartcat');
    }
}