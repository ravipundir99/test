<?php
class Clipartcat_Clipartcat_Block_Clipartcat extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getClipartcat()     
     { 
        if (!$this->hasData('clipartcat')) {
            $this->setData('clipartcat', Mage::registry('clipartcat'));
        }
        return $this->getData('clipartcat');
        
    }
}