<?php
class Clipartcat_Clipartcat_Block_Adminhtml_Clipartcat extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_clipartcat';
    $this->_blockGroup = 'clipartcat';
    $this->_headerText = Mage::helper('clipartcat')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('clipartcat')->__('Add Item');
    parent::__construct();
  }
}