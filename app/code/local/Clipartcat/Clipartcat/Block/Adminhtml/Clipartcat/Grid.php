<?php

class Clipartcat_Clipartcat_Block_Adminhtml_Clipartcat_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('clipartcatGrid');
      $this->setDefaultSort('clipartcat_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('clipartcat/clipartcat')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('clipartcat_id', array(
          'header'    => Mage::helper('clipartcat')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'clipartcat_id',
      ));

      $this->addColumn('title1', array(
          'header'    => Mage::helper('clipartcat')->__('Title'),
          'align'     =>'left',
          'index'     => 'title1',
      ));

	  /*
      $this->addColumn('content', array(
			'header'    => Mage::helper('clipartcat')->__('Item Content'),
			'width'     => '150px',
			'index'     => 'content',
      ));
	  */

      $this->addColumn('status', array(
          'header'    => Mage::helper('clipartcat')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('clipartcat')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('clipartcat')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('clipartcat')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('clipartcat')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('clipartcat_id');
        $this->getMassactionBlock()->setFormFieldName('clipartcat');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('clipartcat')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('clipartcat')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('clipartcat/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('clipartcat')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('clipartcat')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}