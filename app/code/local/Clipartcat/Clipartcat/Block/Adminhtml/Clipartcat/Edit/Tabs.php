<?php

class Clipartcat_Clipartcat_Block_Adminhtml_Clipartcat_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('clipartcat_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('clipartcat')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('clipartcat')->__('Item Information'),
          'title'     => Mage::helper('clipartcat')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('clipartcat/adminhtml_clipartcat_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}