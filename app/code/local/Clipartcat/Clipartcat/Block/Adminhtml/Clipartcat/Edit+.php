<?php

class Clipartcat_Clipartcat_Block_Adminhtml_Clipartcat_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $cipcatid = Mage::registry('clipartcat_data')->getId();		$model = Mage::getModel('clipartcat/clipartcat')->load($cipcatid);         
        $this->_objectId = 'id';
        $this->_blockGroup = 'clipartcat';
        $this->_controller = 'adminhtml_clipartcat';
        
        $this->_updateButton('save', 'label', Mage::helper('clipartcat')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('clipartcat')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);
		$this->_addButton('generatexml', array(            'label'     => Mage::helper('adminhtml')->__('Generate_xml'),            'onclick'   => 'generatexml()',            'class'     => 'save',        ), -100);							
        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('clipartcat_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'clipartcat_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'clipartcat_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }						function generatexml()			{			alert('".$model->getTitle1()."');			}
        ";
    }
	function generatexml()
	{
	echo $model->getFilename();
	}
    public function getHeaderText()
    {
        if( Mage::registry('clipartcat_data') && Mage::registry('clipartcat_data')->getId() ) {
            return Mage::helper('clipartcat')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('clipartcat_data')->getTitle()));
        } else {
            return Mage::helper('clipartcat')->__('Add Item');
        }
    }
}