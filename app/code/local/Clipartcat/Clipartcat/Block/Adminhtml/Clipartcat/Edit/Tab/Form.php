<?php

class Clipartcat_Clipartcat_Block_Adminhtml_Clipartcat_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('clipartcat_form', array('legend'=>Mage::helper('clipartcat')->__('Item information')));
     
      $fieldset->addField('title1', 'text', array(
          'label'     => Mage::helper('clipartcat')->__('Category Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title1',
      ));

     /* $fieldset->addField('filename', 'file', array(
          'label'     => Mage::helper('clipartcat')->__('File'),
          'required'  => false,
          'name'      => 'filename',
	  ));*/
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('clipartcat')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('clipartcat')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('clipartcat')->__('Disabled'),
              ),
          ),
      ));
     
     /* $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('clipartcat')->__('Content'),
          'title'     => Mage::helper('clipartcat')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
          'required'  => true,
      ));*/
     
      if ( Mage::getSingleton('adminhtml/session')->getClipartcatData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getClipartcatData());
          Mage::getSingleton('adminhtml/session')->setClipartcatData(null);
      } elseif ( Mage::registry('clipartcat_data') ) {
          $form->setValues(Mage::registry('clipartcat_data')->getData());
      }
      return parent::_prepareForm();
  }
}