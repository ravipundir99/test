<script>
function encodeValue(val)
{
	var encodedText = Base64.encode(val);
	
	var lenEncTxt = encodedText.length;

	var str1 = encodedText.slice(0,1);
	var str2 = encodedText.slice(1, lenEncTxt);

	var str3 = str1 + "T" + str2;

	var encVal = Base64.encode(str3);
	
	return encVal;
}

function decodeValue(val)
{
	var decodedText = Base64.decode(val);

	var lenDecTxt = decodedText.length;

	var str1 = decodedText.slice(0,1);
	var str2 = decodedText.slice(2, lenDecTxt);

	var str3 = str1 + str2;
	
	var decVal = Base64.decode(str3);

	return decVal;
}

var Base64 = {
	// private property
	_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
 
	// public method for encoding
	encode : function (input) {
		var output = "";
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;
 
		input = Base64._utf8_encode(input);
 
		while (i < input.length) {
 			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);
 
			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;
 
			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}
 
			output = output +
			this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
			this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
 		}
 
		return output;
	},
 
	// public method for decoding
	decode : function (input) {
		var output = "";
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var i = 0;
 
		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
 
		while (i < input.length) {
 			enc1 = this._keyStr.indexOf(input.charAt(i++));
			enc2 = this._keyStr.indexOf(input.charAt(i++));
			enc3 = this._keyStr.indexOf(input.charAt(i++));
			enc4 = this._keyStr.indexOf(input.charAt(i++));
 
			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;
 
			output = output + String.fromCharCode(chr1);
 
			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}
 		}
 
		output = Base64._utf8_decode(output);
 
		return output;
 	},
 
	// private method for UTF-8 encoding
	_utf8_encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";
 
		for (var n = 0; n < string.length; n++) {
 			var c = string.charCodeAt(n);
 
			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
 		}

		return utftext;
	},
 
	// private method for UTF-8 decoding
	_utf8_decode : function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;
 
		while ( i < utftext.length ) {
 			c = utftext.charCodeAt(i);
 
			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
 		}

		return string;
	}
}

function encodeTxnRequest()
 {
document.directpay_checkout.requestparameter.value =encodeValue(document.directpay_checkout.requestparameter.value);
document.directpay_checkout.submit();
} 
	</script>
<?php
/**
 * Direcpay
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * @category   Direcpay test.timesofmoney.com/direcpay/secure/dpMerchantTransaction.jsp
 * @package    Direcpay_Directpay
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Direcpay_Directpay_Block_Processing extends Mage_Core_Block_Abstract
{const XML_PATH_MERCHANTKEY	= 'directpay/settings/directpay_merchantkey';
	/**
	 * prepare the HTML form and submit to directpay server
	 */
	protected function _toHtml()
	{
		$payment = $this->getOrder()->getPayment()->getMethodInstance();
		$order_id	= $this->getOrder()->getRealOrderId();
		$billing	= $this->getOrder()->getBillingAddress();

		if ($this->getOrder()->getBillingAddress()->getEmail()) {
			$email = $this->getOrder()->getBillingAddress()->getEmail();
		} else {
			$email = $this->getOrder()->getCustomerEmail();
		}

		//$storeCurrencyCode = Mage::getSingleton('directory/currency')->load($this->getQuote()->getCurrencyCode());
		// $storeCurrencyCode = $this->getOrder()->getStoreCurrencyCode();
		$baseCurrencyCode = $this->getOrder()->getBaseCurrencyCode();
		
		// if the base currency is the same as the payment method, no need of conversion
		if ( $this->_paymentMethodCurrency == $baseCurrencyCode )
		{
			$orderTotal = round($this->getOrder()->getBaseGrandTotal(), 2);
			$orderTotal = number_format($orderTotal, 2, '.', ''); // Example: http://my.php.net/number-format			
		}
		else // convert from store currency to currency supported by gateway
		{
			//$orderTotal = Mage::getSingleton('directory/currency')->convert($this->getOrder()->getGrandTotal(), $this->_paymentMethodCurrency);
				
			$orderTotal = round($this->getOrder()->getGrandTotal() , 2 );
			$orderTotal = number_format($orderTotal, 2, '.', ''); // Example: http://my.php.net/number-format
		}
		
$MERCHANTKEY = Mage::getStoreConfig(self::XML_PATH_MERCHANTKEY);
		$test= Mage::getStoreConfig('payment/directpay_15/test');
		if($test=='1')
		{
			//$url='https://test.timesofmoney.com/direcpay/secure/dpMerchantTransaction.jsp';
			//$url='https://test.direcpay.com/direcpay/secure/dpMerchantPayment.jsp';
		//	$url='https://test.direcpay.com/direcpay/secure/dpMerchantTransaction.jsp';

		//	$collaborator='TOML';
			$url='https://www.timesofmoney.com/direcpay/secure/dpMerchantTransaction.jsp';
			$collaborator='DirecPay';	
		}
		else
		{
			$url='https://www.timesofmoney.com/direcpay/secure/dpMerchantTransaction.jsp';
			$collaborator='DirecPay';	

		}


		$billing_Firstname=$this->getOrder()->getBillingAddress()->getFirstname();
		$billing_Street= $this->getOrder()->getBillingAddress()->getStreet(1);
    	$billing_City= $this->getOrder()->getBillingAddress()->getCity(); 
		$billing_Region= $this->getOrder()->getBillingAddress()->getRegion();
    	$billing_Postcode= $this->getOrder()->getBillingAddress()->getPostcode();
    	$billing_Country=	$this->getOrder()->getBillingAddress()->getCountry();
		$billing_Countryiso3 = Mage::getModel('directory/country')->load($billing_Country)->getIso3Code(); 
    	$billing_Telephone=	$this->getOrder()->getBillingAddress()->getTelephone();

		$Shipping_Firstname=$this->getOrder()->getShippingAddress()->getFirstname();
		$Shipping_Street= $this->getOrder()->getShippingAddress()->getStreet(1);
    	$Shipping_City= $this->getOrder()->getShippingAddress()->getCity(); 
		$Shipping_Region= $this->getOrder()->getShippingAddress()->getRegion();
    	$Shipping_Postcode= $this->getOrder()->getShippingAddress()->getPostcode();
    	$Shipping_Country=	$this->getOrder()->getShippingAddress()->getCountry_id();
		$Shipping_Countryiso3 = Mage::getModel('directory/country')->load($Shipping_Country)->getIso3Code(); 
    	$Shipping_Telephone=	$this->getOrder()->getShippingAddress()->getTelephone();
		
		 $base_url=Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
$rand = substr(hash('sha512',uniqid(rand(), true)), 0, 2);		
		$baseCurrencyCode = $this->getOrder()->getBaseCurrencyCode();
		$form = new Varien_Data_Form();
		$form->setAction($payment->getUrl())
			->setId('directpay_checkout')
			->setName('directpay_checkout')
			->setMethod('POST')
			->setUseContainer(true);
		foreach ($payment->getFormFields() as $field=>$value) {
			$form->addField($field, 'hidden', array('name'=>$field, 'value'=>$value));
		}
		$html = '<html><body onload="encodeTxnRequest()">';
		$html.= $this->__('Redirecting to secure payment...');
		$html.= '<form method="POST" name="directpay_checkout" id="directpay_checkout" action="'.$url.'">';
		$html.= '<input type="hidden" value="'.$billing_Firstname.'" name="custName" id="custName">';
		$html.= '<input type="hidden" value="'.$billing_Street.'" name="custAddress" id="custAddress">';
		$html.= '<input type="hidden" value="'.$billing_City.'" name="custCity" id="custCity">';
		$html.= '<input type="hidden" value="'.$billing_Region.'" name="custState" id="custState">';
		$html.= '<input type="hidden" value="'.$billing_Postcode.'" name="custPinCode" id="custPinCode">';
		$html.= '<input type="hidden" value="'.$billing_Country.'" name="custCountry" id="custCountry">';
		$html.= '<input type="hidden" value="" name="custPhoneNo1" id="custPhoneNo1">';
		$html.= '<input type="hidden" value="" name="custPhoneNo2" id="custPhoneNo2">';
		$html.= '<input type="hidden" value="'.$billing_Telephone.'" name="custPhoneNo3" id="custPhoneNo3">';
		$html.= '<input type="hidden" value="" name="custMobileNo" id="custMobileNo">';
		$html.= '<input type="hidden" value="'.$email.'" name="custEmailId" id="custEmailId">';
		$html.= '<input type="hidden" value="'.$Shipping_Firstname.'" name="deliveryName" id="deliveryName">';
		$html.= '<input type="hidden" value="'.$Shipping_Street.'" name="deliveryAddress" id="deliveryAddress">';
		$html.= '<input type="hidden" value="'.$Shipping_City.'" name="deliveryCity" id="deliveryCity">';
		$html.= '<input type="hidden" value="'.$Shipping_Region.'" name="deliveryState" id="deliveryState">';
		$html.= '<input type="hidden" value="'.$Shipping_Postcode.'" name="deliveryPinCode" id="deliveryPinCode">';
		$html.= '<input type="hidden" value="'.$Shipping_Country.'" name="deliveryCountry" id="deliveryCountry">';
		$html.= '<input type="hidden" value="" name="deliveryPhNo1" id="deliveryPhNo1">';
		$html.= '<input type="hidden" value="" name="deliveryPhNo2" id="deliveryPhNo2">';
		$html.= '<input type="hidden" value="'.$Shipping_Telephone.'" name="deliveryPhNo3" id="deliveryPhNo3">';
		$html.= '<input type="hidden" value="" name="deliveryMobileNo" id="deliveryMobileNo">';
		$html.= '<input type="hidden" value="test transaction for direcpay" name="otherNotes" id="otherNotes">';
		$html.= '<input type="hidden" value="Y" name="editAllowed" id="editAllowed">';
		$html.= '<input type="hidden" value="'.$MERCHANTKEY.'|DOM|'.$Shipping_Countryiso3.'|'.$baseCurrencyCode.'|'.$orderTotal.'|'.$rand.'-'.$order_id.'|others|'.$base_url.'index.php/Direcpay/processing/complete/|'.$base_url.'index.php/Direcpay/processing/fail/|'.$collaborator.'" name="requestparameter" id="requestparameter">';
		$html.= '</form>';
		$html.= '</body></html>';
		return $html;
	}
}
?>