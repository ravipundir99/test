<?php
/**
 * Direcpay
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * @category   Direcpay
 * @package    Direcpay_Directpay
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Direcpay_Directpay_Block_Failure extends Mage_Core_Block_Template
{
	/**
	 * Get continue shopping url
	 */
	public function getContinueShoppingUrl()
	{
		return Mage::getUrl('checkout/cart');
	}
	
	/**
	 *  Return Error message to shopper
	 *
	 *  @return	  string
	 */
	public function getErrorMessage()
	{
		return Mage::getSingleton('checkout/session')->getDirectpayErrorMessage();
	}
}
?>