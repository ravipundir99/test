<?php
/**
 * Direcpay
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * @category   Direcpay
 * @package    Direcpay_Directpay
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Direcpay_Directpay_Model_10 extends Direcpay_Directpay_Model_Shared
{  
	/**   
	* payment id assigned by iPay88   
	*	
	* AM Bank - MYR only
	**/
	protected $_code			= 'directpay_10';
	protected $_paymentMethod	= '10';
	protected $_paymentMethodCurrency = 'MYR';	
}
?>