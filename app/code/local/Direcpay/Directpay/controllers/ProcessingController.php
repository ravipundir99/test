<?php
/**
 * Direcpay
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * @category   Direcpay
 * @package    Direcpay_Directpay
 * @copyright  Copyright (c) 2009 Direcpay
 */
 
class Direcpay_Directpay_ProcessingController extends Mage_Core_Controller_Front_Action
{
	/**
	 * Processing Block Type
	 *
	 * @var string
	 */
	protected $_redirectBlockType = 'directpay/processing';
	protected $_statusBlockType	= 'directpay/status';

	protected function _expireAjax()
	{
		if (!$this->getCheckout()->getQuote()->hasItems()) {
			$this->getResponse()->setHeader('HTTP/1.1','403 Session Expired');
			exit;
		}
	}

	/**
	 * Get singleton of Checkout Session Model
	 *
	 * @return Mage_Checkout_Model_Session
	 */
	public function getCheckout()
	{
		return Mage::getSingleton('checkout/session');
	}

	/**
	 * Redirection from store to directpay after customer select directpay payment method (like credit card or Maybank2u) and click on the Pay button
	 */
	public function redirectAction()
	{
		$session = $this->getCheckout();
		$session->setDirectpayQuoteId($session->getQuoteId());
		$session->setDirectpayRealOrderId($session->getLastRealOrderId());

		$order = Mage::getModel('sales/order');
		$order->loadByIncrementId($session->getLastRealOrderId());
		$order->addStatusToHistory(Mage_Sales_Model_Order::STATE_HOLDED, Mage::helper('directpay')->__('Customer was redirected.'));
		$order->save();
		
		///===============================Custom Prder Mail By Logic It Solution================================
		$response = $this->getRequest()->getParams();
		$order->loadByIncrementId($response['RefNo']);
		$order->sendNewOrderEmail();
		//======================================================================================================

		$this->getResponse()->setBody(
			$this->getLayout()
				->createBlock($this->_redirectBlockType)
				->setOrder($order)
				->toHtml()
		);

		$session->unsQuoteId();
	}
	
	/**
	 * directpay returns POST variables to this action (Store Response URL or URL 4)
	 */
	//
    public function failAction()
    {
		 $responseparams=explode('|',$_REQUEST['responseparams']);                       
		$oder_id  = explode('-',$responseparams[5]);
		$order=Mage::getModel('sales/order')->loadByIncrementId($oder_id[1]);
		$order->setStatus('canceled'); 
		$order->save();
		
		$this->_redirect('checkout/onepage/failure/');    }
    public function completeAction()
    {
		 $responseparams=explode('|',$_REQUEST['responseparams']);                       
		$oder_id  = explode('-',$responseparams[5]);
		$order=Mage::getModel('sales/order')->loadByIncrementId($oder_id[1]);
		$order->setStatus('complete'); 
		$order->save();
		
		$this->_redirect('checkout/onepage/success/');
    }


	public function statusAction()
	{
		$status = $this->processCallback(); // process the callback
		
		//$session = $this->getCheckout();
		//$session->unsDirectpayRealOrderId();
		//$session->setQuoteId($session->getDirectpayQuoteId(true)); // anything wrong here? changed DirectpayID to QuoteID
		//$session->getQuote()->setIsActive(false)->save();
		
		if ($status) {
			$this->getResponse()->setBody(
				$this->getLayout()
					->createBlock($this->_statusBlockType) 
					->toHtml()
			);
		}
	}
	
	/**
	 * Display failure page if error
	 */
	public function failureAction()
	{
		if (!$this->getCheckout()->getDirectpayErrorMessage()) {
			$this->norouteAction();
			return;
		}

		$this->getCheckout()->clear();

		$this->loadLayout();
		$this->renderLayout();
	}

	/**
	 * Checking POST variables. - this is processed at URL 4
	 * Creating invoice if payment was successful or cancel order if payment was declined
	 */
	protected function processCallback()
	{
		// if there is no POST data, someone is trying to access the page illegally. As a result, display 404 Page not found
		// if (!$this->getRequest()->isPost()) {
		if (!$this->getRequest()->getPost()) {
			$this->norouteAction();
			return;
		}
		
		$response = $this->getRequest()->getParams();	// retrieve POST array	from directpay	
		// check basic parameters returned by directpay
		//if (!isset($response['RefNo']) || !isset($response['Status']) || !isset($response['MerchantCode']) || !isset($response['Amount']) ) {
			$this->norouteAction();
			return;
		//}	

		$order = Mage::getModel('sales/order');
		$order->loadByIncrementId($response['RefNo']); // instantiate an order object for this RefNo (Magento order number)
		if(!$order->getId()) {
			$this->norouteAction();
			return;
		}
		
		// EXPERIMENTAL
		// check order ID - if merchant order id/transaction number mismatch
		if ($this->getCheckout()->getDirectpayRealOrderId() != $response['RefNo']) {
			$this->norouteAction();
			return;
		}	
		
		/*
		// EXPERIMENTAL
		// check if order amount equals the amount given in the callback - possibility of customer editing a cart during checkout process and before call back returns
		$orderTotal = round($order->getOrder()->getBaseGrandTotal() , 2 );
		$orderTotal = number_format($orderTotal, 2, '.', ''); // Example: http://my.php.net/number-format		
		if ($orderTotal != $response['Amount']) {
			$order->addStatusToHistory(Mage_Sales_Model_Order::STATE_HOLDED, Mage::helper('directpay')->__('The amount '. $response['Amount'] .' given by directpay is incorrect. The correct order amount should be ' . $orderTotal));
			$order->save();
			$this->norouteAction();
			return;	
		}
		*/
		
		$paymentInst = $order->getPayment()->getMethodInstance();
			
		// if Signature parameter was not returned or data is null, show 404 page
		if ( empty($response['Signature']) ) {
			$order->addStatusToHistory(Mage_Sales_Model_Order::STATE_HOLDED, Mage::helper('directpay')->__('No signature field.<br/>Error Description: ' . $response['ErrDesc'] . '<br/>Transaction ID: ' . $response['TransId'] . '<br/>RefNo: ' . $response['RefNo'] . '<br/>Currency: ' . $response['Currency'] . '<br/>Amount: ' . $response['Amount'] . '<br/>Bank Approval Code: ' . $response['AuthCode'] . '<br/>Payment ID: ' . $response['PaymentId'] . '<br/>Status: ' . $response['Status']) );
			
			$order->save();
			$this->norouteAction();
			return;			
		}
		// EXPERIMENTAL
		// if payment status is said to be successful but the signatures mismatch - do not process at all
		// if ( $response['Status'] == '1' && $this->signaturesMatch($response) == false ) {
		else if ( $paymentInst->signaturesMatch($response) == false ) {
		 	$order->addStatusToHistory(Mage_Sales_Model_Order::STATE_HOLDED, Mage::helper('directpay')->__('Signature mismatch. Kindly verify before approving order.<br/>Error Description: ' . $response['ErrDesc'] . '<br/>Transaction ID: ' . $response['TransId'] . '<br/>RefNo: ' . $response['RefNo'] . '<br/>Currency: ' . $response['Currency'] . '<br/>Amount: ' . $response['Amount'] . '<br/>Bank Approval Code: ' . $response['AuthCode'] . '<br/>Payment ID: ' . $response['PaymentId'] . '<br/>Status: ' . $response['Status']) );
		 	$order->save();
			//$this->norouteAction();
			//return;
		}
		
		// EXPERIMENTAL
		// re-query directpay to confirm validity callback data
		$RequeryReply = $paymentInst->Requery($response);
		if ( $RequeryReply == '00' )
		{
			$order->addStatusToHistory(Mage_Sales_Model_Order::STATE_HOLDED, Mage::helper('directpay')->__('confirms transaction is genuine.'));
			$order->save();		
		}
		else
		{
			$order->addStatusToHistory(Mage_Sales_Model_Order::STATE_HOLDED, Mage::helper('directpay')->__('Confirmation reply: ' . $RequeryReply) );
			$order->save();
		}
		
		// if transaction successful or failed ============ Front End Display Decision ================================
		if ($response['Status'] == '1') // payment successful
		{			
			$this->getCheckout()->setDirectpayRedirectUrl(Mage::getUrl('checkout/onepage/success'));
			
			
			if ( $RequeryReply == '00' )
			{
				$order->sendNewOrderEmail();
				
				// remove shopping cart session
				$session = $this->getCheckout();
				$session->unsDirectpayRealOrderId();
				$session->setDirectpayId($session->getDirectpayQuoteId(true)); // anything wrong here? changed DirectpayID to QuoteID
				$session->getQuote()->setIsActive(false)->save();
			}
		}
		else if ($response['Status'] == '0') // payment failed or cancelled
		{
			$order->cancel();
			$this->getCheckout()->setDirectpayRedirectUrl(Mage::getUrl('*/*/failure'));
			
			if (isset($response['ErrDesc']))
				$message = $response['ErrDesc'] . '. DirecPay Transaction ID: ' . $response['TransId'] . '. Bank Approval Code: ' . $response['AuthCode'];
			else
				$message = 'No error description was sent by Direct Pay.';
			
			// error message for customers (displayed on failure page)
			$this->getCheckout()->setDirectpayErrorMessage($message);

			$session = $this->getCheckout();
			$session->getQuote()->setIsActive(true)->save();
		}
		else // similar to above except cancelling or order - this part is unlikely to be executed
		{
			$this->getCheckout()->setDirectpayRedirectUrl(Mage::getUrl('*/*/failure'));
			
			if (isset($response['ErrDesc']))
				$message = $response['ErrDesc'] . '. DirecPay Transaction ID: ' . $response['TransId'] . '. Bank Approval Code: ' . $response['AuthCode'];
			else
				$message = 'No error description was sent by DirecPay.';
			
			// error message for customers (displayed on failure page)
			$this->getCheckout()->setDirectpayErrorMessage($message); // log the error message from directpay
			
			$session = $this->getCheckout();
			$session->getQuote()->setIsActive(true)->save();			
		}
		
		//============================================ Back End Processing ============================================
		$paymentInst->setTransactionId($response['TransId']); // set to given directpay transaction ID
		if ($response['Status'] == '1')  // should the checking be more stringent by adding the condition $RequeryReply == '00' ?
		{
			if ($order->canInvoice()) {
				$invoice = $order->prepareInvoice();
				
				$invoice->register()->capture();
				Mage::getModel('core/resource_transaction')
					->addObject($invoice)
					->addObject($invoice->getOrder())
					->save();
			}
			
			if ( $RequeryReply == '00' )
			{
				// error message for internal use only
				$order_status = Mage::helper('direcpay')->__('Payment is successful.');
			
				// $order->addStatusToHistory($paymentInst->getConfigData('order_status'), $order_status);
				$order->addStatusToHistory(Mage_Sales_Model_Order::STATE_PROCESSING, $order_status);
			}
			else
			{
				// error message for internal use only - likely to be executed if the re-query failed
				$order_status = Mage::helper('direcpay')->__('Caution. Possible fraud or Direct Pay gateway was down. Direct Pay was not able to reconfirm this payment status during the payment process. Kindly double check with your report before approving this order. Second confirmation from Direct Pay: ' . $RequeryReply);
				// $order_status = Mage::helper('directpay')->__('Use the following callback data from directpay for verification.<br/>Transaction ID: ' . $response['TransId'] . '<br/>RefNo: ' . $response['RefNo'] . '<br/>Currency: ' . $response['Currency'] . '<br/>Amount: ' . $response['Amount'] . '<br/>Bank Approval Code: ' . $response['AuthCode'] . '<br/>Payment ID: ' . $response['PaymentId'] . '<br/>Status: ' . $response['Status'] . '<br/>Signature: ' . $response['Signature'] );
			
				// $order->addStatusToHistory($paymentInst->getConfigData('order_status'), $order_status);
				$order->addStatusToHistory(Mage_Sales_Model_Order::STATE_HOLDED, $order_status);
			}
		}
		else if ($response['Status'] == '0') 
		{
			// error message for internal use only
			$order_status = Mage::helper('direcpay')->__('Payment failed or was cancelled.<br/>Error Description: ' . $response['ErrDesc'] . '<br/>Transaction ID: ' . $response['TransId'] . '<br/>RefNo: ' . $response['RefNo'] . '<br/>Currency: ' . $response['Currency'] . '<br/>Amount: ' . $response['Amount'] . '<br/>Bank Approval Code: ' . $response['AuthCode'] . '<br/>Payment ID: ' . $response['PaymentId'] . '<br/>Status: ' . $response['Status'] . '<br/>Signature: ' . $response['Signature'] );
			$order->cancel();
			$order->addStatusToHistory(Mage_Sales_Model_Order::STATE_CANCELED, $order_status);
			// $order->addStatusToHistory($paymentInst->getConfigData('order_status'), $order_status);
			// $status = false;
		}
		else // - this part is unlikely to be executed
		{
			// error message for internal use only
			$order_status = Mage::helper('directpay')->__('Invalid payment status. <br/>Error Description: ' . $response['ErrDesc'] . '<br/>Transaction ID: ' . $response['TransId'] . '<br/>RefNo: ' . $response['RefNo'] . '<br/>Currency: ' . $response['Currency'] . '<br/>Amount: ' . $response['Amount'] . '<br/>Bank Approval Code: ' . $response['AuthCode'] . '<br/>Payment ID: ' . $response['PaymentId'] . '<br/>Status: ' . $response['Status'] . '<br/>Signature: ' . $response['Signature'] );
			
			$order->addStatusToHistory(Mage_Sales_Model_Order::STATE_HOLDED, $order_status);
		}		
		//=========================================End of Back End Processing ============================================
		
		$order->save();

		return true;
	}
}