<?php

class Fontcategory_Fontcategory_Adminhtml_FontcategoryController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("fontcategory/fontcategory")->_addBreadcrumb(Mage::helper("adminhtml")->__("Fontcategory  Manager"),Mage::helper("adminhtml")->__("Fontcategory Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Fontcategory"));
			    $this->_title($this->__("Manager Fontcategory"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{			    
			    $this->_title($this->__("Fontcategory"));
				$this->_title($this->__("Fontcategory"));
			    $this->_title($this->__("Edit Item"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("fontcategory/fontcategory")->load($id);
				if ($model->getId()) {
					Mage::register("fontcategory_data", $model);
					$this->loadLayout();
					$this->_setActiveMenu("fontcategory/fontcategory");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Fontcategory Manager"), Mage::helper("adminhtml")->__("Fontcategory Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Fontcategory Description"), Mage::helper("adminhtml")->__("Fontcategory Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("fontcategory/adminhtml_fontcategory_edit"))->_addLeft($this->getLayout()->createBlock("fontcategory/adminhtml_fontcategory_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("fontcategory")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

		$this->_title($this->__("Fontcategory"));
		$this->_title($this->__("Fontcategory"));
		$this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("fontcategory/fontcategory")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("fontcategory_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("fontcategory/fontcategory");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Fontcategory Manager"), Mage::helper("adminhtml")->__("Fontcategory Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Fontcategory Description"), Mage::helper("adminhtml")->__("Fontcategory Description"));


		$this->_addContent($this->getLayout()->createBlock("fontcategory/adminhtml_fontcategory_edit"))->_addLeft($this->getLayout()->createBlock("fontcategory/adminhtml_fontcategory_edit_tabs"));

		$this->renderLayout();

		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {

					try {

						

						$model = Mage::getModel("fontcategory/fontcategory")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Fontcategory was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setFontcategoryData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $model->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setFontcategoryData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$model = Mage::getModel("fontcategory/fontcategory");
						$model->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}

		
}
