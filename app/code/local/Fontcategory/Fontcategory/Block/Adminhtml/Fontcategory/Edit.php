<?php
	
class Fontcategory_Fontcategory_Block_Adminhtml_Fontcategory_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "fontcategory_id";
				$this->_blockGroup = "fontcategory";
				$this->_controller = "adminhtml_fontcategory";
				$this->_updateButton("save", "label", Mage::helper("fontcategory")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("fontcategory")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("fontcategory")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("fontcategory_data") && Mage::registry("fontcategory_data")->getId() ){

				    return Mage::helper("fontcategory")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("fontcategory_data")->getId()));

				} 
				else{

				     return Mage::helper("fontcategory")->__("Add Item");

				}
		}
}