<?php

class Fontcategory_Fontcategory_Block_Adminhtml_Fontcategory_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("fontcategoryGrid");
				$this->setDefaultSort("fontcategory_id");
				$this->setDefaultDir("ASC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("fontcategory/fontcategory")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("fontcategory_id", array(
				"header" => Mage::helper("fontcategory")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "fontcategory_id",
				));
                
				$this->addColumn("fontcategory", array(
				"header" => Mage::helper("fontcategory")->__("fontcategory"),
				"index" => "fontcategory",
				));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		

}