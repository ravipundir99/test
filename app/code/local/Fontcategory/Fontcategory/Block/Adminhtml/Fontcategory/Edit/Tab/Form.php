<?php
class Fontcategory_Fontcategory_Block_Adminhtml_Fontcategory_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("fontcategory_form", array("legend"=>Mage::helper("fontcategory")->__("Item information")));

				
						$fieldset->addField("name", "text", array(
						"label" => Mage::helper("fontcategory")->__("fontcategory"),
						"name" => "name",
						));
						
				$fieldset->addField('status', 'select', array(
				'label'     => Mage::helper('clipartimage')->__('Status'),
				'name'      => 'status',
				'values'    => array(
				array(
					  'value'     => 1,
					  'label'     => Mage::helper('clipartimage')->__('Enabled'),
					  ),
				array(
					  'value'     => 2,
					  'label'     => Mage::helper('clipartimage')->__('Disabled'),
					  ),
				  ),
			  ));
						
						
						
					

				if (Mage::getSingleton("adminhtml/session")->getFontcategoryData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getFontcategoryData());
					Mage::getSingleton("adminhtml/session")->setFontcategoryData(null);
				} 
				elseif(Mage::registry("fontcategory_data")) {
				    $form->setValues(Mage::registry("fontcategory_data")->getData());
				}
				return parent::_prepareForm();
		}
}
