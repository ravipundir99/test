<?php


class Fontcategory_Fontcategory_Block_Adminhtml_Fontcategory extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_fontcategory";
	$this->_blockGroup = "fontcategory";
	$this->_headerText = Mage::helper("fontcategory")->__("Fontcategory Manager");
	$this->_addButtonLabel = Mage::helper("fontcategory")->__("Add New Item");
	parent::__construct();
	
	}

}