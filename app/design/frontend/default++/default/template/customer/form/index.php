<?php
	include "configuration.php";
?>
<!DOCTYPE html>
<html lang="en">
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
	<head>
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.ui.min.js"></script> 
		<script type="text/javascript" src="js/jquery.form.js"></script>
		<script type="text/javascript" src="js/jquery.validate.js"></script>
		<script type="text/javascript" src="js/jquery.ui.touch-punch.js"></script>
		<script type="text/javascript" src="js/json.js"></script>
		<script type="text/javascript" src="js/svg-js.js"></script>
		
		<script type="text/javascript" src="js/ui/jquery.ui.core.js"></script>
		<script type="text/javascript" src="js/ui/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="js/ui/jquery.ui.mouse.js">  </script>
		<script type="text/javascript" src="js/ui/jquery.ui.draggable.js"></script>
		<script type="text/javascript" src="js/ui/rotatable.js"></script>
		<script type="text/javascript" src="js/jquery.formatCurrency.min.js"></script>
		<script src="js/jquery.jqzoom-core.js" type="text/javascript"></script>
		
		<link href="css/styles.css" rel="stylesheet" type="text/css"/>
		<link href="css/stylenew.css" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="css/jquery.jqzoom.css" type="text/css">
		
		<style>
			.show{
				width:500px;
				height:400px;
				display:block;
			}
			.hide{
				width:500px;
				height:400px;
				display:none;
			}
			.handle{
				position:absolute;
				top:0px;
				left:0px;
			}
			.jqzoom{
               text-decoration:none;
	
            }
		</style>
		<style>




#headerTop{
overflow:hidden;
padding:5px;
background:url("images/bg-line.jpg"); 
border: 1px solid #ddd;
border-radius: 0px;
font-size:18px;
}

#l1:hover{background: url("images/icons_hover.jpg") no-repeat scroll left top transparent;}
#l2:hover{background: url("images/icons_hover.jpg") no-repeat scroll -36px top transparent;}
#l3:hover{background: url("images/icons_hover.jpg") no-repeat scroll -76px top transparent; margin-right:8px;}
#l4:hover{background: url("images/icons_hover.jpg") no-repeat scroll -120px top transparent;}

#headerTop .one{background: url("images/lines.jpg"); border-radius:5px; border:1px solid #ccc; margin-left:5%; width:auto; float:left; padding:5px 15px 0 10px;}
#headerTop .two{background: url("images/lines.jpg"); border-radius:5px; border:1px solid #ccc; margin-left:30%; width:auto; float:left; padding:5px 15px 0 10px;}
#headerTop .three{background: url("images/lines.jpg"); border-radius:5px; border:1px solid #ccc; margin-right:5%; width:auto; float:right; padding:5px 15px 0 10px;}
#headerTop .sel{background: url("images/sel-bg.png"); }
#headerTop span{background: url("images/cir.png"); width:28px; height:23px; text-align:center; float:left; margin-right:5px; padding-top:4px; position:relative; top:-2px; }
.arrange{
	border:1px solid #ddd;
	width:180px;
	margin:0 auto 10px;
	overflow:hidden;
	clear:both;
}
.arrange ul{list-style:none; margin:0; padding:0; }
.aling{
height:26px;
width:26px;
float:left;
margin:5px 0 0 5px;
cursor:pointer;
}

#l1{background: url("images/icons.jpg") no-repeat scroll left top transparent;}
#l2{background: url("images/icons.jpg") no-repeat scroll -36px top transparent;}
#l3{background: url("images/icons.jpg") no-repeat scroll -76px top transparent; margin-right:8px;}
#l4{background: url("images/icons.jpg") no-repeat scroll -120px top transparent;}


	
#resolution{z-index:5000;height:26px;width:26px;padding:5px;position: absolute;left:-50px;top:-50px}	
#resolution img{height:90%;width:90%;}	
.img-ldr{
height:20px;
width:20px;
left: 30px;
position: absolute;
top: 28px;
}
.full{
    background-image: url("images/star.gif");
    background-repeat: no-repeat;
    float: left!important;
    font-size: 2px;
    height: 16px;
    text-decoration: none;
    vertical-align: bottom;
    width: 16px;
}
.ui-rating a {
    cursor: pointer;
}
.empty {
    background-image:url("images/star.gif");
    background-repeat:no-repeat;
    float: left;
    font-size: 2px;
    height: 16px;
    text-decoration: none;
    vertical-align: bottom;
    width: 16px;
	background-position: left -32px;
}

.ui-rating{
float:right;
width:30%;
height:21px;
position:relative;
top:-20px;
}
</style>
<style>
@charset "utf-8";
/* CSS Document */


/*container start here*/
#container{
			width:63px;  
			height:63px;
			float:left;
			}
#container:hover .top-left, #container:hover .right-left, #container:hover .bottom-left, #container:hover .bottom-right{ opacity:1;}
#container:hover .panel{display:block;} 
.panel{
			width:63px; 
			height:63px;
			display:none;
}
.inner{
background:url(image/print-toll.png) no-repeat;
   	height: 52px;
    left:6px;
    position: relative;
    top:9px;
    width: 51px;
	z-index:1;
	cursor:pointer;
}
.inner:hover{
background:url(image/print-tollhover.png) no-repeat;
}
.panel-top {
    background:url(image/top-line-hover2.png) no-repeat;
   height: 4px;
    left:23px;
    position: relative;
    top: -68px;
    width: 16px;
	cursor:pointer;
}

.panel-top:hover{
background:url(image/top-line2.png) no-repeat;
}

.panel-bottom {
    background:url(image/top-line-hover2.png) no-repeat;
   height: 4px;
    left: 23px;
    position: relative;
    top: -85px;
    width: 16px;
	cursor:pointer;
}

.panel-bottom:hover{
background:url(image/top-line2.png) no-repeat;
}


.panel-left {
    background:url(image/mid-line-hover2.png) no-repeat;
    height: 16px;
    left:0px;
    position: relative;
    top:-69px;
    width: 4px;
	cursor:pointer;
}

.panel-left:hover{
background:url(image/mid-line2.png) no-repeat;
}

.panel-left-top {
    background:url(image/top-left-hover2.png) no-repeat;
    height: 20px;
    position: relative;
    top: -48px;
    width: 20px;
	cursor:pointer;
}

.panel-left-top:hover{
background:url(image/top-left2.png) no-repeat;
}

.panel-left-bottom {
    background:url(image/bottom-left-hover2.png) no-repeat;
    height: 20px;
    left: 0px;
    position: relative;
    top:-81px;
    width: 20px;
	cursor:pointer;
}

.panel-left-bottom:hover{
background:url(image/bottom-left2.png) no-repeat;
}


.panel-right {
    background:url(image/mid-line-hover2.png) no-repeat;
   height: 16px;
    left: 59px;
    position: relative;
    top:-85px;
    width: 4px;
	cursor:pointer;
}

.panel-right:hover{
background:url(image/mid-line2.png) no-repeat;
}

.panel-right-bottom {
    background:url(image/bottom-right-hover2.png) no-repeat;
   height: 20px;
    left:43px;
    position: relative;
    top: -105px;
    width: 20px;
	cursor:pointer;
}

.panel-right-bottom:hover{
background:url(image/bottom-right2.png) no-repeat;
}

.panel-right-top {
    background:url(image/top-right-hover2.png) no-repeat;
   height: 20px;
    left: 43px;
    position: relative;
    top:-72px;
    width: 20px;
	cursor:pointer;
}

.panel-right-top:hover{
background:url(image/top-right2.png) no-repeat;
}


.top{
    cursor: pointer;
    height:5px;
    left:21px;
    position: relative;
    top:-1px;
    width:9px;
	background:url(image/arrow-t-hover.png) no-repeat;
	
}
.top:hover{
background:url(image/arrow-t-hover.png) no-repeat;
cursor:pointer;
}
.bottom{
  cursor: pointer;
    height: 5px;
    left: 21px;
    position: relative;
    top: -3px;
    width:9px;
	background:url(image/arrow-d-hover.png) no-repeat;
	
	
}
.bottom:hover{
background:url(image/arrow-d-hover.png) no-repeat;
cursor:pointer;
}


.left {
    background: url(image/arrow-l-hover.png) no-repeat scroll 0 0 transparent;
    cursor: pointer;
     height:9px;
    left:5px;
    position: relative;
    top:3px;
    width: 5px;
}
.left:hover{
background:url(image/arrow-l-hover_copy.png) no-repeat;
cursor:pointer;
}
.right {
     height:9px;
    left: 40px;
    position: relative;
    top: -6px;
    width: 5px;
	background:url(image/arrow-r-hover.png) no-repeat;
	cursor:pointer;	
}

.right:hover{
background:url(image/arrow-r-hover_copy.png) no-repeat;
.cursor:pointer;
}

.top-left{
    height: 7px;
    left:4px;
    position: relative;
    top:6px;
    width: 7px;
	opacity:0;
	background:url(image/arrow-t-l-hover.png) no-repeat;	
	cursor:pointer;
}

.top-left:hover{
background:url(image/arrow-t-l.png) no-repeat;
cursor:pointer;
}

.center{
	height: 7px; left:19px; position:absolute;top:21px;
    width: 7px;cursor:pointer;
}

.bottom-left{
  height:8px;
    left: 5px;
    position: relative;
    top:2px;
    width:9px;
	opacity:0;
	background:url(image/arrow-l-d-hover.png) no-repeat;	
	cursor:pointer;
}

.bottom-left:hover{
background:url(image/arrow-l-d.png) no-repeat;
cursor:pointer;
}

.right-left{
   height:8px;
    left:37px;
    position: relative;
    top:-6px;
	opacity:0;
    width:9px;
	background:url(image/arrow-t-r-hover.png) no-repeat;
	cursor:pointer;	
}

.right-left:hover{
background:url(image/arrow-t-r.png) no-repeat;
cursor:pointer;
}

.bottom-right{
   height:8px;
    left:37px;
    position: relative;
    top: -11px;
	opacity:0;
    width:9px;
	background:url(image/arrow-r-d-hover.png) no-repeat;	
	cursor:pointer;
	}
	
.bottom-right:hover{
background:url(image/arrow-r-d.png) no-repeat;
cursor:pointer;
}



.tooltip{background:black; color:white; font-size:11px; padding:2px 4px; position:relative;display:none;z-index:9991;}
.tooltip:after{
    border-color: transparent transparent  #000 transparent;
    border-style: solid;
    border-width: 7px;
    content: "";
    display: block;
   /* height: 0;*/
    left:8px;
    position: absolute;
    top: -13px;
    /*width: 0;*/
	z-index:9990;
}

.reso-tooltip{background:black; color:white; font-size:11px; padding:2px 4px; position:relative;display:none;z-index: 3496;}
/* resizable */




</style>	
	</head>
	<body>
    <div id="main" onselectstart="return false;">
	<div id="in-con"></div>
	<div class="content">
		<!-- left side start here -->
		<?php if($_REQUEST['xtype']=='U'){ ?>
        <div id="left">
		<div class="pr">Switch Product!
			<div class="pr-ar"><img id='switch_button' src="images/ar-shape.png" tooltext='Design a new Product?'/></div>
		</div>
		
		<div class="left-side">
			<div id="holder"></div>
			
		<ul id="tabing" style="list-style:none; padding:0px">
	<!--	<li id="t_1" class="tem">Template</li> -->
	    <li id="t_2" class='select' tooltext='Add Our Designs'>Template</li>
		<li id="t_3" class="shp" tooltext='Use Shapes'>Shapes</li>
		</ul>
    <!--    <ul id="tab_1">
        <li><img src="images/icon+1.png" alt="" /><a href="#">mndfdkkjdf</a></li>
        <li><img src="images/icon+2.png" alt="" /><a href="#">Mugs</a></li>
        <li><img src="images/icon+3.png" alt="" /><a href="#">Posters</a></li>
        <li><img src="images/icon+4.png" alt="" /><a href="#">Collage</a></li>
        <li><img src="images/icon+5.png" alt="" /><a href="#">Key Rings</a></li>
        </ul> -->
     <!--   <ul id="tab_2" class="tb" style="display:block">
        <li><img src="images/image.png" alt="" /></li>
        <li><img src="images/image.png" alt="" /></li>
        <li><img src="images/image.png" alt="" /></li>
        <li><img src="images/image.png" alt="" /></li>
        <li><img src="images/image.png" alt="" /></li>
        <li><img src="images/image.png" alt="" /></li>
        </ul>-->
    <!--<ul id="tab_3" class="tb" style="display:none">
				<li><img src='svg-images/circle.jpg' data='<circle id="redcircle" cx="50%" cy="50%" r="50" fill="red" style="stroke:#ccc;stroke-width:5;"/>'> </li>
				<li> <img src='svg-images/oval.jpg' data='<ellipse id="ellipse" fill="red" ry="25" rx="50" cy="50%" cx="50%"/>'> </li>
				<li> <img src='svg-images/polygon.jpg' data='<polygon id="polygon" points="60,20 100,40 100,80 60,100 20,80 20,40" fill="green" stroke="#000" stroke-width="1" />'> </li>
				<li> <img src='svg-images/star.jpg' data='<polygon id="starPolygon" points="100,10 40,180 190,60 10,60 160,180 100,10" style="fill:red;stroke:black;stroke-width:1"/>'> </li>
				<li id='tringle'> <img src='svg-images/tringle.jpg' data='<path id="triangle" d = "M 250 50 L 300 150 L 200 150 L 251 50" stroke = "black" stroke-width = "3" fill = "red"/>'></li>	
        </ul>-->
		
		</div>
      </div> 
		<?php }; ?>
    <!-- left side end here -->
		
		<div class="right-side" <?php if($_REQUEST['xtype']=="A"){echo "style=left:10px;";}; ?>>
			<div id="tab">
				<div class="line"></div>
				<ul>
					<li><span>1</span><a href="#">Select</a></li>
					<li class="cpt"><span>2</span><a href="#">Personalize</a></li>
					<li><span>3</span><a href="#">Order</a></li>
				</ul>
			</div>
			
			<div class='menucontainer'>
            	<ul>
					<li id="productMode" tooltext="Switch to Product Preview">Product View</li>
                    <li id="DesignMode" tooltext="Switch to Designing">Design View</li>
                </ul>
                <?php include("tool-bar.php");?>
			</div>
			
			<div id="container" style="position:relative; width:99.6%; height:445px; margin-top:5px; float:left; background:#FFFFFF; border:1px solid #DDDDDD; overflow:hidden;">
				<div id="zoomIn-btn" class="toolbarZoomInUp" tooltext="Zoom-In"></div>
			     <div id="zoomOut-btn" class="toolbarZoomOutUp" tooltext="Zoom-Out"></div>
				<div id="editor" class="editor">
					<div id="editor-base" style="position:absolute; width:100%; height:100%;"></div>
					<div id="designEditor" class="editor-container" style="z-index: 20;">
						<div id="base-layer" style="position:absolute; width:100%; height:100%; background:#FFFFFF"></div>
						<!--<div id="image-layers" style="position:absolute; width:100%; height:100%;"></div>-->
						<!--<div id="text-layers" style="position:absolute; width:100%; height:100%;"></div>-->
						<div id="editor-layers" style="position:absolute; width:100%; height:100%;"></div>
						<div id="mask-layer" style="position:absolute; width:100%; height:100%;"><img src=""/></div>
						<div id="inner-mark-layer" style="position:absolute; width:100%; height:100%;"></div>
						<div id="handler-layers" style="position:absolute; width:100%; height:100%;"></div>
					</div>
					
					<div style="z-index: 21; position:absolute; top:0px; left:0px;">
						<div id="move_handler" class="handle" style="border:1px dashed #D95700;">
							<div id="handler_position_display" style="border:1px solid #D95700; position:absolute; width:90px; background:#FFFFCC; top:-30px; left:8px; padding-left:5px; padding-right:5px; display:none;">
								<span style="font-size:10px; color:#000000; z-index:10;"></span>
							</div>
						</div>
					</div>
				</div>
			<!--	<div style="position:absolute; width:93%; height:20px; top:0px; left:20px; background:#FFFFFF; overflow:hidden">
					<img id="rulerH" src="php/ruler.php?direction=H&editorW=300&editorH=300&containerW=3.5&containerH=2.5" style="position:absolute; top:0px;left:0px;"/>				</div>
				<div style="position:absolute; height:357px; width:20px; top:20px;left:0px; background:#FFFFFF; overflow:hidden">
					<img id="rulerV" src="php/ruler.php?direction=V&editorW=300&editorH=300&containerW=3.5&containerH=2.5" style="position:absolute; top:0px;left:0px;"/>				</div>-->
				<div style="position:absolute; background:#F7F7F7; solid #999999; padding:2px; width:14px; top:26px; right:0px; bottom:18px;">
					<div id="vslider" direction="V" style="position:absolute; background-color:#ddd; width:14px; height:100px; top:2px; left:2px;">	</div>
				</div>
				<div style="position:absolute; background:#F7F7F7; padding:2px; height:14px; bottom:0px; left:18px; right:18px;">
					<div id="hslider" direction="H" style="position:absolute; background-color:#ddd; width:100px; height:14px; top:2px; left:2px;">	</div>
				</div>
			<div class="lft-ar"><img id="seePrevViewBtn" tooltext="Previous View" src='images/lft-ar.png'></div>
			<div class="rt-ar"><img id="seeNextViewBtn" tooltext="Next View" src='images/rt-ar.png'></div>
			</div>
			<div id="preview-section" style="display:none;border: 1px solid #DDDDDD; margin-top: 5px;">
				<div class='3dpreview' style="position:relative; margin:auto; text-align:center; height:420px;">
				
					<img src="images/products/preview.jpg" style="height:350px;width:300px" />
				
				</div>
				<div id="controlBtn" style="width;100%; text-align:center; position:relative;">		
					<img id='leftRotateBtn' src="images/rotate_button_left.jpg" />
					<img id='rightRotateBtn' src="images/rotate_button_right.jpg" style="margin-left:-4px;" />
				</div>
				
			</div>
            <div id="save-share">
				<ul>
				<li><a href="#" id="save-btn" tooltext='Want to Save Design'>Save</a></li>
				<li>
				<!-- AddThis Button BEGIN -->
					<a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=300&amp;pubid=ra-508284fa3bd80ea2"></a>
					<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
					<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-508284fa3bd80ea2"></script>
					<!-- AddThis Button END -->
				</li>
				<li><a href="#" id="start-btn" tooltext='Reload Design'>Start-Over</a></li>
				</ul>
			</div>
			<div id="viewHolder"></div>
			<div id="productDesc"><b>Product Description:</b><p></p></div>
		</div>
        
    <!----------------------------------------------------------------->
		<div id="rightmost"><div id='resolution'><img src=""></div>
			<div id="cart">
					<span class="productTitle"></span><span style="display:none" id='prosku'></span>
					<div class="ui-rating">
                    	<a></a>
						<a></a>
						<a></a>
						<a></a>
						<a></a>
						
					</div>
					<div id="seeDetail" tooltext="See Details" class="see-detail">[See Details]</div>
                     <b>In stock!</b> 
                     <a class="wish" href="#">Add to Wishlist</a>
                    <ul>
                        <li><b>Quantity:</b><input id="qty" type="text" value="1"/>
                        	<span id='blkPrice'></span> 
							<span id='blkOpt' style="visibility:hidden">in Bulk Quantity?</span>
							<span class="qtyValidator">Please enter valid number.</span>
						</li> 
                        <li><b>Each at</b><span id="price"> </span></li>
                        <li><div id="viewOptions"></div></li>
                        <li><b>total :</b><strong><span id="totalPrice" class="currency"> </span></strong></li>
                     </ul>
                     
                    
                    </span>
				<!--	<div id="viewOptions" style="display:block" class="controls"></div>-->
                	
					<div class="cartbtn">Add To Cart</div>
                    
                     <div class="frm">
                     <input name="" id="cartCheck" type="checkbox" value="" /> <p>I agree that spelling content and layout are correct. I understand my product will print exactly as it appears & I cannot make any changes once my order has been placed.</p>
                     </div>
				<div id="Bulk-pricePopUp" style="display:none">
		            	<div class="tops">
		                    <span>Edit Your Text</span>
		                </div>
		                <div id="bulk-content"></div>
		        </div>
				
			</div>
			
			<div id="customize">
	            	<div class="label">
	                	Customize!
	                    <em id="guide" tooltext="Image Upload Guidelines">Image Upload Guidelines</em>
	                </div>
					<div class="toolbar-option-pa">
                    			<div id="addImage-btn" tooltext="Click To Add Image" class="toolbarAddImageUp"></div>
								<div id="addText-btn" tooltext="Click To Add Text" class="toolbarAddTextUp"></div>
					</div>
				<div id="paninig-rotate" style="display:none; height:70px; margin-left:29px; width:257px; ">	
				<div id="container" style="margin-left:10px;">
    							 <div class="inner">
									 <div direction="NW" class="top-left"></div>
									 <div direction="N" class="top"></div>
									 <div direction="NE" class="right-left"></div>
									 <div direction="W" class="left"></div>
									 <div direction="Center" class="center"></div>
									 <div direction="E" class="right"></div>
									 <div direction="SW" class="bottom-left"></div>
									 <div direction="S" class="bottom"></div>
									 <div direction="SE" class="bottom-right"></div>
								</div>
								<div class="panel">
									<div class="panel-left-top" direction="LT"></div>
									<div class="panel-top" direction="T"></div>
									<div class="panel-right-top" direction="RT"></div>
									<div class="panel-left" direction="L"></div>
									<div class="panel-right" direction="R"></div>
									<div class="panel-left-bottom" direction="LB"></div>
									<div class="panel-bottom" direction="B"></div>
									<div class="panel-right-bottom" direction="RB"></div>
								</div>
				</div>
			<div style="float: left; position: relative; width: 105px; margin-top: 10px; margin-left: 50px;">
				<div class="rt-txt" style="background:url(images/Blender-meta-ball1.png)">Rotate</div>	
				<div class="rt-bx">				
					<img src="images/Blender-meta-ball.png" width="100%" height="100%"  id="element" />
				</div>
				<div id="rotateResetBtn" class="reset" tooltext="Reset"><a>Reset</a></div>
			</div>
			
			
			<script type="text/javascript">
			$(document).ready(function(){
			$('#element').parent().rotatable();
			
				$('#element').click(function(){
					return false;
				});
			});
			</script>
		</div>

		<!--	<div class='arrange'>
            	<ul>
                	<li value="UP" class='aling' id='l1' tooltext="BringForward"></li>
                    <li value="DOWN" class='aling' id='l2' tooltext="SendBackward"></li>
                    <li value="FRONT" class='aling' id='l4' tooltext="BringToFront"></li>
					<li value="BACK" class='aling' id='l3' tooltext="SendBack"></li>
                </ul>    
			</div>  -->

			<div class="box">
				<div class="box-head label"><span style="position: relative; top: 2px;"><input type="checkbox" id="selectAllLayerCB" >Select All</span></div>
				<div id="layerContainer" style="overflow-y:auto; float:left; width:100%; height:77%">
					
				</div>
		    </div>
				
			</div>
		   
			<div class="sml">
				<div class="box-head">Similar Products</div>
				<div id="similar_products">
					
				</div>
			</div>
		</div>
		
		<!------------------------------------------------------------------>
		
		<?php include("color-picker.php");?>
	</div>
	<div class="edit-menu" style="display:none;">
		<div><img src="images/picture.png"><span>EDIT</span><img src="images/arrow_down.png"></div>
		<ul style="display:none;">
			<li>Crop and Rotate</li>
			<li>Replace</li>
			<li>Add Photo Effect</li>
			<li>Lock in place</li>
			<li>Delete</li>
		</ul>
	</div>
	<?php include("layout-panel.php");?>
	<?php include("save-panel.php")?>
	<?php include("loader-panel.php")?>
	
	<iframe id="library-iframe" src="library/index.html?uid=<?php echo time() ?>" class="libraryframe" style="display:none;" scrolling="no"></iframe>
	<iframe id="color-picker-iframe" src="color-picker/index.html?uid=<?php echo time() ?>" style="position:absolute; border:0px; width:275px; height:195px; left:649px; top:44px; z-index:3000; display:none;"></iframe>
	<!--<iframe id="color-effects-iframe" src="color-effects/index.html?uid=<?php echo time() ?>" class="coloreffectsframe" style="display:none;" scrolling="no"></iframe>-->
	<div id="messagePanel" class="popup-window" style="display:none;">
		<div class="title"><h1>Saving design</h1></div>
		<div style="text-align:center; padding:20px">
			<img src="images/loader.gif"><span> Please Wait...</span>
		</div>
	</div>
	
	<div id="backgroundPopup"></div>
		
	<!--------------------------OPTION PANEL------------------------------------------->
	<div id="textPanel" class="optionPanel" style="display:none">
		<div id="top" class='tops'>
        	<span>Edit Your Text</span>
            <div class="cross">Delete</div>
        </div>
	<div class="toolbar-option-panel">
		<div id="text-tool" style="display: block;">
        	<div class="toolnm">
                <div class="studioToolbarSectionLabel">Font Size</div>
                <div id="textSizeSelectBox">14</div>
            </div>
            <div class="toolar" style="width:130px;">
            <div class="studioToolbarSectionLabel">Font Category</div>
			<div class="textSizeDropDown">
				<ul><li value="9">9</li><li value="10">10</li><li value="11">11</li><li value="12">12</li><li value="14">14</li><li value="16">16</li><li value="18">18</li><li value="20">20</li><li value="24">24</li><li value="28">28</li><li value="32">32</li><li value="36">36</li><li value="40">40</li><li value="50">50</li><li value="60">60</li></ul>
			</div>
			
			<div class="fontBtnStyle" id="fontBtn">Arial</div>
			<div style="display:none" class="fontDropDownStyle" id="fontDropDown">
				<div>
					<ul id="fontcategorylist"><li id="1">Bold(8)</li><li id="2">Classic	(1)</li><li id="3">Fun(2)</li><li id="5">International(0)</li><li id="6">Modern(0)</li><li id="7">Script(0)</li><li id="8">Symbols(0)</li></ul>
					<ul id="fontslist">
						
					</ul>
				
				</div>
			</div>
            </div>
			
            <div class="toolsp">
            	<div class="studioToolbarSectionLabel">Line Spacing</div>
				<div id="lineSpacing" class="studioToolbarLineSpacing">35</div>
            </div>
            <div class="toolar">
            	<div class="studioToolbarSectionLabel">Character Spacing</div>
				<div id="characterSpacing" class="studioToolbarCharSpacing">25</div>
            </div>
             
			 <div class="toolal">
            	<div class="studioToolbarSectionLabel">Alignment</div>
                <div class="toolbarAlignLeftUp" id="align-left-btn" tooltext="Left"></div>
                <div class="toolbarAlignCenterUp" id="align-center-btn" tooltext="Center"></div>
                <div class="toolbarAlignRightUp" id="align-right-btn" tooltext="Right"></div>
			</div>
            <div class="studioToolbarSectionfontcolor" tooltext="Color"></div>
            <div class="toolas">
            	<div class="studioToolbarSectionLabel">Font Style</div>
                <div class="boldBtnDisabled" selected="0" id="bold-btn" tooltext="Bold"></div>
                <div class="italicBtnDisabled" selected="0" id="italic-btn" tooltext="Italic"></div>
            </div>
            <div class="toolas">
            	<div class="studioToolbarSectionLabel">Insert</div>
				<div id="insertSymbol" class="studioToolbarSymbol">Symbol</div>
			</div>
			 
			<div class="symbolDropDown">
				<ul>
					<li value="&copy"> &laquo </li>
					<li value="&laquo"> &laquo </li>
					<li value="&reg"> &reg </li>
					<li value="	&micro"> &micro </li>
					<li value="&frac14"> &frac14 </li>
					<li value="&frac12"> &frac12 </li>
					<li value="&laquo"> &laquo </li>
					<li value="&para"> &para </li>
					<li value="&ordm"> &ordm </li>
					<li value="&Oslash"> &Oslash </li>
					<li value="&thorn"> &thorn </li>
				</ul>
			</div>
			
			<div class="linespacingDropDown">
				<ul><li value="1">1</li><li value="2">2</li><li value="3">3</li><li value="4">4</li><li value="5">5</li></ul>
			</div>
			
			<div class="characterspacingDropDown">
				<ul><li value="1">1</li><li value="2">2</li><li value="3">3</li><li value="4">4</li><li value="5">5</li></ul>
			</div>
		</div>
		
	</div>
	
	
	
		<div class="tx-con">
        	<span>Type Your Text</span>
				<div>
				<textarea id="textInsert" class="txarea" value="Enter Your Text" type="text"></textarea>
				</div>
		</div>
	</div>
	<div id="imagePanel" class="optionPanel" style="display:none;height:100px">
		<div id="imageHeader" class='tops'>
			<span style="color:black;line-height:1.9em;margin-left:9px;">Edit Your Image</span>
			<div class="cross">Delete</div>
		</div>
		<div id="image-tool">
			<div class="studioToolbarSectionLabel">Image Options</div>
			<a id="effects-btn" selected="0" class="effectBtn"></a>
			<a id="lock-btn" selected="0" class="lockBtn" style="display:block;"></a>
			<a id="unlock-btn" selected="0" class="unlockBtn" style="display:none;"></a>
			<a id="replace-btn" selected="0" class="replaceBtn"></a>
			<a id="imgfill" selected="0" class="fill"></a>
			<a id="imgfit" selected="0" class="fit"></a>
			<a id="removewhite" selected="0" class="removewhite"></a>
			<div style="left: 282px;position: relative;top:0px;" tooltext="Transparent Background"> <span><input id="remove-White" type=checkbox /> Transparent</span>
			<div id="rstImg" tooltext="Reset" style="position: absolute;top:17px; left:24px;">Reset </div>
            </div>
		</div>
	</div>
	<!--  Panel For Svg Img -->
	
	<div id="svgPanel" class="optionPanel" style="display:block;height:80px;width:310px;">
		<div id="svgHeader" class='tops'>
			<span style="color:black;line-height:1.9em;margin-left:9px;">Edit Your Svg Element</span>
			<div class="cross">Delete</div>
		</div>
	<div id="svg-tool">		
		<div class="toolar" style="width:120px;">
			<div>
				<div class="studioToolbarSectionLabel">Shape</div>		
				<div class="svgShape" id="shape">Circle</div>
			</div>			
			<div style="display:none;margin-left:3px;width:98px;top:82px;height:auto;" class="imgWidthDropDown" id="strokeDropDown">				
					<ul style="cursor:pointer"><li id="1" Shape='CIRCLE'>Circle</li><li id="2" Shape='RECTANGLE'>Rectangle</li><li id="3" Shape='CLOUD'>Cloud</li><li id="5" Shape='STAR'>Star</li><li id="6" Shape='SQUARE'>Square</li><li id="7" Shape='HEART'>Heart</li></ul>									
			</div>
        </div>
		<div>
			<span style='margin-left:-28px;'> Fill Color </span>
		<!--	<span class="colorStrip" id='rightcolorStrip'></span>-->
			<div id='fillclr' class="studioToolbarSectionImgcolor"></div>
			
		</div>
		<div style='margin-left:190px;margin-top:-17px;'>
			<span style='margin-left:-28px;'> Stroke Color</span>
		<!--	<span class="colorStrip" id='leftColorStrip'></span>-->
			<div id='strokeclr' class="studioToolbarSectionImgcolor"></div>			
			
		</div>		
		<div style="margin-left:240px;margin-top:-17px;" id='strokeWidthControl'>
				<div class="toolstr" >
					<div class="studioToolbarSectionLabel" style='padding-left:0px;'>Stroke Width</div>
					<div class="strokeWidth" id="svgSizeSelectBox">2</div>
				</div>
				<div class="imgWidthDropDown" id="stroke_width" style='display:none;top:83px;margin-left:2px;'>
					<ul style='cursor:pointer'><li value="1">1</li><li value="2">2</li><li value="3">3</li><li value="4">4</li><li value="5">5</li><li value="6">6</li><li value="7">7</li><li value="8">8</li><li value="9">9</li><li value="10">10</li></ul>
				</div>
		</div>				
		</div>
	</div>
	<!--  END -->
	
	<div class='reso-tooltip'> </div>
	<div id="cartPopup" style="display:none;height:158px;width:365px;position:absolute;top:130;left:500;z-index:2001; padding:0 15px; text-align:justify; box-shadow: 0 0 13px 5px #999999;backGround:#FFFFFF;top:200px;left:484px">
		<p style="font-size:12px">I agree that spelling content and layout are correct. I understand my product will print exactly as it appears & I cannot make any changes once my order has been placed.</p>
		<ul class="add_to_card">
			<li class="ys"><a href="#" id="proceed">Yes, I Agree</a></li>
			<li><a href="#" id="cncl">NO</a></li>
		</ul>
	</div>
	<div id="resoPopup" style="display:none;height:158px;width:365px;position:absolute;top:130;left:500;z-index:2001; padding:0 15px; text-align:justify; box-shadow: 0 0 13px 5px #999999;backGround:#FFFFFF;top:200px;left:484px">
		<p style="font-size:12px">Image Quality is low and print impression will not come well after printing. We recommend you to upload picture of high resolution. However, You can proceed with the same image if print quality is not a concern to you.</p>
		<ul class="add_to_card">
			<li class="ys"><a href="#" id="proceed">Yes, I Agree</a></li>
			<li class="no"><a href="#" id="cncl">NO</a></li>
		</ul>
	</div>
	<div id="detailPopup">
		<div class="tops" id="top">
		<span>Product Specification</span>
		<div class="cross">Delete</div>
		</div>
		<iframe id='specIframe' src='specipication.php'></iframe>
	</div>
	<div id="guideline">
          	 	<img src="images/image-upload-guidelines.png">
                <div class="removeGuidelines"></div>
    </div>
	
	<div class='tooltip' style="display:none;"></div>
	<div class='preview-tip' style="background: none repeat scroll 0 0 #F2F5A9;border: 1px solid gray;position: absolute;font-size:10px;display:none;z-index:5000">Click to Go To Design View with drag & drop Controls</div>
	<!--------------------------------------------------------------------------------->
	<div id="ajaxloader"></div>
	<div id="ZoomPopup" style="height:505px;width:505px;display:none;border:3px groove gray;position:absolute;left:10px;top:50px;z-index: 200;background:#fff">
	<img src="images/products/preview.jpg" />
	</div>
	</body>
</html>
	<?php
		$jscript = '<script type="text/javascript">';
		if($_REQUEST['width']){
			$jscript .='var width='.$_REQUEST['width'].';';
		}else{
		    $jscript .='var width=0;';
		}
		if($_REQUEST['height']){
			$jscript .= 'var height='.$_REQUEST['height'].';';
		}else{
		    $jscript .= 'var height=0;';
		}
		if($_REQUEST['product']){
			$jscript .= 'var productId='.$_REQUEST['product'].';';
		}else{
			$jscript .= 'var productId=0;';
		}
		if($_REQUEST['design']){
			$jscript .= 'var designId='.$_REQUEST['design'].';';
		}else{
			$jscript .= 'var designId=0;';
		}
		if($_REQUEST['utype']){
			$jscript .= 'var utype='.$_REQUEST['utype'].';';
		}else{
			$jscript .= 'var utype=0;';
		}
		if($_REQUEST['xtype']){
			$jscript .= 'var xtype="'.$_REQUEST['xtype'].'";';
		}else{
			$jscript .= 'var xtype="U";';
		}
		if($_REQUEST['isTemplate']){
			$jscript .= 'var isTemplate='.$_REQUEST['isTemplate'].';';
		}else{
			$jscript .= 'var isTemplate=0;';
		}
		if($_REQUEST['totalpages']){
			$jscript .= 'var totalpages='.$_REQUEST['totalpages'].';';
		}else{
			$jscript .= 'var totalpages=1;';
		}
		if($_REQUEST['preview']){
			$jscript .= 'var preview='.$_REQUEST['preview'].';';
		}else{
			$jscript .= 'var preview=0;';
		}
		if($_REQUEST['ptype']){
			$jscript .= 'var ptype="'.$_REQUEST['ptype'].'";';
		}else{
			$jscript .= 'var ptype="business-card";';
		}
		if($_REQUEST['xtra']){
			$jscript .= 'var xtra="'.$_REQUEST['xtra'].'";';
		}else{
			$jscript .= 'var xtra="";';
		}
		if($_REQUEST['attributeId']){
			$jscript .= 'var attributeId='.$_REQUEST['attributeId'].';';
		}else{
			$jscript .= 'var attributeId=0;';
		}
		if($_REQUEST['showlayouts']){
			$jscript .= 'var showlayouts='.$_REQUEST['showlayouts'].';';
		}else{
			$jscript .= 'var showlayouts=0;';
		}
		if($_REQUEST['categoryId']){
			$jscript .= 'var categoryId='.$_REQUEST['categoryId'].';';
		}else{
			$jscript .= 'var categoryId=0;';
		}
		if($_REQUEST['img']){
			$jscript .= 'var urlImage="'.$_REQUEST['img'].'";';
		}else{
			$jscript .= 'var urlImage="";';
		}
		$jscript .='</script>';
		echo $jscript;
	?>
	
<script type="text/javascript" src="js/studio.js"></script>
<script type="text/javascript" src="js/studio-extra.js"></script>

<script type="text/javascript">
$(document).ready(function(){
		


loadShapes();
var loadProducts=function(){
$.ajax({
		type: "POST",
		url: "php/getcategory.php",
		success: function(data){
				
			var products = JSON.parse(data);
			var proList="<ul id='tab_1' >";		
			for(i=0;i<products.length;i++){
				proList+="<li id='proId_"+products[i].id+"' type='"+products[i].type+"'><img src='php/resizeImage.php?url="+products[i].thumbnail+"&height=20&width=20'><a href='#'>"+products[i].title+"</a></li>"
			//	console.log(products[i].title+products[i].thumbnail);
			}
			proList+="</ul>";
			$(".left-side").append(proList);
			$('#tab_1 li').bind('click',loadNewProduct);
		}
	});
}
loadProducts();
var loadNewProduct=function(){
		if($(this).attr('type')=='simple'){
			alert('Loading New Product..! ');
				for(var k=js[currentView].length-1;k>=0;k--){
							deleteElementByIndex(js[currentView][k].id);
				}
			$('#handler-layers').children('.item-loader').remove();
			isTemplate=0;
			designId=0;
			currentDesign=0;
			alert('productId:'+$(this).attr('id').split('_')[1]+'isTemplate:'+isTemplate+"----"+'designId:'+designId);
			loadProductById($(this).attr('id').split('_')[1]);
			$('#viewHolder').html('');
			$('#DesignMode').trigger('click');
		}else if($(this).attr('type')=='bundle'){
			
			$('#bndl-popup').remove();
			var popupX=$(this).offset().left+120;
			var popupY=$(this).offset().top-10;
			var id=$(this).attr('id').split('_')[1];
			
			$.ajax({
				type: "POST",
				url: "php/getBundleItemList.php",
				data:'productId='+id,
				success: function(data){
				
					products = JSON.parse(data);
					var proList='';
				    proList+="<ul style=''>";		
					for(i=0;i<products.length;i++){
						proList+="<li id='proId_"+products[i].id+"' type='"+products[i].type+"'><img src='php/resizeImage.php?url="+products[i].thumbnail+"&height=20&width=20'><a href='#'>"+products[i].title+"</a></li>"
					}
				    proList+="</ul>";
					
					$('<div id="bndl-popup" class="bndl-popup" >'+proList+'</div>').appendTo('.left-side');
					$('#bndl-popup').css({'left':popupX,'top':popupY});
					
					$('#bndl-popup ul li').bind('click',function(){
					
								alert('Loading New Product..! ');
									for(var k=js[currentView].length-1;k>=0;k--){
												deleteElementByIndex(js[currentView][k].id);
									}
								$('#handler-layers').children('.item-loader').remove();
								isTemplate=0;
								designId=0;
								currentDesign=0;
								//alert('productId:'+$(this).attr('id').split('_')[1]+'isTemplate:'+isTemplate+"----"+'designId:'+designId);
								loadProductById($(this).attr('id').split('_')[1]);
								$('#viewHolder').html('');
								$('#DesignMode').trigger('click');
								$('#bndl-popup').remove();
								$('ul#tab_2').html('');
					});	
				}
			});
		}
}

/*
productShortDesc=function(descid){
$.ajax({
		type: "GET",
		url: "php/shortdesc.php",
		data:"productid="+descid,
		success: function(data){
			$('#detailPopup #cntnt').html(data);	
			
		}
	});
}
productShortDesc();*/

//----------------------------------------------------------------------------------------------

var loadDesignTemplates=function(){
$.ajax({
		type: "GET",
		url: "php/siblings.php",
		data:"productid="+productId+"&category="+categoryId,
		success: function(data){
				
			var designTemplates = JSON.parse(data);
			if(designTemplates){
			var designList="<ul id='tab_2' class='tb'>";		
			for(i=0;i<designTemplates.length;i++){
				designList+="<li style='position:relative' id='proId_"+designTemplates[i].id+"' designid='"+designTemplates[i].designid+"' tooltext='"+designTemplates[i].title+"'><div class='img-ldr'><img src='images/printLoader.gif' style='height:100%;width:100%' /></div><img src='php/resizeImage.php?url="+designTemplates[i].imageurl+"&height=80&width=70' /></li>"
			}
			designList+="</ul>";
			$(".left-side").append(designList);
			$('#tab_2 li img').load(function(){
			$(this).siblings('.img-ldr').hide();
			});
			$('#tab_2 li').bind('click',loadNewDesignTemplate);
		}
		}
	});
}
loadDesignTemplates();

var loadNewDesignTemplate=function(){
alert('Loading Your designs..! ');
isTemplate=1;
jsbackup=js;
		for(var k=js[currentView].length-1;k>=0;k--){
					deleteElementByIndex(js[currentView][k].id);
		}
$('#handler-layers').children('.item-loader').remove();
loadDesign($(this).attr('designid'));
$('#DesignMode').trigger('click');
}
//----------------------------------------------------------------------------------------------
$("#top").mousedown(function(){	
	$(this).parent().draggable('enable');
	$(this).parent().draggable({containment: "parent"});
});
$("#top").mouseup(function(){
	$(this).parent().draggable('disable');
});
$("#top").mouseout(function(){
	$(this).parent().draggable('disable');
});

$('.cross').click(function(){
$(this).parent().parent().fadeOut();
});
//-------------------------------------------------------
$("#imageHeader").mousedown(function(){	
	$(this).parent().draggable('enable');
	$(this).parent().draggable({containment: "parent"});
});
$("#imageHeader").mouseup(function(){
	$(this).parent().draggable('disable');
});
$("#imageHeader").mouseout(function(){
	$(this).parent().draggable('disable');
});

$("#svgHeader").mousedown(function(){	
	$(this).parent().draggable('enable');
	$(this).parent().draggable({containment: "parent"});
});
$("#svgHeader").mouseup(function(){
	$(this).parent().draggable('disable');
});
$("#svgHeader").mouseout(function(){
	$(this).parent().draggable('disable');
});
//---------------------------------------------------------------------------

$("#tabing li").click(function(){
$("#tabing li").removeClass('select');
var active=$(this).attr('id').split("_")[1];
$('.tb').hide();
$('#tab_'+active).show();
$(this).addClass('select');
$('#tab_1').hide();
$('#switch_button').attr('src','images/ar-shape.png');
});



$(".pr-ar").click(function(){
if($('#tab_1').css('display')=='none'){
$('#tab_1').slideDown();
$('#switch_button').attr('src','images/ar-shape2.png');
$('.tb').hide();
$("#tabing li").removeClass('select');
}else{
$('#tab_1').slideUp();
$('#switch_button').attr('src','images/ar-shape.png');
}
});




//----------------------tool tip---------------------------------------------


$('.toolbar-option-epanel div').hover(function(){
$('.tooltip').html($(this).attr('tooltext')).css({'position':'absolute','top':$(this).offset().top+34,'left':$(this).offset().left}).show();
},function(){
$('.tooltip').hide();
});
$('.tooltip').hover(function(){
	$('.tooltip').show();
},function(){
	$('.tooltip').hide();
})
//---------------------------------------------------------------------
})

  
    $(".3dpreview img").hover(function(){
     $('#ZoomPopup').fadeIn();
	 },function(){
	  $('#ZoomPopup').fadeOut();
    });

  /*$(".3dpreview").mousemove(function(event) {
  var left=event.pageX+15; 
  var top=event.pageY-15;
  $('.preview-tip').css({'left':left,'top':top});
  });*/
  
  $(".3dpreview").click(function(e){
  $("#preview-section").fadeOut();
  $('#ZoomPopup').fadeOut();
  $('#container').fadeIn()
  
  })  
</script>